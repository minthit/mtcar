package com.minthittun.mtcar;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.adapter.AuthorizedDealerListAdapter;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.adapter.CarSearchAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.MemberModel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthorizedDealerListActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    private AuthorizedDealerListAdapter authorizedDealerListAdapter;
    private ServiceHelper.ApiService service;
    private Call<ArrayList<MemberModel>> callList;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_authorized_dealer_list;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        getList();

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText(getIntent().getStringExtra("brand").toString());

        service = ServiceHelper.getClient(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        authorizedDealerListAdapter = new AuthorizedDealerListAdapter();
        recycler_view.setAdapter(authorizedDealerListAdapter);
    }

    private void getList()
    {
        recycler_view.showShimmerAdapter();

        authorizedDealerListAdapter.clear();
        // Clear already showing footers
        authorizedDealerListAdapter.clearFooter();

        // Show Loading
        authorizedDealerListAdapter.showLoading();

        callList = service.getAuthorizedDealerListByBrand(getIntent().getStringExtra("brand"));
        callList.enqueue(new Callback<ArrayList<MemberModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberModel>> call, Response<ArrayList<MemberModel>> response) {

                recycler_view.hideShimmerAdapter();
                // Loading Ends
                authorizedDealerListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {


                    ArrayList<MemberModel> resultModel = response.body();

                    if(resultModel.size() > 0)
                    {
                        for (MemberModel t : resultModel) {
                            authorizedDealerListAdapter.add(t);
                        }
                    }
                    else
                    {
                        authorizedDealerListAdapter.showEmptyView(R.layout.layout_empty_view);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<MemberModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                recycler_view.hideShimmerAdapter();
                authorizedDealerListAdapter.clearFooter();
                authorizedDealerListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }

        });
    }

}
