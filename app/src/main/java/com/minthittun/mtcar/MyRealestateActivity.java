package com.minthittun.mtcar;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.minthittun.mtcar.adapter.MyCarListAdapter;
import com.minthittun.mtcar.adapter.MyRealestateListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.RealestateSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.RealEstatePhotoModel;
import com.minthittun.mtcar.model.SaveRealestateModel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRealestateActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RealestateSaveCallback {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    private SharePreferenceHelper sharePreferenceHelper;
    public static boolean isRefreshNeed = false;

    ServiceHelper.ApiService service;
    Call<ArrayList<RealEstatePhotoModel>> callList;

    MyRealestateListAdapter myRealestateListAdapter;

    private String member_id;
    private boolean isMe;
    private String member_name = "", member_phone;

    Call<SaveRealestateModel> callSaveRealestate;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_realestate;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();



        getList();

    }

    private void init()
    {
        setupToolbar(true);
        swipe_refresh_layout.setOnRefreshListener(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);

        if(getIntent().getStringExtra("id") == null)
        {
            setupToolbarText("My Real Estate");
            member_id = sharePreferenceHelper.getLoginMemberId();
            isMe = true;
            member_name = "";
            member_phone = "";
        }
        else
        {
            setupToolbarText("Real Estate");
            member_id = getIntent().getStringExtra("id");
            isMe = false;
            member_name = getIntent().getStringExtra("name");
            member_phone = getIntent().getStringExtra("phone");
        }



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        myRealestateListAdapter = new MyRealestateListAdapter(isMe, member_name, member_phone, this);
        myRealestateListAdapter.registerCallback(this);
        recycler_view.setAdapter(myRealestateListAdapter);
    }

    private void getList()
    {
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        myRealestateListAdapter.clearFooter();

        // Show Loading
        myRealestateListAdapter.showLoading();
        if(getIntent().getStringExtra("save") == null)
        {
            callList = service.getRealEstateListByMemberId(member_id);
        }
        else
        {
            callList = service.getSaveRealestateListByMemberId(member_id);
        }
        callList.enqueue(new Callback<ArrayList<RealEstatePhotoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RealEstatePhotoModel>> call, Response<ArrayList<RealEstatePhotoModel>> response) {

                // Loading Ends
                myRealestateListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {
                    ArrayList<RealEstatePhotoModel> resultModel = response.body();

                    for (RealEstatePhotoModel q : resultModel) {
                        myRealestateListAdapter.add(q);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<RealEstatePhotoModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                myRealestateListAdapter.clearFooter();
                myRealestateListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new MyCarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {

        myRealestateListAdapter.clear();
        getList();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if(isRefreshNeed)
        {
            onRefresh();
            isRefreshNeed = false;
        }

    }

    @Override
    public void saveRealestate(String realestateId) {
        callSaveRealestate = service.saveRealestate(realestateId, sharePreferenceHelper.getLoginMemberId());
        callSaveRealestate.enqueue(new Callback<SaveRealestateModel>() {
            @Override
            public void onResponse(Call<SaveRealestateModel> call, Response<SaveRealestateModel> response) {

            }

            @Override
            public void onFailure(Call<SaveRealestateModel> call, Throwable t) {

            }
        });
    }
}
