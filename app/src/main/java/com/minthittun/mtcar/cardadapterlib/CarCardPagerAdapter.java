package com.minthittun.mtcar.cardadapterlib;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.model.CarModel;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CarCardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CarPhotoMemberModel> mData;
    private float mBaseElevation;

    public CarCardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CarPhotoMemberModel item) {
        mViews.add(null);
        mData.add(item);
    }

    public void clearList()
    {
        mData.clear();
        notifyDataSetChanged();
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.car_pager_layout, container, false);
        container.addView(view);


        final CarPhotoMemberModel carModel = mData.get(position);
        bind(carModel, view);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        /*if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);*/
        cardView.setElevation(0);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                carPhotoModel.setCar(carModel.getCar());
                carPhotoModel.setPhotos(carModel.getPhotos());
                carPhotoModel.setMemberId(carModel.getMember().getMemberId());
                carPhotoModel.setContact(carModel.getMember().getPhone());
                carPhotoModel.setMemberName(carModel.getMember().getName());
                carPhotoModel.setMemberType(carModel.getMember().getType());
                carPhotoModel.setAuthorizedDealerBrand(carModel.getMember().getAuthorizedDealerBrand());

                Intent intent = new Intent(view.getContext(), CarDetailActivity.class);
                intent.putExtra("obj", carPhotoModel);
                view.getContext().startActivity(intent);

            }
        });

        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(CarPhotoMemberModel model, View view) {
        ImageView imgv_car = (ImageView) view.findViewById(R.id.imgv_car);
        MyanBoldTextView tv_title = (MyanBoldTextView) view.findViewById(R.id.tv_title);
        MyanTextView tv_desc = (MyanTextView) view.findViewById(R.id.tv_desc);

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
        Picasso.with(view.getContext())
                .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerCrop()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_car);

        tv_title.setMyanmarText(model.getCar().getModelName() + " - Car No. " + model.getCar().getCarNo());
        tv_desc.setMyanmarText("Year " + model.getCar().getYear() + " - Miles " + model.getCar().getKilometer() + " Km");
    }


}
