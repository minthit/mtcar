package com.minthittun.mtcar.cardadapterlib;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.minthittun.mtcar.CarFeedActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.DpToPxHelper;
import com.minthittun.mtcar.model.CarModel;
import com.minthittun.mtcar.model.CartypeModel;

import java.util.ArrayList;
import java.util.List;

public class CarTypeCardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CartypeModel> mData;
    private float mBaseElevation;

    public CarTypeCardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CartypeModel item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.car_type_pager_layout, container, false);
        container.addView(view);


        final CartypeModel carType = mData.get(position);
        bind(carType, view);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        /*if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);*/


        cardView.setElevation(0);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarFeedActivity.setQuery(0, 0, false, carType.getTitle(), "");
                Intent intent = new Intent(view.getContext(), CarFeedActivity.class);
                view.getContext().startActivity(intent);

            }
        });

        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(CartypeModel model, View view) {
        ImageView imgv_car_type = (ImageView) view.findViewById(R.id.imgv_car_type);
        MyanBoldTextView tv_title = (MyanBoldTextView) view.findViewById(R.id.tv_title);

        int resID = view.getContext().getResources().getIdentifier(model.getIcon(), "mipmap",  view.getContext().getPackageName());
        imgv_car_type.setImageResource(resID);
        tv_title.setMyanmarText(model.getTitle());
    }


}
