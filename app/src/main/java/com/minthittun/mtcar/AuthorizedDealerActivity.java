package com.minthittun.mtcar;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koekoetech.androidcommonlibrary.custom_control.MyanProgressDialog;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CustomDialogHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthorizedDealerActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.imgv_photo)
    ImageView imgv_photo;

    @BindView(R.id.imgv_brand_logo)
    ImageView imgv_brand_logo;

    @BindView(R.id.linear_main)
    LinearLayout linear_main;

    @BindView(R.id.btn_call_now)
    FloatingActionButton btn_call_now;

    @BindView(R.id.tv_name)
    MyanTextView tv_name;

    @BindView(R.id.tv_email)
    MyanTextView tv_email;

    @BindView(R.id.tv_detail)
    MyanTextView tv_detail;

    @BindView(R.id.tv_address)
    MyanTextView tv_address;

    @BindView(R.id.cv_inventory)
    CardView cv_inventory;

    @BindView(R.id.mapView)
    MapView mapView;
    private GoogleMap mMap;

    private MemberModel memberModel;
    private ServiceHelper.ApiService service;
    private Call<MemberModel> callMember;
    private MyanProgressDialog myanProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_authorized_dealer;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        MapsInitializer.initialize(this);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        init();

        showDetail();

        btn_call_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + memberModel.getAuthorizedDealerContact()));
                startActivity(intent);

            }
        });

        cv_inventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(AuthorizedDealerActivity.this, CarFeedActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(AuthorizedDealerActivity.this);*/
                Intent intent = new Intent(AuthorizedDealerActivity.this, MyCarActivity.class);
                intent.putExtra("me", false);
                intent.putExtra("name", memberModel.getName());
                intent.putExtra("phone", memberModel.getPhone());
                intent.putExtra("id", memberModel.getMemberId());
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(AuthorizedDealerActivity.this);

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText(" ");

        myanProgressDialog = new MyanProgressDialog(this);
        service = ServiceHelper.getClient(this);

    }

    private void showDetail()
    {
        linear_main.setVisibility(View.GONE);
        //myanProgressDialog.showDialog();
        callMember = service.getMemberById(getIntent().getStringExtra("member_id"));
        callMember.enqueue(new Callback<MemberModel>() {
            @Override
            public void onResponse(Call<MemberModel> call, Response<MemberModel> response) {

                myanProgressDialog.hideDialog();

                if(response.isSuccessful())
                {
                    linear_main.setVisibility(View.VISIBLE);
                    memberModel = response.body();

                    int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));

                    Picasso.with(AuthorizedDealerActivity.this)
                            .load(MtCarConstant.BASE_PHOTO_URL + memberModel.getPhoto())
                            .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.placeholder)
                            .into(imgv_photo);

                    setupToolbarText(memberModel.getName());

                    int resID = getResources().getIdentifier("brand_" + memberModel.getAuthorizedDealerBrand().toLowerCase().replace(" ", "_"), "mipmap",  getPackageName());
                    imgv_brand_logo.setImageResource(resID);

                    tv_name.setMyanmarText(memberModel.getName());
                    tv_email.setMyanmarText(memberModel.getAuthorizedDealerEmail());
                    tv_detail.setMyanmarText(memberModel.getDescription());
                    tv_address.setMyanmarText(memberModel.getAuthorizedDealerAddress());
                }
                else
                {
                    retry();
                }

            }

            @Override
            public void onFailure(Call<MemberModel> call, Throwable t) {

                myanProgressDialog.hideDialog();
                retry();

            }

            private void retry()
            {
                final CustomDialogHelper customDialogHelper = new CustomDialogHelper(getApplicationContext(), false, false);
                customDialogHelper.showDialog("Connection problem", "Please try again.", "Retry");
                customDialogHelper.getTextView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        customDialogHelper.hideDialog();
                        showDetail();

                    }
                });
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng latLng = new LatLng(16.871311, 96.199379);
        updateUserCamera(latLng);

    }

    private void updateUserCamera(LatLng latLng)
    {
        mMap.clear();

        mMap.addMarker(new MarkerOptions().draggable(false).position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin))
                .title("You are here"));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(18)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }
}
