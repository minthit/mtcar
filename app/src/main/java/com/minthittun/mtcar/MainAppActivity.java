package com.minthittun.mtcar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.minthittun.mtcar.adapter.MainAppViewPagerAdapter;
import com.minthittun.mtcar.common.BaseActivity;

import butterknife.BindView;

public class MainAppActivity extends BaseActivity {

    //View declaration
    @BindView(R.id.view_pager)
    ViewPager view_pager;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    //View declaration

    //Utils declaration
    private MainAppViewPagerAdapter mainAppViewPagerAdapter;
    //Utils declaration

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_car_feed:
                    //Change Fragment
                    view_pager.setCurrentItem(0);
                    return true;
                case R.id.navigation_sell:
                    //Change Fragment
                    view_pager.setCurrentItem(1);
                    return true;
                case R.id.navigation_dealer_showroom:
                    //Change Fragment
                    view_pager.setCurrentItem(2);
                    return true;
                case R.id.navigation_menu:
                    //Change Fragment
                    view_pager.setCurrentItem(3);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main_app;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(false);
        setupToolbarText("");
        getSupportActionBar().hide();

        init();

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    navigation.getMenu().getItem(0).setChecked(true);
                }
                else if (position == 1) {
                    navigation.getMenu().getItem(1).setChecked(true);
                }
                else if (position == 2) {
                    navigation.getMenu().getItem(2).setChecked(true);
                }
                else if (position == 3) {
                    navigation.getMenu().getItem(3).setChecked(true);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void init() {

        //getSupportActionBar().setIcon(R.mipmap.logo_toolbar);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        view_pager.setOffscreenPageLimit(4);
        mainAppViewPagerAdapter = new MainAppViewPagerAdapter(getSupportFragmentManager(), MainAppActivity.this);
        view_pager.setAdapter(mainAppViewPagerAdapter);

    }

    public void movePager(int page){

        view_pager.setCurrentItem(page);

    }

    @Override
    public void onBackPressed() {
        int currentId = view_pager.getCurrentItem();
        switch (currentId) {
            case 0:
                finish();
                break;
            case 1:
                view_pager.setCurrentItem(0);
                break;
            case 2:
                view_pager.setCurrentItem(0);
                break;
            case 3:
                view_pager.setCurrentItem(0);
                break;
        }
    }


}
