package com.minthittun.mtcar;

import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.materialrangebar.IRangeBarFormatter;
import com.appyvet.materialrangebar.RangeBar;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.adapter.CarSearchAdapter;
import com.minthittun.mtcar.adapter.RealestateSearchAdapter;
import com.minthittun.mtcar.adapter.StateAdapter;
import com.minthittun.mtcar.adapter.TownshipAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.LocationModel;
import com.minthittun.mtcar.model.RealestatePhotoMemberModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealestateSearchActivity extends BaseActivity implements CarSaveCallback {

    private Timer timer;
    private Handler handler;

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    @BindView(R.id.edt_keyword)
    EditText edt_keyword;

    @BindView(R.id.linear_filter)
    LinearLayout linear_filter;

    ServiceHelper.ApiService service;
    Call<ArrayList<RealestatePhotoMemberModel>> callList;

    RealestateSearchAdapter realestateSearchAdapter;

    private SharePreferenceHelper sharePreferenceHelper;
    private MyDecimalFormat myDecimalFormat;

    //Filter
    private boolean isFiltered = false;
    private int PRICE_RANGE_CONST = 1000000;
    private float minPricePreview = 50, minprice = 50, maxPricePreview = 1000, maxprice = 1000;
    private String keyword = "", state = "", township = "";
    private Spinner spinner_state, spinner_township;
    private TextView tv_price_range_left_preview, tv_price_range_right_preview;
    private RangeBar range_bar_price;
    private Button btn_filter;
    private LinearLayout linear_clear_filter;
    private StateAdapter stateAdapter;
    private TownshipAdapter townshipAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_realestate_search;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        edt_keyword.addTextChangedListener(searchTextWatcher);

        linear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = getLayoutInflater().inflate(R.layout.real_estate_filter_layout, null);

                final BottomSheetDialog dialog = new BottomSheetDialog(RealestateSearchActivity.this);
                dialog.setContentView(view);

                //init
                tv_price_range_left_preview = dialog.findViewById(R.id.tv_price_range_left_preview);
                tv_price_range_right_preview = dialog.findViewById(R.id.tv_price_range_right_preview);
                btn_filter = dialog.findViewById(R.id.btn_filter);

                spinner_state = dialog.findViewById(R.id.spinner_state);
                stateAdapter = new StateAdapter(RealestateSearchActivity.this, R.layout.item_state_township);
                spinner_state.setAdapter(stateAdapter);
                stateAdapter.addAll(getAllState());

                spinner_township = dialog.findViewById(R.id.spinner_township);
                townshipAdapter = new TownshipAdapter(RealestateSearchActivity.this, R.layout.item_state_township);
                spinner_township.setAdapter(townshipAdapter);

                linear_clear_filter = dialog.findViewById(R.id.linear_clear_filter);
                range_bar_price = dialog.findViewById(R.id.range_bar_price);
                //init

                dialog.show();

                //data bind

                selectValue(spinner_state, state);
                selectValue(spinner_township, township);

                range_bar_price.setRangePinsByValue(minPricePreview, maxPricePreview);
                minprice = Float.parseFloat(range_bar_price.getLeftPinValue()) * PRICE_RANGE_CONST;
                maxprice = Float.parseFloat(range_bar_price.getRightPinValue()) * PRICE_RANGE_CONST;
                tv_price_range_left_preview.setText(myDecimalFormat.getCurrencyFormat(minprice) + " KS");
                tv_price_range_right_preview.setText(myDecimalFormat.getCurrencyFormat(maxprice) +  " KS");

                //data bind

                //Event

                range_bar_price.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                    @Override
                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                        minPricePreview = Float.parseFloat(leftPinValue);
                        maxPricePreview = Float.parseFloat(rightPinValue);
                        minprice = Float.parseFloat(leftPinValue) * PRICE_RANGE_CONST;
                        maxprice = Float.parseFloat(rightPinValue) * PRICE_RANGE_CONST;
                        tv_price_range_left_preview.setText(myDecimalFormat.getCurrencyFormat(minprice) + " KS");
                        tv_price_range_right_preview.setText(myDecimalFormat.getCurrencyFormat(maxprice) +  " KS");

                    }
                });

                range_bar_price.setFormatter(new IRangeBarFormatter() {
                    @Override
                    public String format(String value) {

                        return value + "lk";
                    }
                });

                spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        townshipAdapter.clear();
                        townshipAdapter.addAll(getAllTownship(spinner_state.getSelectedItem().toString()));

                        if(state != null)
                        {
                            if(township != null)
                            {
                                selectValue(spinner_township, township);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                linear_clear_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        state = "";
                        township = "";

                        minprice = 50 * PRICE_RANGE_CONST;
                        minPricePreview = 50;
                        maxprice = 1000 * PRICE_RANGE_CONST;
                        maxPricePreview = 1000;

                        range_bar_price.setRangePinsByValue(minPricePreview, maxPricePreview);

                        performSearch();

                        dialog.dismiss();

                    }
                });

                btn_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        isFiltered = true;
                        dialog.dismiss();

                        if(!spinner_state.getSelectedItem().toString().equals("Select"))
                        {
                            state = spinner_state.getSelectedItem().toString();
                        }

                        if(!spinner_township.getSelectedItem().toString().equals("Select"))
                        {
                            township = spinner_township.getSelectedItem().toString();
                        }

                        performSearch();

                    }
                });

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Real Estate Search");

        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        myDecimalFormat = new MyDecimalFormat();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        realestateSearchAdapter = new RealestateSearchAdapter();
        realestateSearchAdapter.registerCallback(this);
        recycler_view.setAdapter(realestateSearchAdapter);

    }

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // do your actual work here

                    RealestateSearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!edt_keyword.getText().toString().isEmpty())
                            {
                                keyword = edt_keyword.getText().toString();
                                performSearch();
                            }
                            else
                            {
                                keyword = "";
                            }
                        }
                    });

                }
            }, 600);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // nothing to do here
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // user is typing: reset already started timer (if existing)
            if (timer != null) {
                timer.cancel();
            }
        }
    };


    private void performSearch()
    {
        if(!isFiltered)
        {
            minprice = 50 * PRICE_RANGE_CONST;
            maxprice = 1000 * PRICE_RANGE_CONST;
        }

        recycler_view.showShimmerAdapter();
        handler = new Handler();

        realestateSearchAdapter.clear();
        // Clear already showing footers
        realestateSearchAdapter.clearFooter();

        // Show Loading
        realestateSearchAdapter.showLoading();

        callList = service.searchRealestate(sharePreferenceHelper.getLoginMemberId(), keyword, state, township, minprice, maxprice);
        callList.enqueue(new Callback<ArrayList<RealestatePhotoMemberModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RealestatePhotoMemberModel>> call, Response<ArrayList<RealestatePhotoMemberModel>> response) {

                recycler_view.hideShimmerAdapter();
                // Loading Ends
                realestateSearchAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {

                    ArrayList<RealestatePhotoMemberModel> resultModel = response.body();

                    if(resultModel.size() > 0)
                    {
                        for (RealestatePhotoMemberModel t : resultModel) {
                            realestateSearchAdapter.add(t);
                        }
                    }
                    else
                    {
                        realestateSearchAdapter.showEmptyView(R.layout.layout_empty_view);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<RealestatePhotoMemberModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                recycler_view.hideShimmerAdapter();
                realestateSearchAdapter.clearFooter();
                realestateSearchAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        performSearch();
                    }
                });
            }

        });
    }


    @Override
    public void saveCar(String carId) {

    }

    private ArrayList<String> getAllState()
    {
        ArrayList<String> resultList = new ArrayList<>();
        RealmQuery<LocationModel> stateList = Realm.getDefaultInstance().where(LocationModel.class).distinct("StateDivision");

        resultList.add("Select");
        for (LocationModel location :
                stateList.findAll()) {
            resultList.add(location.getStateDivision().toString());
        }

        return  resultList;
    }

    private ArrayList<String> getAllTownship(String state)
    {
        ArrayList<String> resultList = new ArrayList<>();
        RealmResults<LocationModel> townshipList = Realm.getDefaultInstance().where(LocationModel.class).equalTo("StateDivision", state).findAll();

        resultList.add("Select");
        for(int i = 0; i<townshipList.size(); i++)
        {
            resultList.add(townshipList.get(i).getTownship().toString());
        }

        return  resultList;
    }

    private void selectValue(Spinner spinner, Object value)
    {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

}
