package com.minthittun.mtcar;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CameraHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.PictureCameraCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;
import java.text.ParseException;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity implements PictureCameraCallback, UploadStatusDelegate {

    @BindView(R.id.btn_save)
    Button btn_save;

    @BindView(R.id.img_camera)
    ImageView btn_camera;

    @BindView(R.id.img_gallery)
    ImageView btn_gallery;

    @BindView(R.id.imgv_profile)
    CircleImageView imgv_profile;

    @BindView(R.id.edt_name)
    EditText edt_name;

    @BindView(R.id.edt_detail)
    EditText edt_detail;

    @BindView(R.id.spinner_type)
    Spinner spinner_type;

    @BindView(R.id.linear_authorized_dealer)
    LinearLayout linear_authorized_dealer;

    @BindView(R.id.spinner_authorized_dealer_brand)
    Spinner spinner_authorized_dealer_brand;

    @BindView(R.id.edt_authorized_dealer_contact)
    EditText edt_authorized_dealer_contact;

    @BindView(R.id.edt_authorized_dealer_email)
    EditText edt_authorized_dealer_email;

    @BindView(R.id.edt_authorized_dealer_address)
    EditText edt_authorized_dealer_address;

    private ServiceHelper.ApiService service;
    private Call<MemberModel> callUpdateMember;
    private SharePreferenceHelper sharePreferenceHelper;
    private Realm realm;
    private MemberModel oldMember;
    private Handler handler;
    private CameraHelper cameraHelper;
    private ProgressDialog progressDialog;
    private String picturePath = "";
    private String fileName = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getMemberInfo();

        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callCamera();

            }
        });

        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGallery();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isValid())
                {
                    updateMemberInfo(getDataFromControl());
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please type your name and choose account type.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        spinner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(spinner_type.getSelectedItem().toString().equals("Authorized Dealer"))
                {
                    linear_authorized_dealer.setVisibility(View.VISIBLE);
                }
                else
                {
                    linear_authorized_dealer.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Profile Setup");

        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        realm = Realm.getDefaultInstance();
        handler = new Handler();
        cameraHelper = new CameraHelper(this);
        cameraHelper.registerCallback(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.setCancelable(false);
    }

    private MemberModel getMemberInfoById()
    {
        MemberModel model = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();
        return model;
    }

    private void getMemberInfo()
    {
        oldMember = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
        Picasso.with(ProfileActivity.this)
                .load(MtCarConstant.BASE_PHOTO_URL + oldMember.getPhoto())
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerCrop()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_profile);

        edt_name.setText(oldMember.getName());
        edt_detail.setText(oldMember.getDescription());
        selectValue(spinner_type, oldMember.getType());

        edt_authorized_dealer_contact.setText(oldMember.getAuthorizedDealerContact());
        edt_authorized_dealer_email.setText(oldMember.getAuthorizedDealerEmail());
        edt_authorized_dealer_address.setText(oldMember.getAuthorizedDealerAddress());
        selectValue(spinner_authorized_dealer_brand, oldMember.getAuthorizedDealerBrand());

    }

    private void updateMemberInfo(MemberModel model)
    {
        progressDialog.show();
        callUpdateMember = service.memberProfileUpdate(model);
        callUpdateMember.enqueue(new Callback<MemberModel>() {
            @Override
            public void onResponse(Call<MemberModel> call, Response<MemberModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    MemberModel memberInfoModel = response.body();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(memberInfoModel);
                    realm.commitTransaction();
                    Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }

            @Override
            public void onFailure(Call<MemberModel> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private MemberModel getDataFromControl()
    {
        MemberModel model = new MemberModel();
        model.setMemberId(oldMember.getMemberId());
        model.setPhone(oldMember.getPhone());
        model.setPhoto(oldMember.getPhoto());
        model.setName(edt_name.getText().toString());
        model.setDescription(edt_detail.getText().toString());

        if(!spinner_type.getSelectedItem().toString().equals("Select account type here"))
        {
            model.setType(spinner_type.getSelectedItem().toString());
        }

        model.setAuthorizedDealerContact(edt_authorized_dealer_contact.getText().toString());
        model.setAuthorizedDealerEmail(edt_authorized_dealer_email.getText().toString());
        model.setAuthorizedDealerAddress(edt_authorized_dealer_address.getText().toString());
        if(!spinner_authorized_dealer_brand.getSelectedItem().toString().equals("Select"))
        {
            model.setAuthorizedDealerBrand(spinner_authorized_dealer_brand.getSelectedItem().toString());
        }

        return model;
    }

    private void selectValue(Spinner spinner, Object value)
    {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public void callCamera() {

        sharePreferenceHelper.setTempPhotoName(cameraHelper.getDateTImeStampForFileName() + ".jpg");

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File pictureDirectory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                cameraHelper.TEMP_FOLDER_NAME);
        if (!pictureDirectory.exists()) {
            pictureDirectory.mkdirs();
        }
        File file = new File(pictureDirectory.getAbsolutePath()
                + File.separator + sharePreferenceHelper.getTempPhotoName());

        sharePreferenceHelper.setTempPhotoPath(file.getAbsolutePath());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, 222);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 111) {

                galleryProcess(data);

            }
            else if (requestCode == 222) {

                progressDialog.show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        cameraProcess();

                    }
                }, 2000);


            }
        }

    }

    private void cameraProcess() {
        File pictureDirectory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                cameraHelper.TEMP_FOLDER_NAME);
        File file = new File(pictureDirectory.getAbsolutePath()
                + File.separator + sharePreferenceHelper.getTempPhotoName());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;// reduce images resolution

        Bitmap bmImg = BitmapFactory.decodeFile(file.getAbsolutePath(),
                options);

        Log.d("PhotoListActivity", "FileName : " + sharePreferenceHelper.getTempPhotoName() + " Path : " + file.getPath());
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, file.getAbsolutePath());

        cameraHelper.saveMemberProfilePicture(temp);

        progressDialog.dismiss();
    }

    private void galleryProcess(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap bmImg = BitmapFactory.decodeFile(picturePath, options);
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, picturePath);

        cameraHelper.saveMemberProfilePicture(temp);
    }

    public void callGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, 111);
    }

    @Override
    public void getFilePath(String name, String path) {

        fileName = name;
        imgv_profile.setImageURI(Uri.parse(path));

        uploadMultipart(name, path);

    }

    public void uploadMultipart(String photoName, String path) {
        try {
            MultipartUploadRequest req =
                    new MultipartUploadRequest(this, MtCarConstant.BASE_URL + "member/uploadPhoto")
                            .addFileToUpload(path, "file")
                            .addParameter("memberId", String.valueOf(sharePreferenceHelper.getLoginMemberId()))
                            .addParameter("fileName", photoName)
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2);

            String uploadID = req.setDelegate(this).startUpload();

            cameraHelper.deleteFile(sharePreferenceHelper.getTempPhotoPath());

        } catch (Exception exc) {
            Log.e("AndroidUploadService", exc.getMessage(), exc);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (checkWriteExternalStoragePermission() && checkReadExternalStoragePermission() && checkCameraPermission()) {
        } else {
            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA}, 1);
        }
    }


    private boolean checkCameraPermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkReadExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkWriteExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }




    private boolean isValid()
    {
        if(!edt_name.getText().toString().equals("") && !spinner_type.getSelectedItem().toString().equals("Select"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {
        progressDialog.show();
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        progressDialog.dismiss();
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        MemberModel model = getMemberInfoById();
        realm.beginTransaction();
        model.setPhoto(fileName);
        realm.commitTransaction();

        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Profile photo uploaded successful!", Toast.LENGTH_SHORT).show();
        //finish();
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {
        progressDialog.dismiss();
    }
}
