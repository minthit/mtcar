package com.minthittun.mtcar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.minthittun.mtcar.adapter.StateAdapter;
import com.minthittun.mtcar.adapter.TownshipAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarModel;
import com.minthittun.mtcar.model.LocationModel;
import com.minthittun.mtcar.model.RealEstateModel;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealEstateFormActivity extends BaseActivity {

    @BindView(R.id.linear_is_deal)
    LinearLayout linear_is_deal;

    @BindView(R.id.linear_gallery)
    LinearLayout linear_gallery;

    @BindView(R.id.edt_address)
    EditText edt_address;

    @BindView(R.id.spinner_state)
    Spinner spinner_state;

    @BindView(R.id.spinner_township)
    Spinner spinner_township;

    @BindView(R.id.edt_price)
    EditText edt_price;

    @BindView(R.id.edt_bed)
    EditText edt_bed;

    @BindView(R.id.edt_baths)
    EditText edt_baths;

    @BindView(R.id.edt_house_size)
    EditText edt_house_size;

    @BindView(R.id.edt_lot_size)
    EditText edt_lot_size;

    @BindView(R.id.chk_active)
    CheckBox chk_active;

    @BindView(R.id.chk_is_deal)
    CheckBox chk_is_deal;

    @BindView(R.id.btn_save)
    Button btn_save;

    @BindView(R.id.edt_detail)
    EditText edt_detail;

    @BindView(R.id.rbtn_sale)
    RadioButton rbtn_sale;

    @BindView(R.id.rbtn_rent)
    RadioButton rbtn_rent;

    private SharePreferenceHelper sharePreferenceHelper;
    private ServiceHelper.ApiService service;
    private Call<RealEstateModel> callRealestate;
    private Call<RealEstateModel> callUpdateRealestate;
    private ProgressDialog progressDialog;

    private RealEstateModel oldRealEstateModel;

    private StateAdapter stateAdapter;
    private TownshipAdapter townshipAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_real_estate_form;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(btn_save.getText().equals("SAVE"))
                {
                    if(!edt_address.getText().toString().equals(""))
                    {
                        saveCar(getDataFromControl());
                    }
                }
                else if(btn_save.getText().equals("UPDATE"))
                {
                    if(!edt_address.getText().toString().equals(""))
                    {
                        updateRealestate(getDataFromControl());
                    }
                }

            }
        });

        linear_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RealEstateFormActivity.this, CarPhotoGalleryActivity.class);
                intent.putExtra("id", oldRealEstateModel.getRealEstateId());
                startActivity(intent);

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Add new");

        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading ....");

        stateAdapter = new StateAdapter(this, R.layout.item_state_township);
        spinner_state.setAdapter(stateAdapter);
        stateAdapter.addAll(getAllState());

        townshipAdapter = new TownshipAdapter(this, R.layout.item_state_township);
        spinner_township.setAdapter(townshipAdapter);

        oldRealEstateModel = (RealEstateModel)getIntent().getSerializableExtra("obj");

        if(oldRealEstateModel != null)
        {
            linear_is_deal.setVisibility(View.VISIBLE);
            binData();
            btn_save.setText("UPDATE");
            linear_gallery.setVisibility(View.VISIBLE);
        }
        else
        {
            linear_is_deal.setVisibility(View.GONE);
            btn_save.setText("SAVE");
            linear_gallery.setVisibility(View.GONE);
        }

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                townshipAdapter.clear();
                townshipAdapter.addAll(getAllTownship(spinner_state.getSelectedItem().toString()));

                if(oldRealEstateModel != null)
                {
                    if(oldRealEstateModel.getTownship() != null)
                    {
                        selectValue(spinner_township, oldRealEstateModel.getTownship());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void binData()
    {
        if(oldRealEstateModel.getSaleOrRent() == 1)
        {
            rbtn_sale.setChecked(true);
        }
        else if(oldRealEstateModel.getSaleOrRent() == 2)
        {
            rbtn_rent.setChecked(true);
        }
        else
        {
            rbtn_sale.setChecked(false);
            rbtn_rent.setChecked(false);
        }

        edt_address.setText(oldRealEstateModel.getAddress());
        selectValue(spinner_state, oldRealEstateModel.getState());
        selectValue(spinner_township, oldRealEstateModel.getTownship());
        edt_price.setText(getFormattedMoney(oldRealEstateModel.getPrice()));
        edt_bed.setText(String.valueOf(oldRealEstateModel.getBeds()));
        edt_baths.setText(String.valueOf(oldRealEstateModel.getBaths()));
        edt_house_size.setText(String.valueOf(oldRealEstateModel.getHouseSize()));
        edt_lot_size.setText(String.valueOf(oldRealEstateModel.getLotSize()));
        edt_detail.setText(oldRealEstateModel.getDetail());
        chk_active.setChecked(oldRealEstateModel.isActive());
        chk_is_deal.setChecked(oldRealEstateModel.isDeal());
    }

    private RealEstateModel getDataFromControl()
    {
        RealEstateModel model = new RealEstateModel();

        if(oldRealEstateModel != null)
        {
            model = oldRealEstateModel;
        }

        model.setSaleOrRent(isSaleOrRent());
        model.setMemberId(sharePreferenceHelper.getLoginMemberId());
        model.setAddress(edt_address.getText().toString());
        if(!spinner_state.getSelectedItem().toString().equals("Select"))
        {
            model.setState(spinner_state.getSelectedItem().toString());
        }
        if(!spinner_township.getSelectedItem().toString().equals("Select"))
        {
            model.setTownship(spinner_township.getSelectedItem().toString());
        }
        model.setPrice(Double.parseDouble(edt_price.getText().toString()));
        model.setBeds(Integer.parseInt(edt_bed.getText().toString()));
        model.setBaths(Integer.parseInt(edt_baths.getText().toString()));
        model.setHouseSize(Double.parseDouble(edt_house_size.getText().toString()));
        model.setLotSize(Double.parseDouble(edt_lot_size.getText().toString()));
        model.setDetail(edt_detail.getText().toString());
        model.setActive(chk_active.isChecked());
        model.setDeal(chk_is_deal.isChecked());


        return model;
    }

    private void saveCar(RealEstateModel model)
    {
        progressDialog.setMessage("Saving new record ......");
        progressDialog.show();
        callRealestate = service.addRealEstate(model);
        callRealestate.enqueue(new Callback<RealEstateModel>() {
            @Override
            public void onResponse(Call<RealEstateModel> call, final Response<RealEstateModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    new AlertDialog.Builder(RealEstateFormActivity.this)
                            .setMessage("Successfully added new item. Please upload photos.")
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int whichButton) {

                                            finish();

                                            Intent intent = new Intent(RealEstateFormActivity.this, RealEstateGalleryActivity.class);
                                            intent.putExtra("id", response.body().getRealEstateId());
                                            startActivity(intent);

                                        }
                                    }).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RealEstateModel> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateRealestate(RealEstateModel model)
    {
        progressDialog.setMessage("Updating record ....");
        progressDialog.show();
        callUpdateRealestate = service.updateRealEstate(model);
        callUpdateRealestate.enqueue(new Callback<RealEstateModel>() {
            @Override
            public void onResponse(Call<RealEstateModel> call, Response<RealEstateModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RealEstateModel> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private ArrayList<String> getAllState()
    {
        ArrayList<String> resultList = new ArrayList<>();
        RealmQuery<LocationModel> stateList = Realm.getDefaultInstance().where(LocationModel.class).distinct("StateDivision");

        resultList.add("Select");
        for (LocationModel model :
                stateList.findAll()) {
            resultList.add(model.getStateDivision().toString());
        }

        return  resultList;
    }

    private ArrayList<String> getAllTownship(String state)
    {
        ArrayList<String> resultList = new ArrayList<>();
        RealmResults<LocationModel> townshipList = Realm.getDefaultInstance().where(LocationModel.class).equalTo("StateDivision", state).findAll();

        for(int i = 0; i<townshipList.size(); i++)
        {
            resultList.add(townshipList.get(i).getTownship().toString());
        }

        return  resultList;
    }
    private void selectValue(Spinner spinner, Object value)
    {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public String getFormattedMoney(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%.2f",d);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkWriteExternalStoragePermission() && checkReadExternalStoragePermission() && checkCameraPermission()) {
        } else {
            ActivityCompat.requestPermissions(RealEstateFormActivity.this, new String[]{
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA}, 1);
        }
    }


    private boolean checkCameraPermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkReadExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkWriteExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private int isSaleOrRent()
    {
        if(rbtn_sale.isChecked())
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

}
