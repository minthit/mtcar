package com.minthittun.mtcar;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.minthittun.mtcar.adapter.PhotoPagerAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.model.PhotoModel;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;

public class PhotoDetailPagerActivity extends BaseActivity {

    @BindView(R.id.pager)
    ViewPager pager;

    private PhotoPagerAdapter photoPagerAdapter;

    private Realm realm;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_photo_detail_pager;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

    }

    private void init()
    {
        setupToolbar(true);
        getSupportActionBar().hide();

        realm = Realm.getDefaultInstance();

        photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager(), (ArrayList<PhotoModel>) getIntent().getSerializableExtra("photos"), PhotoDetailPagerActivity.this);
        pager.setAdapter(photoPagerAdapter);
        pager.setCurrentItem(getIntent().getIntExtra("current_index", 0));
    }


}
