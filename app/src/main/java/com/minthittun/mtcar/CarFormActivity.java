package com.minthittun.mtcar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.minthittun.mtcar.adapter.CarColorListAdapter;
import com.minthittun.mtcar.adapter.CarTypeListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.CarTypeSelectCallback;
import com.minthittun.mtcar.helper.ColorSelectCallback;
import com.minthittun.mtcar.helper.CustomDialogHelper;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarModel;
import com.minthittun.mtcar.model.CartypeModel;
import com.minthittun.mtcar.model.ColorModel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarFormActivity extends BaseActivity implements ColorSelectCallback, CarTypeSelectCallback {

    @BindView(R.id.linear_is_deal)
    LinearLayout linear_is_deal;

    @BindView(R.id.edt_model_name)
    EditText edt_model_name;

    @BindView(R.id.edt_car_no)
    EditText edt_car_no;

    @BindView(R.id.edt_price)
    EditText edt_price;


    @BindView(R.id.spinner_manufacture)
    Spinner spinner_manufacture;

    @BindView(R.id.edt_year)
    EditText edt_year;

    @BindView(R.id.edt_km)
    EditText edt_km;

    @BindView(R.id.edt_grade)
    EditText edt_grade;

    @BindView(R.id.edt_license_type)
    EditText edt_license_type;

    @BindView(R.id.edt_current_location)
    EditText edt_current_location;

    @BindView(R.id.edt_insurance_company)
    EditText edt_insurance_company;

    @BindView(R.id.edt_insurance_remain)
    EditText edt_insurance_remain;

    @BindView(R.id.rbtn_sale)
    RadioButton rbtn_sale;

    @BindView(R.id.rbtn_rent)
    RadioButton rbtn_rent;

    @BindView(R.id.rbtn_auto)
    RadioButton rbtn_auto;

    @BindView(R.id.rbtn_manual)
    RadioButton rbtn_manual;

    @BindView(R.id.spinner_fuel)
    Spinner spinner_fuel;

    @BindView(R.id.edt_engine)
    EditText edt_engine;

    @BindView(R.id.rbtn_steering_auto)
    RadioButton rbtn_steering_auto;

    @BindView(R.id.rbtn_steering_manual)
    RadioButton rbtn_steering_manual;

    @BindView(R.id.rbtn_steering_left)
    RadioButton rbtn_steering_left;

    @BindView(R.id.rbtn_steering_right)
    RadioButton rbtn_steering_right;

    @BindView(R.id.edt_detail)
    EditText edt_detail;

    @BindView(R.id.chk_active)
    CheckBox chk_active;

    @BindView(R.id.chk_is_brand_new)
    CheckBox chk_is_brand_new;

    @BindView(R.id.chk_is_deal)
    CheckBox chk_is_deal;

    @BindView(R.id.btn_save)
    Button btn_save;

    @BindView(R.id.linear_gallery)
    LinearLayout linear_gallery;

    @BindView(R.id.rv_car_category)
    RecyclerView rv_car_category;
    private CarTypeListAdapter carTypeListAdapter;

    @BindView(R.id.rv_color)
    RecyclerView rv_color;
    private CarColorListAdapter carColorListAdapter;

    private SharePreferenceHelper sharePreferenceHelper;
    private ServiceHelper.ApiService service;
    private Call<CarModel> callCar;
    private Call<CarModel> callUpdateCar;
    private ProgressDialog progressDialog;

    private CarModel oldCarmodel;
    public ColorModel selectedColorModel = null;
    public CartypeModel selectedCarTypeModel = null;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_car_form;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();



        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(btn_save.getText().equals("SAVE"))
                {
                    if(!edt_model_name.getText().toString().equals(""))
                    {
                        final CustomDialogHelper customDialogHelper = new CustomDialogHelper(CarFormActivity.this, false, true);
                        customDialogHelper.showDialog("Confirm Message", "Are you sure want to post this car information.", "Post");
                        customDialogHelper.getTextView().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                customDialogHelper.hideDialog();

                                saveCar(getDataFromControl());

                            }
                        });

                    }
                }
                else if(btn_save.getText().equals("UPDATE"))
                {
                    if(!edt_model_name.getText().toString().equals(""))
                    {

                        final CustomDialogHelper customDialogHelper = new CustomDialogHelper(CarFormActivity.this, false, true);
                        customDialogHelper.showDialog("Confirm Message", "Are you sure want to post this car information.", "Post");
                        customDialogHelper.getTextView().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                customDialogHelper.hideDialog();

                                updateCar(getDataFromControl());

                            }
                        });

                    }
                }

            }
        });

        linear_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CarFormActivity.this, CarPhotoGalleryActivity.class);
                intent.putExtra("id", oldCarmodel.getCarId());
                startActivity(intent);

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Car entry form");

        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading ....");

        carTypeListAdapter = new CarTypeListAdapter(this);
        rv_car_category.setHasFixedSize(true);
        rv_car_category.setLayoutManager(new GridLayoutManager(this, 2));
        rv_car_category.setAdapter(carTypeListAdapter);

        carColorListAdapter = new CarColorListAdapter(this);
        rv_color.setHasFixedSize(true);
        rv_color.setLayoutManager(new GridLayoutManager(this, 4));
        rv_color.setAdapter(carColorListAdapter);

        oldCarmodel = (CarModel)getIntent().getSerializableExtra("obj");

        if(oldCarmodel != null)
        {
            linear_is_deal.setVisibility(View.VISIBLE);
            binData();
            getCarTypeList(oldCarmodel.getCategory());
            getColorList(oldCarmodel.getColor());
            btn_save.setText("UPDATE");
            linear_gallery.setVisibility(View.VISIBLE);
        }
        else
        {
            getCarTypeList("");
            getColorList("");
            linear_is_deal.setVisibility(View.GONE);
            btn_save.setText("SAVE");
            linear_gallery.setVisibility(View.GONE);
        }
    }

    private void binData()
    {
        if(oldCarmodel.getSaleOrRent() == 1)
        {
            rbtn_sale.setChecked(true);
        }
        else if(oldCarmodel.getSaleOrRent() == 2)
        {
            rbtn_rent.setChecked(true);
        }
        else
        {
            rbtn_sale.setChecked(false);
            rbtn_rent.setChecked(false);
        }

        edt_model_name.setText(oldCarmodel.getModelName());
        edt_car_no.setText(oldCarmodel.getCarNo());
        edt_price.setText(getFormattedMoney(oldCarmodel.getPrice()));
        selectValue(spinner_manufacture, oldCarmodel.getManufacture());
        edt_year.setText(String.valueOf(oldCarmodel.getYear()));
        edt_km.setText(String.valueOf(oldCarmodel.getKilometer()));
        edt_grade.setText(oldCarmodel.getGrade());
        edt_license_type.setText(oldCarmodel.getLicenseType());
        edt_current_location.setText(oldCarmodel.getCurrentLocation());
        edt_insurance_company.setText(oldCarmodel.getInsuranceCompany());
        edt_insurance_remain.setText(String.valueOf(oldCarmodel.getInsuranceRemain()));
        if(oldCarmodel.getTransmissionType().equals("Auto"))
        {
            rbtn_auto.setChecked(true);
        }
        else if (oldCarmodel.getTransmissionType().equals("Manual"))
        {
            rbtn_manual.setChecked(true);
        }
        selectValue(spinner_fuel, oldCarmodel.getFuel());

        edt_engine.setText(String.valueOf(oldCarmodel.getEngine()));
        if(oldCarmodel.getSteeringType().equals("Auto"))
        {
            rbtn_steering_auto.setChecked(true);
        }
        else if (oldCarmodel.getSteeringType().equals("Manual"))
        {
            rbtn_steering_manual.setChecked(true);
        }

        if(oldCarmodel.getSteeringPlace().equals("Left"))
        {
            rbtn_steering_left.setChecked(true);
        }
        else if (oldCarmodel.getSteeringPlace().equals("Right"))
        {
            rbtn_steering_right.setChecked(true);
        }

        edt_detail.setText(oldCarmodel.getDetail());
        chk_active.setChecked(oldCarmodel.isActive());
        if(oldCarmodel.getIsBrandNew() == 1)
        {
            chk_is_brand_new.setChecked(true);
        }
        else
        {
            chk_is_brand_new.setChecked(false);
        }

        chk_is_deal.setChecked(oldCarmodel.isDeal());
    }

    private CarModel getDataFromControl()
    {
        CarModel model = new CarModel();

        if(oldCarmodel != null)
        {
            model = oldCarmodel;
        }

        model.setSaleOrRent(isSaleOrRent());
        model.setMemberId(sharePreferenceHelper.getLoginMemberId());
        model.setModelName(edt_model_name.getText().toString());
        model.setCarNo(edt_car_no.getText().toString());
        model.setPrice(Double.parseDouble(edt_price.getText().toString()));
        model.setCategory(selectedCarTypeModel.getTitle());

        if(!spinner_manufacture.getSelectedItem().toString().equals("Select"))
        {
            model.setManufacture(spinner_manufacture.getSelectedItem().toString());
        }

        model.setYear(Integer.parseInt(edt_year.getText().toString()));
        model.setKilometer(Double.parseDouble(edt_km.getText().toString()));
        model.setGrade(edt_grade.getText().toString());
        model.setLicenseType(edt_license_type.getText().toString());
        model.setCurrentLocation(edt_current_location.getText().toString());
        model.setInsuranceCompany(edt_insurance_company.getText().toString());
        model.setInsuranceRemain(Integer.parseInt(edt_insurance_remain.getText().toString()));

        if(rbtn_auto.isChecked())
        {
            model.setTransmissionType(rbtn_auto.getText().toString());
        }
        else if(rbtn_manual.isChecked())
        {
            model.setTransmissionType(rbtn_manual.getText().toString());
        }

        if(!spinner_fuel.getSelectedItem().toString().equals("Select"))
        {
            model.setFuel(spinner_fuel.getSelectedItem().toString());
        }

        model.setColor(selectedColorModel.getColorName());
        model.setEngine(Integer.parseInt(edt_engine.getText().toString()));

        if(rbtn_steering_auto.isChecked())
        {
            model.setSteeringType(rbtn_steering_auto.getText().toString());
        }
        else if(rbtn_steering_manual.isChecked())
        {
            model.setSteeringType(rbtn_steering_manual.getText().toString());
        }

        if(rbtn_steering_left.isChecked())
        {
            model.setSteeringPlace(rbtn_steering_left.getText().toString());
        }
        else if(rbtn_steering_right.isChecked())
        {
            model.setSteeringPlace(rbtn_steering_right.getText().toString());
        }

        model.setDetail(edt_detail.getText().toString());
        model.setActive(chk_active.isChecked());
        if(chk_is_brand_new.isChecked())
        {
            model.setIsBrandNew(1);
        }
        else
        {
            model.setIsBrandNew(0);
        }
        model.setDeal(chk_is_deal.isChecked());


        return model;
    }

    private void saveCar(CarModel model)
    {
        progressDialog.setMessage("Saving new record ......");
        progressDialog.show();
        callCar = service.addCar(model);
        callCar.enqueue(new Callback<CarModel>() {
            @Override
            public void onResponse(Call<CarModel> call, final Response<CarModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    new AlertDialog.Builder(CarFormActivity.this)
                            .setMessage("Successfully added new car. Please upload car photos.")
                            .setCancelable(false)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int whichButton) {

                                            finish();

                                            Intent intent = new Intent(CarFormActivity.this, CarPhotoGalleryActivity.class);
                                            intent.putExtra("id", response.body().getCarId());
                                            startActivity(intent);

                                        }
                                    }).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CarModel> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateCar(CarModel model)
    {
        progressDialog.setMessage("Updating record ....");
        progressDialog.show();
        callUpdateCar = service.updateCar(model);
        callUpdateCar.enqueue(new Callback<CarModel>() {
            @Override
            public void onResponse(Call<CarModel> call, Response<CarModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CarModel> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void selectValue(Spinner spinner, Object value)
    {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public String getFormattedMoney(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%.2f",d);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkWriteExternalStoragePermission() && checkReadExternalStoragePermission() && checkCameraPermission()) {
        } else {
            ActivityCompat.requestPermissions(CarFormActivity.this, new String[]{
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA}, 1);
        }
    }


    private boolean checkCameraPermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkReadExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private boolean checkWriteExternalStoragePermission() {

        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;


        }
    }

    private int isSaleOrRent()
    {
        if(rbtn_sale.isChecked())
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    private void getColorList(String colorName){

        carColorListAdapter.clear();

        ArrayList<ColorModel> tempList = new ArrayList<>();

        ColorModel black = new ColorModel();
        black.setColorCode("#000000");
        black.setColorName("Black");
        tempList.add(black);

        ColorModel white = new ColorModel();
        white.setColorCode("#ffffff");
        white.setColorName("White");
        tempList.add(white);

        ColorModel silver = new ColorModel();
        silver.setColorCode("#C0C0C0");
        silver.setColorName("Silver");
        tempList.add(silver);

        ColorModel grey = new ColorModel();
        grey.setColorCode("#808080");
        grey.setColorName("Grey");
        tempList.add(grey);

        ColorModel blue = new ColorModel();
        blue.setColorCode("#0000FF");
        blue.setColorName("Blue");
        tempList.add(blue);

        ColorModel red = new ColorModel();
        red.setColorCode("#FF0000");
        red.setColorName("Red");
        tempList.add(red);

        ColorModel brown = new ColorModel();
        brown.setColorCode("#A52A2A");
        brown.setColorName("Brown");
        tempList.add(brown);

        ColorModel green = new ColorModel();
        green.setColorCode("#008000");
        green.setColorName("Green");
        tempList.add(green);

        for (ColorModel model :
                tempList) {

            if(model.getColorName().equals(colorName))
            {
                selectedColorModel = model;
                model.setSelected(true);
            }
            else
            {
                model.setSelected(false);
            }

            carColorListAdapter.add(model);

        }


    }

    private void getCarTypeList(String carTypeName)
    {
        ArrayList<CartypeModel> tempList = new ArrayList<>();
        String[] categories = getResources().getStringArray(R.array.category);
        for(int i = 1; i<=(categories.length - 1); i++)
        {
            CartypeModel cartype = new CartypeModel();
            cartype.setTitle(categories[i]);
            cartype.setIcon("type_" + categories[i].toLowerCase().replace(" ", "_"));
            tempList.add(cartype);
        }

        for (CartypeModel model :
                tempList) {

            if(model.getTitle().equals(carTypeName))
            {
                selectedCarTypeModel = model;
                model.setSelected(true);
            }
            else
            {
                model.setSelected(false);
            }

            carTypeListAdapter.add(model);

        }
    }

    @Override
    public void select(int position, ColorModel model) {

        selectedColorModel = model;

        carColorListAdapter.replace(position, model);

        for (int i = 0; i < carColorListAdapter.getItemCount(); i++) {
            if (i != position) {
                ColorModel colorModel = (ColorModel) carColorListAdapter.getItemsList().get(i);
                colorModel.setSelected(false);
                carColorListAdapter.replace(i, colorModel);
            }
        }
        carColorListAdapter.notifyDataSetChanged();

    }

    @Override
    public void select(int position, CartypeModel model) {

        selectedCarTypeModel = model;

        carTypeListAdapter.replace(position, model);

        for (int i = 0; i < carTypeListAdapter.getItemCount(); i++) {
            if (i != position) {
                CartypeModel cartypeModel = (CartypeModel) carTypeListAdapter.getItemsList().get(i);
                cartypeModel.setSelected(false);
                carTypeListAdapter.replace(i, cartypeModel);
            }
        }
        carTypeListAdapter.notifyDataSetChanged();

    }
}
