package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.util.ArrayList;

public class CarHomeViewModel {

    private CarPhotoMemberModel carOfTheDay;
    private ArrayList<CarPhotoMemberModel> featuredCar;
    private ArrayList<CarPhotoMemberModel> premiumCar;
    private ArrayList<CarPhotoMemberModel> editorChoice;

    public CarPhotoMemberModel getCarOfTheDay() {
        return carOfTheDay;
    }

    public void setCarOfTheDay(CarPhotoMemberModel carOfTheDay) {
        this.carOfTheDay = carOfTheDay;
    }

    public ArrayList<CarPhotoMemberModel> getFeaturedCar() {
        return featuredCar;
    }

    public void setFeaturedCar(ArrayList<CarPhotoMemberModel> featuredCar) {
        this.featuredCar = featuredCar;
    }

    public ArrayList<CarPhotoMemberModel> getPremiumCar() {
        return premiumCar;
    }

    public void setPremiumCar(ArrayList<CarPhotoMemberModel> premiumCar) {
        this.premiumCar = premiumCar;
    }

    public ArrayList<CarPhotoMemberModel> getEditorChoice() {
        return editorChoice;
    }

    public void setEditorChoice(ArrayList<CarPhotoMemberModel> editorChoice) {
        this.editorChoice = editorChoice;
    }
}
