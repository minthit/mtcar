package com.minthittun.mtcar.model;

import com.minthittun.mtcar.RealEstateFormActivity;
import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class RealEstatePhotoModel implements Pageable, Serializable {

    private RealEstateModel realestate;
    private ArrayList<PhotoModel> photos;
    private boolean isSaved;
    private boolean isReact;
    private int reactCount;

    public RealEstateModel getRealestate() {
        return realestate;
    }

    public void setRealestate(RealEstateModel realestate) {
        this.realestate = realestate;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PhotoModel> photos) {
        this.photos = photos;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public boolean isReact() {
        return isReact;
    }

    public void setReact(boolean react) {
        isReact = react;
    }

    public int getReactCount() {
        return reactCount;
    }

    public void setReactCount(int reactCount) {
        this.reactCount = reactCount;
    }
}
