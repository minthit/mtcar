package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

public class CarBrandModel implements Pageable {

    private String logo;
    private String brandName;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
