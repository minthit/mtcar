package com.minthittun.mtcar.model;

/**
 * Created by hello on 11/19/17.
 */

public class SaveCarModel {

    private String SaveCarId;
    private String CarId;
    private String MemberId;
    private String Accesstime;

    public String getSaveCarId() {
        return SaveCarId;
    }

    public void setSaveCarId(String saveCarId) {
        SaveCarId = saveCarId;
    }

    public String getCarId() {
        return CarId;
    }

    public void setCarId(String carId) {
        CarId = carId;
    }

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }
}
