package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class RealestatePhotoModel2 implements Pageable, Serializable {

    private RealEstateModel realestate;
    private ArrayList<PhotoModel> photos;
    private String memberName;
    private String contact;


    public RealEstateModel getRealestate() {
        return realestate;
    }

    public void setRealestate(RealEstateModel realestate) {
        this.realestate = realestate;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PhotoModel> photos) {
        this.photos = photos;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
