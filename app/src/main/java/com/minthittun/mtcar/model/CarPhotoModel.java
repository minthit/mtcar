package com.minthittun.mtcar.model;

import android.provider.ContactsContract;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class CarPhotoModel implements Pageable, Serializable {

    private CarModel car;
    private ArrayList<PhotoModel> photos;
    private boolean isSaved;
    private boolean isReact;
    private int reactCount;

    public CarModel getCar() {
        return car;
    }

    public void setCar(CarModel car) {
        this.car = car;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PhotoModel> photos) {
        this.photos = photos;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public boolean isReact() {
        return isReact;
    }

    public void setReact(boolean react) {
        isReact = react;
    }

    public int getReactCount() {
        return reactCount;
    }

    public void setReactCount(int reactCount) {
        this.reactCount = reactCount;
    }
}
