package com.minthittun.mtcar.model;

/**
 * Created by hello on 12/13/17.
 */

public class ReactModel {

    private String ReactId;
    private String CarOrRealestateId;
    private String MemberId;
    private String Accesstime;
    private int Type;

    public String getReactId() {
        return ReactId;
    }

    public void setReactId(String reactId) {
        ReactId = reactId;
    }

    public String getCarOrRealestateId() {
        return CarOrRealestateId;
    }

    public void setCarOrRealestateId(String carOrRealestateId) {
        CarOrRealestateId = carOrRealestateId;
    }

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }
}
