package com.minthittun.mtcar.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hello on 5/15/17.
 */

public class LocationModel extends RealmObject {

    @PrimaryKey
    private int LocationID;
    private String StateDivision;
    private String StateDivision_Unicode;
    private int StateDivisionCode;
    private String Township;
    private String Township_Unicode;
    private int TownshipCode;
    private boolean Active;
    private String Accesstime;

    public int getLocationID() {
        return LocationID;
    }

    public void setLocationID(int locationID) {
        LocationID = locationID;
    }

    public String getStateDivision() {
        return StateDivision;
    }

    public void setStateDivision(String stateDivision) {
        StateDivision = stateDivision;
    }

    public String getStateDivision_Unicode() {
        return StateDivision_Unicode;
    }

    public void setStateDivision_Unicode(String stateDivision_Unicode) {
        StateDivision_Unicode = stateDivision_Unicode;
    }

    public int getStateDivisionCode() {
        return StateDivisionCode;
    }

    public void setStateDivisionCode(int stateDivisionCode) {
        StateDivisionCode = stateDivisionCode;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getTownship_Unicode() {
        return Township_Unicode;
    }

    public void setTownship_Unicode(String township_Unicode) {
        Township_Unicode = township_Unicode;
    }

    public int getTownshipCode() {
        return TownshipCode;
    }

    public void setTownshipCode(int townshipCode) {
        TownshipCode = townshipCode;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }
}
