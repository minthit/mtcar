package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

/**
 * Created by hello on 12/2/17.
 */

public class PhotoCarModel implements Pageable {

    private PhotoModel photoModel;
    private CarModel carModel;

    public PhotoModel getPhotoModel() {
        return photoModel;
    }

    public void setPhotoModel(PhotoModel photoModel) {
        this.photoModel = photoModel;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }
}
