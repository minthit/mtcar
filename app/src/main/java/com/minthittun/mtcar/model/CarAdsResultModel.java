package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.util.ArrayList;

/**
 * Created by hello on 1/31/18.
 */

public class CarAdsResultModel implements Pageable {

    private ArrayList<CarPhotoMemberAdsModel> data;
    private PaginationModel pagination;

    public ArrayList<CarPhotoMemberAdsModel> getData() {
        return data;
    }

    public void setData(ArrayList<CarPhotoMemberAdsModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }

}
