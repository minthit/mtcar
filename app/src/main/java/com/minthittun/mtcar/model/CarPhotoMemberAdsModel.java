package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;

/**
 * Created by hello on 1/31/18.
 */

public class CarPhotoMemberAdsModel implements Pageable, Serializable {

    private CarPhotoMemberModel car;
    private AdvertisementModel advertisement;
    private int type;

    public CarPhotoMemberModel getCar() {
        return car;
    }

    public void setCar(CarPhotoMemberModel car) {
        this.car = car;
    }

    public AdvertisementModel getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(AdvertisementModel advertisement) {
        this.advertisement = advertisement;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
