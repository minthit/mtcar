package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;

/**
 * Created by hello on 1/31/18.
 */

public class RealestatePhotoMemberAdsModel implements Pageable, Serializable {

    private RealestatePhotoMemberModel realestate;
    private AdvertisementModel advertisement;
    private int type;

    public RealestatePhotoMemberModel getRealestate() {
        return realestate;
    }

    public void setRealestate(RealestatePhotoMemberModel realestate) {
        this.realestate = realestate;
    }

    public AdvertisementModel getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(AdvertisementModel advertisement) {
        this.advertisement = advertisement;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
