package com.minthittun.mtcar.model;

/**
 * Created by hello on 11/19/17.
 */

public class SaveRealestateModel {

    private String SaveRealEstateId;
    private String RealestateId;
    private String MemberId;
    private String Accesstime;

    public String getSaveRealEstateId() {
        return SaveRealEstateId;
    }

    public void setSaveRealEstateId(String saveRealEstateId) {
        SaveRealEstateId = saveRealEstateId;
    }

    public String getRealestateId() {
        return RealestateId;
    }

    public void setRealestateId(String realestateId) {
        RealestateId = realestateId;
    }

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }
}
