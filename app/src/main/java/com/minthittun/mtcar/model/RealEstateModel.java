package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;

/**
 * Created by hello on 12/12/17.
 */

public class RealEstateModel implements Pageable, Serializable {

    private String RealEstateId;
    private String State;
    private String Township;
    private String Address;
    private double Price;
    private int Beds;
    private int Baths;
    private double HouseSize;
    private double LotSize;
    private String Accesstime;
    private boolean Active;
    private boolean IsDeal;
    private int IsAdminApproved;
    private int IsPremium;
    private String MemberId;
    private String PremiumDatetime;
    private String Detail;
    private int SaleOrRent;

    public String getRealEstateId() {
        return RealEstateId;
    }

    public void setRealEstateId(String realEstateId) {
        RealEstateId = realEstateId;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getBeds() {
        return Beds;
    }

    public void setBeds(int beds) {
        Beds = beds;
    }

    public int getBaths() {
        return Baths;
    }

    public void setBaths(int baths) {
        Baths = baths;
    }

    public double getHouseSize() {
        return HouseSize;
    }

    public void setHouseSize(double houseSize) {
        HouseSize = houseSize;
    }

    public double getLotSize() {
        return LotSize;
    }

    public void setLotSize(double lotSize) {
        LotSize = lotSize;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public boolean isDeal() {
        return IsDeal;
    }

    public void setDeal(boolean deal) {
        IsDeal = deal;
    }

    public int getIsAdminApproved() {
        return IsAdminApproved;
    }

    public void setIsAdminApproved(int isAdminApproved) {
        IsAdminApproved = isAdminApproved;
    }

    public int getIsPremium() {
        return IsPremium;
    }

    public void setIsPremium(int isPremium) {
        IsPremium = isPremium;
    }

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getPremiumDatetime() {
        return PremiumDatetime;
    }

    public void setPremiumDatetime(String premiumDatetime) {
        PremiumDatetime = premiumDatetime;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public int getSaleOrRent() {
        return SaleOrRent;
    }

    public void setSaleOrRent(int saleOrRent) {
        SaleOrRent = saleOrRent;
    }
}
