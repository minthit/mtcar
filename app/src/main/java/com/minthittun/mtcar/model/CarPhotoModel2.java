package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class CarPhotoModel2 implements Pageable, Serializable {

    private CarModel car;
    private ArrayList<PhotoModel> photos;
    private String memberId;
    private String memberName;
    private String memberType;
    private String authorizedDealerBrand;
    private String contact;

    public CarModel getCar() {
        return car;
    }

    public void setCar(CarModel car) {
        this.car = car;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PhotoModel> photos) {
        this.photos = photos;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getAuthorizedDealerBrand() {
        return authorizedDealerBrand;
    }

    public void setAuthorizedDealerBrand(String authorizedDealerBrand) {
        this.authorizedDealerBrand = authorizedDealerBrand;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
