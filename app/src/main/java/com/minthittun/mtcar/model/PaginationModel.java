package com.minthittun.mtcar.model;

/**
 * Created by hello on 11/6/16.
 */

public class PaginationModel {

    private int totalPage;
    private int totalCount;

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
