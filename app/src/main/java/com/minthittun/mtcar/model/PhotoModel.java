package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;

/**
 * Created by hello on 11/18/17.
 */

public class PhotoModel implements Pageable, Serializable {

    private String PhotoId;
    private String PhotoName;
    private String CarOrRealestateId;

    public String getPhotoId() {
        return PhotoId;
    }

    public void setPhotoId(String photoId) {
        PhotoId = photoId;
    }

    public String getPhotoName() {
        return PhotoName;
    }

    public void setPhotoName(String photoName) {
        PhotoName = photoName;
    }

    public String getCarOrRealestateId() {
        return CarOrRealestateId;
    }

    public void setCarOrRealestateId(String carOrRealestateId) {
        CarOrRealestateId = carOrRealestateId;
    }
}
