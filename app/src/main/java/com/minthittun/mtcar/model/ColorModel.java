package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

public class ColorModel implements Pageable {

    private String colorName;
    private String colorCode;
    private boolean isSelected;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
