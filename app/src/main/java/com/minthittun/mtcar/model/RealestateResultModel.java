package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class RealestateResultModel implements Pageable {

    private ArrayList<RealestatePhotoMemberModel> data;
    private PaginationModel pagination;

    public ArrayList<RealestatePhotoMemberModel> getData() {
        return data;
    }

    public void setData(ArrayList<RealestatePhotoMemberModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
