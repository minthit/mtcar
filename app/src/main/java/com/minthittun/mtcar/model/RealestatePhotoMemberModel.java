package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class RealestatePhotoMemberModel implements Pageable, Serializable {

    private RealEstateModel realestate;
    private ArrayList<PhotoModel> photos;
    private MemberModel member;
    private boolean isSaved;
    private int reactCount;
    private boolean isReact;

    public RealEstateModel getRealestate() {
        return realestate;
    }

    public void setRealestate(RealEstateModel realestate) {
        this.realestate = realestate;
    }

    public ArrayList<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PhotoModel> photos) {
        this.photos = photos;
    }

    public MemberModel getMember() {
        return member;
    }

    public void setMember(MemberModel member) {
        this.member = member;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public int getReactCount() {
        return reactCount;
    }

    public void setReactCount(int reactCount) {
        this.reactCount = reactCount;
    }

    public boolean isReact() {
        return isReact;
    }

    public void setReact(boolean react) {
        isReact = react;
    }

}
