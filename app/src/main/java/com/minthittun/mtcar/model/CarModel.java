package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.io.Serializable;

/**
 * Created by hello on 11/18/17.
 */

public class CarModel implements Pageable, Serializable {

    private String CarId;
    private String MemberId;
    private String ModelName;
    private String Category;
    private String Manufacture;
    private int Year;
    private double Kilometer;
    private String TransmissionType;
    private String Fuel;
    private String Color;
    private int Engine;
    private String Detail;
    private String SteeringPlace;
    private String CarNo;
    private String Accesstime;
    private String SteeringType;
    private boolean Active;
    private boolean IsDeal;
    private double Price;
    private String Grade;
    private String LicenseType;
    private String CurrentLocation;
    private String InsuranceCompany;
    private int InsuranceRemain;
    private int IsPremium;
    private int IsAdminApproved;
    private String PremiumDatetime;
    private int SaleOrRent;
    private int IsBrandNew;


    public String getCarId() {
        return CarId;
    }

    public void setCarId(String carId) {
        CarId = carId;
    }

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getManufacture() {
        return Manufacture;
    }

    public void setManufacture(String manufacture) {
        Manufacture = manufacture;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public double getKilometer() {
        return Kilometer;
    }

    public void setKilometer(double kilometer) {
        Kilometer = kilometer;
    }

    public String getTransmissionType() {
        return TransmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        TransmissionType = transmissionType;
    }

    public String getFuel() {
        return Fuel;
    }

    public void setFuel(String fuel) {
        Fuel = fuel;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getEngine() {
        return Engine;
    }

    public void setEngine(int engine) {
        Engine = engine;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getSteeringPlace() {
        return SteeringPlace;
    }

    public void setSteeringPlace(String steeringPlace) {
        SteeringPlace = steeringPlace;
    }

    public String getCarNo() {
        return CarNo;
    }

    public void setCarNo(String carNo) {
        CarNo = carNo;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public String getSteeringType() {
        return SteeringType;
    }

    public void setSteeringType(String steeringType) {
        SteeringType = steeringType;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public boolean isDeal() {
        return IsDeal;
    }

    public void setDeal(boolean deal) {
        IsDeal = deal;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getLicenseType() {
        return LicenseType;
    }

    public void setLicenseType(String licenseType) {
        LicenseType = licenseType;
    }

    public String getCurrentLocation() {
        return CurrentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        CurrentLocation = currentLocation;
    }

    public String getInsuranceCompany() {
        return InsuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        InsuranceCompany = insuranceCompany;
    }

    public int getInsuranceRemain() {
        return InsuranceRemain;
    }

    public void setInsuranceRemain(int insuranceRemain) {
        InsuranceRemain = insuranceRemain;
    }

    public int getIsPremium() {
        return IsPremium;
    }

    public void setIsPremium(int isPremium) {
        IsPremium = isPremium;
    }

    public int getIsAdminApproved() {
        return IsAdminApproved;
    }

    public void setIsAdminApproved(int isAdminApproved) {
        IsAdminApproved = isAdminApproved;
    }

    public String getPremiumDatetime() {
        return PremiumDatetime;
    }

    public void setPremiumDatetime(String premiumDatetime) {
        PremiumDatetime = premiumDatetime;
    }

    public int getSaleOrRent() {
        return SaleOrRent;
    }

    public void setSaleOrRent(int saleOrRent) {
        SaleOrRent = saleOrRent;
    }

    public int getIsBrandNew() {
        return IsBrandNew;
    }

    public void setIsBrandNew(int isBrandNew) {
        IsBrandNew = isBrandNew;
    }
}
