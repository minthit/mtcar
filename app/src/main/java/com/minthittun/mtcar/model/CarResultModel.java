package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.util.ArrayList;

/**
 * Created by hello on 11/19/17.
 */

public class CarResultModel implements Pageable {

    private ArrayList<CarPhotoMemberModel> data;
    private PaginationModel pagination;

    public ArrayList<CarPhotoMemberModel> getData() {
        return data;
    }

    public void setData(ArrayList<CarPhotoMemberModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }

}
