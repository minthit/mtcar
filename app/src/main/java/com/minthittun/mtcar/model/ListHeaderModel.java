package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

public class ListHeaderModel implements Pageable {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
