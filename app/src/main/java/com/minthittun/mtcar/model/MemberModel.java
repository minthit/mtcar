package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hello on 11/18/17.
 */

public class MemberModel extends RealmObject implements Pageable {

    @PrimaryKey
    private String MemberId;
    private String Name;
    private String Type;
    private String Photo;
    private String Phone;
    private boolean Active;
    private String Description;
    private boolean IsApproved;
    private String AuthorizedDealerBrand;
    private String AuthorizedDealerAddress;
    private String AuthorizedDealerContact;
    private String AuthorizedDealerEmail;

    public String getMemberId() {
        return MemberId;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public boolean isApproved() {
        return IsApproved;
    }

    public void setApproved(boolean approved) {
        IsApproved = approved;
    }

    public String getAuthorizedDealerBrand() {
        return AuthorizedDealerBrand;
    }

    public void setAuthorizedDealerBrand(String authorizedDealerBrand) {
        AuthorizedDealerBrand = authorizedDealerBrand;
    }

    public String getAuthorizedDealerAddress() {
        return AuthorizedDealerAddress;
    }

    public void setAuthorizedDealerAddress(String authorizedDealerAddress) {
        AuthorizedDealerAddress = authorizedDealerAddress;
    }

    public String getAuthorizedDealerContact() {
        return AuthorizedDealerContact;
    }

    public void setAuthorizedDealerContact(String authorizedDealerContact) {
        AuthorizedDealerContact = authorizedDealerContact;
    }

    public String getAuthorizedDealerEmail() {
        return AuthorizedDealerEmail;
    }

    public void setAuthorizedDealerEmail(String authorizedDealerEmail) {
        AuthorizedDealerEmail = authorizedDealerEmail;
    }
}
