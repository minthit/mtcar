package com.minthittun.mtcar.model;

import com.minthittun.mtcar.common.Pageable;

import java.util.ArrayList;

/**
 * Created by hello on 1/31/18.
 */

public class RealestateAdsResultModel implements Pageable {

    private ArrayList<RealestatePhotoMemberAdsModel> data;
    private PaginationModel pagination;

    public ArrayList<RealestatePhotoMemberAdsModel> getData() {
        return data;
    }

    public void setData(ArrayList<RealestatePhotoMemberAdsModel> data) {
        this.data = data;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }

}
