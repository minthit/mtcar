package com.minthittun.mtcar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class PhotoDetailActivity extends BaseActivity {

    @BindView(R.id.imgv_detail)
    PhotoView imgv_detail;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_photo_detail;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));

        Picasso.with(PhotoDetailActivity.this)
                .load(getIntent().getStringExtra("photo"))
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerInside()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_detail);

    }

    private void init()
    {
        setupToolbar(true);
        getSupportActionBar().hide();
    }

}
