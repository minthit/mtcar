package com.minthittun.mtcar;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.adapter.OnBoardingViewPagerAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;

import butterknife.BindView;

public class OnBoardingViewActivity extends BaseActivity {

    @BindView(R.id.view_pager)
    ViewPager view_pager;

    @BindView(R.id.imgv_close)
    ImageView imgv_close;

    @BindView(R.id.scroll_view)
    HorizontalScrollView scroll_view;

    @BindView(R.id.background)
    ImageView background;

    @BindView(R.id.tv_foot)
    MyanTextView tv_foot;

    @BindView(R.id.btn_continue)
    Button btn_continue;

    private SharePreferenceHelper sharePreferenceHelper;

    private OnBoardingViewPagerAdapter onBoardingViewPagerAdapter;

    private int current_page = 0;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_on_boarding_view;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        try {
            tv_foot.setMyanmarText("Version " + getVersionName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int x = (int) ((view_pager.getWidth() * position + positionOffsetPixels) * computeFactor());
                scroll_view.scrollTo(x, 0);
            }

            @Override
            public void onPageSelected(int position) {
                current_page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            private float computeFactor() {
                return (background.getWidth() - view_pager.getWidth()) /
                        (float)(view_pager.getWidth() * (view_pager.getAdapter().getCount() - 1));
            }
        });

        imgv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OnBoardingViewActivity.this, MainAppActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(OnBoardingViewActivity.this);

            }
        });

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(current_page < 3)
                {
                    current_page ++;
                    view_pager.setCurrentItem(current_page);
                }
                else
                {
                    sharePreferenceHelper.setTutorialShown();

                    Intent intent = new Intent(OnBoardingViewActivity.this, MainAppActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    }
                    startActivity(intent);
                    ActivityAnimationHelper.setFadeInOutAnimation(OnBoardingViewActivity.this);
                }

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText(" ");
        getSupportActionBar().hide();

        sharePreferenceHelper = new SharePreferenceHelper(this);

        view_pager.setOffscreenPageLimit(4);
        onBoardingViewPagerAdapter = new OnBoardingViewPagerAdapter(getSupportFragmentManager(), this);
        view_pager.setAdapter(onBoardingViewPagerAdapter);
    }

    private String getVersionName() throws PackageManager.NameNotFoundException {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                this.getPackageName(), 0);

        return info.versionName;
    }

}
