package com.minthittun.mtcar.helper;

import android.net.ParseException;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hello on 6/1/17.
 */

public class ShowDatetimeHelper {

    private MyDateFormat myDateFormat;

    public ShowDatetimeHelper()
    {
        myDateFormat = new MyDateFormat();
    }

    public String getDatetime(String datetime) throws java.text.ParseException {
        long dt = myDateFormat.DATE_FORMAT_YMD_HMS.parse(datetime.replace("T", " ")).getTime();
        long dt_now = myDateFormat.DATE_FORMAT_YMD_HMS.parse(myDateFormat.DATE_FORMAT_YMD_HMS.format(Calendar.getInstance().getTime())).getTime();
        CharSequence result = DateUtils.getRelativeTimeSpanString(dt, dt_now, DateUtils.MINUTE_IN_MILLIS);

        return result.toString();
    }


}
