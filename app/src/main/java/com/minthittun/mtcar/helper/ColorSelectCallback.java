package com.minthittun.mtcar.helper;

import com.minthittun.mtcar.model.ColorModel;

/**
 * Created by hello on 12/4/17.
 */

public interface ColorSelectCallback {

    void select(int position, ColorModel model);

}
