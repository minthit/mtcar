package com.minthittun.mtcar.helper;

import java.text.SimpleDateFormat;

public class MyDateFormat {

	public SimpleDateFormat DATE_FORMAT_YMD;
    public SimpleDateFormat DATE_FORMAT_YMD_TEXT;
    public SimpleDateFormat DATE_FORMAT_YMD_HMS;
    public SimpleDateFormat DATE_FORMAT_DMY;
    public SimpleDateFormat DATE_FORMAT_DMY_HMS_Z;

	private String result;
	
	public MyDateFormat()
	{
        DATE_FORMAT_YMD = new SimpleDateFormat("yyyy-MM-dd");
        DATE_FORMAT_YMD_TEXT = new SimpleDateFormat("dd MMMM, yyyy");
        DATE_FORMAT_YMD_HMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DATE_FORMAT_DMY = new SimpleDateFormat("dd/MM/yyyy");
        DATE_FORMAT_DMY_HMS_Z = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
	}
    public String removeTfromServerDate(String datetime)
    {
        return  datetime.replace("T", " ");
    }
}
