package com.minthittun.mtcar.helper;

/**
 * Created by hello on 8/31/17.
 */

public class MtCarConstant {

    //http://192.168.99.18:8000/api/
    //http://mycardb.azurewebsites.net/api/

    public static final String BASE_URL = "https://mycarmobileapi.azurewebsites.net/api/";
    public static final String BASE_PHOTO_URL = "https://mycarmobileapi.azurewebsites.net/Content/UploadPhotos/";
    public static final String BASE_ADS_PHOTO_URL = "https://mycarmobileapi.azurewebsites.net/Content/AdsPhotos/";

    public static final int MAX_WIDTH = 1024;
    public static final int MAX_HEIGHT = 768;

}