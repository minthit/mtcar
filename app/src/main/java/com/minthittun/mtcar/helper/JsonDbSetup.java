package com.minthittun.mtcar.helper;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.minthittun.mtcar.model.LocationModel;

import java.io.IOException;
import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by HELLO on 6/23/2016.
 */
public class JsonDbSetup {

    private Context context;
    private Realm realm;

    public JsonDbSetup(Context context)
    {
        this.context = context;
        realm = Realm.getDefaultInstance();
    }

    public boolean isDbsetupDone()
    {
        long count = realm.where(LocationModel.class).count();
        if(count != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void locationDataSetup() throws IOException {
        Gson gson = new Gson();
        ArrayList<LocationModel> locationArrayList = gson.fromJson(JsonFileHelper.getLocationFromJson(context), new TypeToken<ArrayList<LocationModel>>(){}.getType());

        for(int i = 0; i<locationArrayList.size(); i++)
        {
            LocationModel locationModel = locationArrayList.get(i);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(locationModel);
            realm.commitTransaction();
        }
    }


}
