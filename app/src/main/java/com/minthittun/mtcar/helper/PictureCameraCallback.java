package com.minthittun.mtcar.helper;

/**
 * Created by hello on 2/23/17.
 */

public interface PictureCameraCallback {

    void getFilePath(String name, String path);

}
