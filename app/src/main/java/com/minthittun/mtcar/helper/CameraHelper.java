package com.minthittun.mtcar.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by minthit on 3/17/2015.
 */
public class CameraHelper {

    private PictureCameraCallback pictureCameraCallback;
    private PictureCameraGalleryCallback pictureCameraGalleryCallback;
    public static String TEMP_FOLDER_NAME = "MtCarTemp", FOLDER_NAME = "MtCar";
    static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
    Context context;
    private SharePreferenceHelper sharePreferenceHelper;

    public void registerCallback(PictureCameraCallback pictureCameraCallback)
    {
        this.pictureCameraCallback = pictureCameraCallback;
    }

    public void registerGalleryCallback(PictureCameraGalleryCallback pictureCameraGalleryCallback)
    {
        this.pictureCameraGalleryCallback = pictureCameraGalleryCallback;
    }

    public CameraHelper(Context context) {
        this.context = context;
        sharePreferenceHelper = new SharePreferenceHelper(context);
    }

    public String getDateTImeStampForFileName() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

        String result = simpleDateFormat.format(Calendar.getInstance().getTime());

        return result;

    }

    public double getDegree(int orientation) {
        double degree = 0;

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            degree = 90;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            degree = 180;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            degree = 270;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
            degree = 0.2;
            //Toast.makeText(context, "Undefined Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_NORMAL) {
            degree = 0.2;
            //Toast.makeText(context, "Normal Degree = " + degree, Toast.LENGTH_SHORT).show();
        }
        return degree;
    }

    public void deleteFile(String selectedFilePath) {
        File file = new File(selectedFilePath);
        boolean deleted = file.delete();

        if (deleted) {
            sharePreferenceHelper.setTempPhotoName("");
            sharePreferenceHelper.setTempPhotoPath("");
        }

    }


    public void saveMemberProfilePicture(Bitmap newBitmap) {
        try {
            File pictureDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FOLDER_NAME);

            if (!pictureDirectory.exists()) {
                pictureDirectory.mkdirs();
            }

            File file = new File(pictureDirectory.getAbsolutePath() + File.separator + getDateTImeStampForFileName() + ".jpg");
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(file));

            //uploadBusinessMultipart(context, Uri.fromFile(file), regno, regioncode);
            pictureCameraCallback.getFilePath(file.getName(), file.getPath());


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void saveGalleryPhoto(Bitmap newBitmap) {
        try {
            File pictureDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FOLDER_NAME);

            if (!pictureDirectory.exists()) {
                pictureDirectory.mkdirs();
            }

            File file = new File(pictureDirectory.getAbsolutePath() + File.separator + getDateTImeStampForFileName() + ".jpg");
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(file));

            //uploadBusinessMultipart(context, Uri.fromFile(file), regno, regioncode);
            pictureCameraGalleryCallback.getFilePath(file.getPath());


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public Bitmap rotateBitmap(Bitmap original, String picturePath) {

        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(picturePath);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

        Matrix matrix = new Matrix();
        matrix.setRotate((float) getDegree(orientation));

        Bitmap editedBitmap = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);


        return editedBitmap;

    }

}