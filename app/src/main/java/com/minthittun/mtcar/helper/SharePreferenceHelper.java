package com.minthittun.mtcar.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceHelper {

	private SharedPreferences sharedPreference;

	private static String SHARE_PREFRENCE = "mtCarPref";

	private static String MEMBER_PHONE_KEY = "member_phone";
	private static String MEMBER_ID_KEY = "member_id";
	private static String PHOTO_NAME_TEMP_KEY = "photo_name";
	private static String PHOTO_PATH_TEMP_KEY = "photo_path";
	private static String TUTORIAL = "tutorial";

		
	public SharePreferenceHelper(Context context)
	{
		sharedPreference = context.getSharedPreferences(SHARE_PREFRENCE, Context.MODE_PRIVATE);
	}

	public void setTempPhotoName(String photoName)
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(PHOTO_NAME_TEMP_KEY, photoName);
		editor.commit();
	}

	public void setTempPhotoPath(String photoPath)
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(PHOTO_PATH_TEMP_KEY, photoPath);
		editor.commit();
	}

	public String getTempPhotoName()
	{
		return sharedPreference.getString(PHOTO_NAME_TEMP_KEY, "");
	}

	public String getTempPhotoPath()
	{
		return sharedPreference.getString(PHOTO_PATH_TEMP_KEY, "");
	}

	//***** Member Login *****//
	public void setLoginPhoneSharePreference(String phone)
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(MEMBER_PHONE_KEY, phone);
		editor.commit();
	}

	public void setLoginIdSharePreference(String id)
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(MEMBER_ID_KEY, id);
		editor.commit();
	}

	public void logoutSharePreference()
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.clear();
		editor.commit();
	}


	public String getLoginMemberId()
	{
		return sharedPreference.getString(MEMBER_ID_KEY, "");
	}

	public String getLoginMemberPhone()
	{
		return sharedPreference.getString(MEMBER_PHONE_KEY, "");
	}


	public boolean isLogin()
	{
		if(sharedPreference.contains(MEMBER_PHONE_KEY))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//***** Member Login *****//

	//Tutorial
	public void setTutorialShown()
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(TUTORIAL, "shown");
		editor.commit();
	}

	public boolean isTutorialShown()
	{
		if(sharedPreference.contains(TUTORIAL))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
