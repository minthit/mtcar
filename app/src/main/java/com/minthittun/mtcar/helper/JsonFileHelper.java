package com.minthittun.mtcar.helper;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by minthittun on 1/28/2016.
 */
public class JsonFileHelper {

    public static String getLocationFromJson(Context context) throws IOException {

        InputStream is = context.getAssets().open("location.json");

        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        String json = new String(buffer, "UTF-8");
        Log.d("JsonFileHelpter", json);
        return json;

    }


}
