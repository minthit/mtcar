package com.minthittun.mtcar.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import com.minthittun.mtcar.R;

/**
 * Created by hello on 10/2/17.
 */

public class CustomDialogHelper {

    private Dialog dialog;
    private TextView tv_title;
    private TextView tv_message;
    private TextView tv_cancel;
    private TextView tv_click_here;

    public CustomDialogHelper(Context context, boolean setCancelable, boolean isConfirmDialog)
    {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(setCancelable);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        tv_click_here = (TextView) dialog.findViewById(R.id.tv_click_here);

        if(isConfirmDialog)
        {
            tv_cancel.setVisibility(View.VISIBLE);
        }
        else
        {
            tv_cancel.setVisibility(View.GONE);
        }
    }

    public void showDialog(String title, String message, String buttonText)
    {
        tv_title.setText(title);
        tv_message.setText(message);
        tv_click_here.setText(buttonText);
        dialog.show();

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });

    }

    public void hideDialog()
    {
        dialog.dismiss();
    }

    public TextView getTextView()
    {
        return tv_click_here;
    }

}
