package com.minthittun.mtcar.helper;

import android.content.Context;

import com.minthittun.mtcar.model.CarAdsResultModel;
import com.minthittun.mtcar.model.CarHomeViewModel;
import com.minthittun.mtcar.model.CarModel;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.CarResultModel;
import com.minthittun.mtcar.model.MemberModel;
import com.minthittun.mtcar.model.PhotoModel;
import com.minthittun.mtcar.model.ReactModel;
import com.minthittun.mtcar.model.RealEstateModel;
import com.minthittun.mtcar.model.RealEstatePhotoModel;
import com.minthittun.mtcar.model.RealestateAdsResultModel;
import com.minthittun.mtcar.model.RealestatePhotoMemberModel;
import com.minthittun.mtcar.model.RealestateResultModel;
import com.minthittun.mtcar.model.SaveCarModel;
import com.minthittun.mtcar.model.SaveRealestateModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import static com.minthittun.mtcar.helper.MtCarConstant.BASE_URL;

/**
 * Created by minthittun on 12/30/2015.
 */
public class ServiceHelper {


    private static ApiService apiService;
    private static Cache cache;

    public static ApiService getClient(final Context context) {
        if (apiService == null) {

            Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    int maxAge = 300; // read from cache for 5 minute
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, max-age=" + maxAge)
                            .build();
                }
            };

            //setup cache
            File httpCacheDirectory = new File(context.getCacheDir(), "responses");
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            cache = new Cache(httpCacheDirectory, cacheSize);

            final OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
            okClientBuilder.addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR);
            okClientBuilder.readTimeout(60, TimeUnit.SECONDS);
            okClientBuilder.connectTimeout(60, TimeUnit.SECONDS);
            okClientBuilder.cache(cache);

            OkHttpClient okClient = okClientBuilder.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService = client.create(ApiService.class);
        }
        return apiService;
    }

    public static void removeFromCache(String url) {
        try {
            Iterator<String> it = cache.urls();
            while (it.hasNext()) {
                String next = it.next();
                if (next.contains(BASE_URL + url)) {
                    it.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public interface ApiService {

        @GET("member/getMemberById")
        Call<MemberModel> getMemberById(@Query("memberId") String memberId);

        @GET("member/getAuthorizedDealerListByBrand")
        Call<ArrayList<MemberModel>> getAuthorizedDealerListByBrand(@Query("brandName") String brandName);

        @GET("member/getAuthorizedDealerAndShowRoom")
        Call<ArrayList<MemberModel>> getAuthorizedDealerAndShowRoom();

        @POST("member/memberlogin")
        Call<MemberModel> memberlogin(@Body MemberModel model);

        @POST("member/memberProfileUpdate")
        Call<MemberModel> memberProfileUpdate(@Body MemberModel model);

        @GET("car/getCarDiscover")
        Call<CarHomeViewModel> getCarDiscover(@Query("memberId") String memberId);

        @POST("car/addCar")
        Call<CarModel> addCar(@Body CarModel model);

        @POST("car/updateCar")
        Call<CarModel> updateCar(@Body CarModel model);

        @GET("car/getCarPhotoList")
        Call<ArrayList<PhotoModel>> getCarPhotoList(@Query("carId") String carId);

        @GET("car/getCarListByMemberId")
        Call<ArrayList<CarPhotoModel>> getCarListByMemberId(@Query("memberId") String memberId);

        @GET("car/getSaveCarListByMemberId")
        Call<ArrayList<CarPhotoModel>> getSaveCarListByMemberId(@Query("memberId") String memberId);

        @GET("car/getAllCars")
        Call<CarResultModel> getAllCars(@Query("page") int page, @Query("memberId") String memberId);

        @GET("car/getAllCarsv2")
        Call<CarAdsResultModel> getAllCarsv2(
                @Query("page") int page,
                @Query("memberId") String memberId,
                @Query("featuredCar") int featuredCar,
                @Query("premiumCar") int premiumCar,
                @Query("editorChoiceCar") boolean editorChoiceCar,
                @Query("category") String category,
                @Query("brand") String brand);

        @GET("car/saveCar")
        Call<SaveCarModel> saveCar(@Query("carId") String carId, @Query("memberId") String memberId);

        @GET("car/deletePhotoById")
        Call<Integer> deletePhotoById(@Query("photoId") String photoId);

        @GET("car/saveReact")
        Call<ReactModel> saveReact(@Query("carId") String carId, @Query("memberId") String memberId);

        @GET("car/searchCar")
        Call<ArrayList<CarPhotoMemberModel>> searchCar(@Query("memberId") String memberId,
                                                       @Query("keyword") String keyword,
                                                       @Query("manufacture") String manufacture,
                                                       @Query("category") String category,
                                                       @Query("minPrice") float minPrice,
                                                       @Query("maxPrice") float maxPrice,
                                                       @Query("minYear") int minYear,
                                                       @Query("maxYear") int maxYear);

        //RE
        @POST("realestate/addRealEstate")
        Call<RealEstateModel> addRealEstate(@Body RealEstateModel model);

        @POST("realestate/updateRealEstate")
        Call<RealEstateModel> updateRealEstate(@Body RealEstateModel model);

        @GET("realestate/getRealEstatePhotoList")
        Call<ArrayList<PhotoModel>> getRealEstatePhotoList(@Query("realEstateId") String realEstateId);

        @GET("realestate/deletePhotoByReId")
        Call<Integer> deletePhotoByReId(@Query("photoId") String photoId);

        @GET("realestate/getAllRealestatesv2")
        Call<RealestateAdsResultModel> getAllRealestatesv2(@Query("page") int page, @Query("memberId") String memberId);

        @GET("realestate/saveRealestate")
        Call<SaveRealestateModel> saveRealestate(@Query("realEstateId") String realEstateId, @Query("memberId") String memberId);

        @GET("realestate/getRealEstateListByMemberId")
        Call<ArrayList<RealEstatePhotoModel>> getRealEstateListByMemberId(@Query("memberId") String memberId);

        @GET("realestate/getSaveRealestateListByMemberId")
        Call<ArrayList<RealEstatePhotoModel>> getSaveRealestateListByMemberId(@Query("memberId") String memberId);


        @GET("realestate/searchRealestate")
        Call<ArrayList<RealestatePhotoMemberModel>> searchRealestate(@Query("memberId") String memberId,
                                                                     @Query("keyword") String keyword,
                                                                     @Query("state") String state,
                                                                     @Query("township") String township,
                                                                     @Query("minPrice") float minPrice,
                                                                     @Query("maxPrice") float maxPrice);



        @GET("realestate/saveReact")
        Call<ReactModel> saveRealestateReact(@Query("realestateId") String realestateId, @Query("memberId") String memberId);

    }

}
