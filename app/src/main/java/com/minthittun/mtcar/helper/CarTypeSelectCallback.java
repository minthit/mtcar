package com.minthittun.mtcar.helper;

import com.minthittun.mtcar.model.CartypeModel;

/**
 * Created by hello on 12/4/17.
 */

public interface CarTypeSelectCallback {

    void select(int position, CartypeModel model);

}
