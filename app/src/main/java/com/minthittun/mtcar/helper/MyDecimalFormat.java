package com.minthittun.mtcar.helper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by HELLO on 6/16/2016.
 * Removed warning code by AAM on 29-Jun-17
 */
public class MyDecimalFormat {

    private DecimalFormat decimalFormat;

    public MyDecimalFormat() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        decimalFormat = new DecimalFormat();

        symbols.setGroupingSeparator(',');
        decimalFormat.setGroupingSize(3);
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setDecimalFormatSymbols(symbols);
    }

    public String getCurrencyFormat(int money) {
        return decimalFormat.format(money);
    }

    public String getCurrencyFormat(float money) {
        return decimalFormat.format(money);
    }

    public String getCurrencyFormat(double money) {
        return decimalFormat.format(money);
    }


}
