package com.minthittun.mtcar.helper;


import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmSchema;

/**
 * Created by hello on 12/19/17.
 */

public class MyRealmMigration implements io.realm.RealmMigration {



    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if(schema.get("MemberModel").hasField("IsRecommanded"))
        {
            schema.get("MemberModel").removeField("IsRecommanded");
        }

        if(schema.get("MemberModel").hasField("RecommandedBroker"))
        {
            schema.get("MemberModel").removeField("RecommandedBroker");
        }


        if(schema.get("MemberModel").hasField("AuthorizedDealerBrand") != true)
        {
            schema.get("MemberModel").addField("AuthorizedDealerBrand", String.class);
        }

        if(schema.get("MemberModel").hasField("AuthorizedDealerAddress") != true)
        {
            schema.get("MemberModel").addField("AuthorizedDealerAddress", String.class);
        }

        if(schema.get("MemberModel").hasField("AuthorizedDealerContact") != true)
        {
            schema.get("MemberModel").addField("AuthorizedDealerContact", String.class);
        }

        if(schema.get("MemberModel").hasField("AuthorizedDealerEmail") != true)
        {
            schema.get("MemberModel").addField("AuthorizedDealerEmail", String.class);
        }

        if(schema.get("MemberModel").hasField("IsApproved") != true)
        {
            schema.get("MemberModel").addField("IsApproved", Boolean.class).setRequired("IsApproved", true);
        }

        oldVersion++;

    }

    private boolean isSchemaExist(RealmSchema schema, String schemaName)
    {
        if(schema.get(schemaName) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
