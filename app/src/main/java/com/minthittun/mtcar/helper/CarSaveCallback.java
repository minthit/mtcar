package com.minthittun.mtcar.helper;

/**
 * Created by hello on 11/19/17.
 */

public interface CarSaveCallback {

    void saveCar(String carId);

}
