package com.minthittun.mtcar;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.MemberModel;

import java.lang.reflect.Member;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {


    public static int APP_REQUEST_CODE = 99;
    private String PHONE_NO = "";

    @BindView(R.id.btn_login)
    Button btn_login;

    @BindView(R.id.tv_version)
    MyanTextView tv_version;

    private ServiceHelper.ApiService service;
    private Call<MemberModel> memberLogin;
    private ProgressDialog progressDialog;
    private SharePreferenceHelper sharePreferenceHelper;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        try {
            tv_version.setMyanmarText("Version " + getVersionName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onLoginPhone();

            }
        });

    }

    private void init()
    {
        setupToolbar(false);
        getSupportActionBar().hide();

        service = ServiceHelper.getClient(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.setCancelable(false);
        sharePreferenceHelper = new SharePreferenceHelper(this);
    }

    public void onLoginPhone() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setReceiveSMS(true);
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == APP_REQUEST_CODE) {
            final AccountKitLoginResult loginResult =
                    data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            final String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                //showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String authorizationCode = loginResult.getAuthorizationCode();
                final long tokenRefreshIntervalInSeconds =
                        loginResult.getTokenRefreshIntervalInSeconds();
                if (accessToken != null) {
                    toastMessage = "Success:" + accessToken.getAccountId()
                            + tokenRefreshIntervalInSeconds;

                    //showHelloActivity(loginResult.getFinalAuthorizationState());

                    getAccountInformation();

                } else if (authorizationCode != null) {
                    toastMessage = String.format(
                            "Success:%s...",
                            authorizationCode.substring(0, 10));

                    //showHelloActivity(authorizationCode, loginResult.getFinalAuthorizationState());


                    getAccountInformation();

                } else {
                    toastMessage = "Unknown response type";
                }
            }

            /*Toast.makeText(
                    this,
                    toastMessage,
                    Toast.LENGTH_LONG)
                    .show();*/
        }

    }

    private void getAccountInformation()
    {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();

                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                PHONE_NO = phoneNumber.toString();

                //Sync with local db
                if(PHONE_NO.contains("+"))
                {
                    memberLogin(PHONE_NO.replace("+", ""));
                }
                else
                {
                    memberLogin(PHONE_NO);
                }

            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
                Toast.makeText(LoginActivity.this, "Please try again !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void memberLogin(final String memberPhone)
    {
        progressDialog.show();

        MemberModel model = new MemberModel();
        model.setPhone(memberPhone);

        memberLogin = service.memberlogin(model);
        memberLogin.enqueue(new Callback<MemberModel>() {
            @Override
            public void onResponse(Call<MemberModel> call, Response<MemberModel> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    MemberModel memberModel = response.body();
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(memberModel);
                    realm.commitTransaction();

                    sharePreferenceHelper.setLoginPhoneSharePreference(memberModel.getPhone());
                    sharePreferenceHelper.setLoginIdSharePreference(memberModel.getMemberId());

                    if(sharePreferenceHelper.isTutorialShown())
                    {
                        Intent intent = new Intent(LoginActivity.this, MainAppActivity.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        }
                        startActivity(intent);
                        ActivityAnimationHelper.setFadeInOutAnimation(LoginActivity.this);
                    }
                    else
                    {
                        Intent intent = new Intent(LoginActivity.this, OnBoardingViewActivity.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        }
                        startActivity(intent);
                        ActivityAnimationHelper.setFadeInOutAnimation(LoginActivity.this);
                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(), response.message().toString(), Toast.LENGTH_SHORT).show();
                    showAlertDialog();
                }

            }

            @Override
            public void onFailure(Call<MemberModel> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                showAlertDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkPermissionReadPhoneState() && checkPermissionSms())
        {
        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.RECEIVE_SMS}, 1);
        }

    }

    private boolean checkPermissionReadPhoneState(){

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;


        }
    }

    private boolean checkPermissionSms(){

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;


        }
    }

    private void showAlertDialog()
    {
        new AlertDialog.Builder(this)
                .setMessage("Internet connection problem!")
                .setCancelable(false)
                .setPositiveButton(
                        "Retry",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {

                                if(PHONE_NO.contains("+"))
                                {
                                    memberLogin(PHONE_NO.replace("+", ""));
                                }
                                else
                                {
                                    memberLogin(PHONE_NO);
                                }
                                dialog.dismiss();

                            }
                        }).show();
    }

    private String getVersionName() throws PackageManager.NameNotFoundException {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                this.getPackageName(), 0);

        return info.versionName;
    }

}
