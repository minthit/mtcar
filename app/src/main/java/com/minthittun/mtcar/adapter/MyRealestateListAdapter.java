package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.RealEstateFormActivity;
import com.minthittun.mtcar.RealestateDetailActivity;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.RealestateSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.ReactModel;
import com.minthittun.mtcar.model.RealEstatePhotoModel;
import com.minthittun.mtcar.model.RealestatePhotoModel2;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class MyRealestateListAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;
    private boolean isMe = false;
    private String memberName = "", memberPhone = "";
    private RealestateSaveCallback realestateSaveCallback;
    private ServiceHelper.ApiService service;
    private SharePreferenceHelper sharePreferenceHelper;
    private Call<ReactModel> callReact;

    public void registerCallback(RealestateSaveCallback realestateSaveCallback)
    {
        this.realestateSaveCallback = realestateSaveCallback;
    }

    public MyRealestateListAdapter(boolean isMe, String memberName, String memberPhone, Context context)
    {
        sharePreferenceHelper = new SharePreferenceHelper(context);
        service = ServiceHelper.getClient(context);
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
        this.isMe = isMe;
        this.memberName = memberName;
        this.memberPhone = memberPhone;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_realestate, parent, false);
        return new MyRealestateListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyRealestateListAdapter.ViewHolder) holder).bindPost((RealEstatePhotoModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_address)
        TextView tv_address;

        @BindView(R.id.tv_state_township)
        TextView tv_state_township;

        @BindView(R.id.tv_datetime)
        TextView tv_datetime;

        @BindView(R.id.tv_price)
        TextView tv_price;

        @BindView(R.id.imgv_admin_approved)
        ImageView imgv_admin_approved;

        @BindView(R.id.imgv_premium)
        ImageView imgv_premium;

        @BindView(R.id.chk_save)
        CheckBox chk_save;

        @BindView(R.id.chk_like)
        CheckBox chk_like;

        @BindView(R.id.chk_inquiry)
        CheckBox chk_inquiry;

        @BindView(R.id.tv_react_count)
        TextView tv_react_count;

        @BindView(R.id.linear_react_section)
        LinearLayout linear_react_section;

        int reactCount = 0;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final RealEstatePhotoModel model) {

            if(model.getPhotos().size() > 0)
            {
                int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                Picasso.with(context)
                        .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                        .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into(imgv_photo);
            }

            if(model.getRealestate().getIsAdminApproved() == 1)
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
            }
            else
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
            }

            if(model.getRealestate().getIsPremium() == 1)
            {
                imgv_premium.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_premium.setVisibility(View.GONE);
            }

            tv_price.setText(myDecimalFormat.getCurrencyFormat(model.getRealestate().getPrice()) + " KS");
            tv_address.setText(model.getRealestate().getAddress());
            tv_state_township.setText(model.getRealestate().getState() + " - " + model.getRealestate().getTownship());
            try {
                tv_datetime.setText(showDatetimeHelper.getDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getRealestate().getAccesstime().replace("T", " ")))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(isMe)
                    {
                        Intent intent = new Intent(context, RealEstateFormActivity.class);
                        intent.putExtra("obj", model.getRealestate());
                        context.startActivity(intent);
                    }
                    else
                    {
                        RealestatePhotoModel2 realestatePhotoModel = new RealestatePhotoModel2();
                        realestatePhotoModel.setRealestate(model.getRealestate());
                        realestatePhotoModel.setPhotos(model.getPhotos());
                        realestatePhotoModel.setContact(memberPhone);
                        realestatePhotoModel.setMemberName(memberName);

                        Intent intent = new Intent(context, RealestateDetailActivity.class);
                        intent.putExtra("obj", realestatePhotoModel);
                        context.startActivity(intent);
                    }

                }
            });

            if(isMe)
            {
                linear_react_section.setVisibility(View.GONE);
            }
            else
            {
                linear_react_section.setVisibility(View.VISIBLE);
            }
            chk_like.setChecked(model.isReact());

            reactCount = model.getReactCount();
            tv_react_count.setText(getReactCountValue(reactCount));

            chk_save.setChecked(model.isSaved());

            chk_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_like);

                    if(chk_like.isChecked())
                    {
                        reactCount ++;
                    }
                    else
                    {
                        reactCount --;
                    }

                    tv_react_count.setText(getReactCountValue(reactCount));

                    callReact = service.saveRealestateReact(model.getRealestate().getRealEstateId(), sharePreferenceHelper.getLoginMemberId());
                    callReact.enqueue(new Callback<ReactModel>() {
                        @Override
                        public void onResponse(Call<ReactModel> call, Response<ReactModel> response) {

                        }

                        @Override
                        public void onFailure(Call<ReactModel> call, Throwable t) {

                        }
                    });

                }
            });

            chk_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_save);

                    realestateSaveCallback.saveRealestate(model.getRealestate().getRealEstateId());

                }
            });

            chk_inquiry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_inquiry);*/

                    //Call Phone
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + "0" +memberPhone.substring(2)));
                    context.startActivity(intent);

                }
            });

        }

        private String getReactCountValue(int count)
        {
            if(count == 0)
            {
                return count + " like";
            }
            else
            {
                if(count > 1)
                {
                    return count + " likes";
                }
                else
                {
                    return count + " like";
                }
            }
        }

    }

}
