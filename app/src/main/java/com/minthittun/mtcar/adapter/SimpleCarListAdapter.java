package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.ReactModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class SimpleCarListAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;


    public SimpleCarListAdapter()
    {
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_simple_car, parent, false);
        return new SimpleCarListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SimpleCarListAdapter.ViewHolder) holder).bindPost((CarPhotoMemberModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_car)
        MyanBoldTextView tv_car;

        @BindView(R.id.tv_price)
        MyanBoldTextView tv_price;


        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final CarPhotoMemberModel model) {

            if(model.getPhotos() != null)
            {
                if(model.getPhotos().size() > 0)
                {
                    int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                            .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.placeholder)
                            .into(imgv_photo);
                }
            }


            tv_price.setMyanmarText(myDecimalFormat.getCurrencyFormat(model.getCar().getPrice()) + " KS");
            tv_car.setMyanmarText(model.getCar().getModelName() + " - Year " + model.getCar().getYear());

            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                    carPhotoModel.setCar(model.getCar());
                    carPhotoModel.setPhotos(model.getPhotos());
                    carPhotoModel.setContact(model.getMember().getPhone());
                    carPhotoModel.setMemberName(model.getMember().getName());
                    carPhotoModel.setMemberType(model.getMember().getType());
                    carPhotoModel.setAuthorizedDealerBrand(model.getMember().getAuthorizedDealerBrand());

                    Intent intent = new Intent(context, CarDetailActivity.class);
                    intent.putExtra("obj", carPhotoModel);
                    context.startActivity(intent);

                }
            });


        }

    }

}
