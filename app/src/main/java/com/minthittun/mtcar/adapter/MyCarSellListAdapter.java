package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.ListHeaderModel;
import com.minthittun.mtcar.model.ReactModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class MyCarSellListAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;
    private boolean isMe = false;
    private String memberId = "", memberName = "", memberPhone = "", memberType = "", authorizedDealerBrand = "";
    private CarSaveCallback carSaveCallback;
    private ServiceHelper.ApiService service;
    private SharePreferenceHelper sharePreferenceHelper;
    private Call<ReactModel> callReact;

    public void registerCallback(CarSaveCallback carSaveCallback)
    {
        this.carSaveCallback = carSaveCallback;
    }

    public MyCarSellListAdapter(boolean isMe, String memberId, String memberName, String memberPhone, String memberType, String authorizedDealerBrand, Context context)
    {
        sharePreferenceHelper = new SharePreferenceHelper(context);
        service = ServiceHelper.getClient(context);
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
        this.isMe = isMe;
        this.memberId = memberId;
        this.memberName = memberName;
        this.memberPhone = memberPhone;
        this.memberType = memberType;
        this.authorizedDealerBrand = authorizedDealerBrand;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sell_car, parent, false);
        return new MyCarSellListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyCarSellListAdapter.ViewHolder) holder).bindPost((CarPhotoModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_sell, parent, false);

        return new MyCarSellListAdapter.HeaderHolder(view);
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerHeader recyclerHeader = (RecyclerHeader) getItemsList().get(position);
        ((MyCarSellListAdapter.HeaderHolder) holder).bindHeader((ListHeaderModel)recyclerHeader.getHeaderData());
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_car)
        MyanTextView tv_car;

        @BindView(R.id.tv_car_detail)
        MyanTextView tv_car_detail;

        @BindView(R.id.tv_datetime)
        MyanTextView tv_datetime;

        @BindView(R.id.tv_price)
        MyanTextView tv_price;

        @BindView(R.id.imgv_admin_approved)
        ImageView imgv_admin_approved;

        @BindView(R.id.imgv_premium)
        ImageView imgv_premium;


        @BindView(R.id.tv_react_count)
        MyanTextView tv_react_count;

        @BindView(R.id.tv_is_active)
        MyanTextView tv_is_active;

        @BindView(R.id.tv_is_deal)
        MyanTextView tv_is_deal;


        int reactCount = 0;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final CarPhotoModel model) {

            if(model.getPhotos().size() > 0)
            {
                int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                Picasso.with(context)
                        .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                        .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into(imgv_photo);
            }

            if(model.getCar().getIsAdminApproved() == 1)
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
            }
            else
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
            }

            if(model.getCar().getIsPremium() == 1)
            {
                imgv_premium.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_premium.setVisibility(View.GONE);
            }

            if(model.getCar().isDeal())
            {
                tv_is_deal.setVisibility(View.VISIBLE);
                tv_is_deal.setMyanmarText("Sold Out");
            }
            else
            {
                tv_is_deal.setVisibility(View.GONE);
                tv_is_deal.setMyanmarText("");
            }

            if(model.getCar().isActive())
            {
                tv_is_active.setMyanmarText("Active Now");
            }
            else
            {
                tv_is_active.setMyanmarText("Pending");
            }

            tv_price.setMyanmarText(myDecimalFormat.getCurrencyFormat(model.getCar().getPrice()) + " KS");
            tv_car.setMyanmarText(model.getCar().getModelName() + " - Car No. " + model.getCar().getCarNo());
            tv_car_detail.setMyanmarText("Year " + model.getCar().getYear() + " - Miles " + model.getCar().getKilometer() + " Km");

            try {
                tv_datetime.setMyanmarText(showDatetimeHelper.getDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getCar().getAccesstime().replace("T", " ")))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(isMe)
                    {
                        Intent intent = new Intent(context, CarFormActivity.class);
                        intent.putExtra("obj", model.getCar());
                        context.startActivity(intent);
                    }
                    else
                    {
                        CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                        carPhotoModel.setCar(model.getCar());
                        carPhotoModel.setPhotos(model.getPhotos());
                        carPhotoModel.setMemberId(memberId);
                        carPhotoModel.setContact(memberPhone);
                        carPhotoModel.setMemberName(memberName);
                        carPhotoModel.setMemberType(memberType);
                        carPhotoModel.setAuthorizedDealerBrand(authorizedDealerBrand);

                        Intent intent = new Intent(context, CarDetailActivity.class);
                        intent.putExtra("obj", carPhotoModel);
                        context.startActivity(intent);
                    }

                }
            });

            reactCount = model.getReactCount();
            tv_react_count.setMyanmarText(getReactCountValue(reactCount));


        }

        private String getReactCountValue(int count)
        {
            if(count == 0)
            {
                return count + " like";
            }
            else
            {
                if(count > 1)
                {
                    return count + " likes";
                }
                else
                {
                    return count + " like";
                }
            }
        }

    }

    class HeaderHolder extends RecyclerView.ViewHolder{

        private Context context;

        public HeaderHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }
        private void bindHeader(final ListHeaderModel model) {


        }


    }

}
