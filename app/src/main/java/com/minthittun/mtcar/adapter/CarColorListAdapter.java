package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.ColorSelectCallback;
import com.minthittun.mtcar.model.ColorModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarColorListAdapter extends BaseAdapter {

    private ColorSelectCallback colorSelectCallback;

    public CarColorListAdapter(ColorSelectCallback colorSelectCallback){
        this.colorSelectCallback = colorSelectCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car_color, parent, false);
        return new CarColorListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CarColorListAdapter.ViewHolder) holder).bindPost(position, (ColorModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.cv_color)
        CardView cv_color;

        @BindView(R.id.imgv_select)
        ImageView imgv_select;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final int position, final ColorModel model) {

            if(model.isSelected())
            {
                imgv_select.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_select.setVisibility(View.GONE);
            }

            cv_color.setCardBackgroundColor(Color.parseColor(model.getColorCode()));

            cv_color.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(model.isSelected())
                    {
                        model.setSelected(false);
                    }
                    else
                    {
                        model.setSelected(true);
                    }

                    colorSelectCallback.select(position, model);

                }
            });


        }

    }


}
