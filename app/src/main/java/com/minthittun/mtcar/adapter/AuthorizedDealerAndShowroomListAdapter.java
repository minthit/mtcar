package com.minthittun.mtcar.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.AuthorizedDealerActivity;
import com.minthittun.mtcar.MyCarActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.ListHeaderModel;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class AuthorizedDealerAndShowroomListAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;

    public AuthorizedDealerAndShowroomListAdapter()
    {
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_authorized_dealer_n_showroom, parent, false);
        return new AuthorizedDealerAndShowroomListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AuthorizedDealerAndShowroomListAdapter.ViewHolder) holder).bindPost((MemberModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_authorized_dealer_n_showroom, parent, false);

        return new AuthorizedDealerAndShowroomListAdapter.HeaderHolder(view);
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerHeader recyclerHeader = (RecyclerHeader) getItemsList().get(position);
        ((AuthorizedDealerAndShowroomListAdapter.HeaderHolder) holder).bindHeader((ListHeaderModel)recyclerHeader.getHeaderData());
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_name)
        MyanTextView tv_name;

        @BindView(R.id.tv_account_type)
        MyanTextView tv_account_type;

        @BindView(R.id.tv_contact)
        MyanTextView tv_contact;

        @BindView(R.id.imgv_brand_logo)
        ImageView imgv_brand_logo;


        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final MemberModel model) {

            int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(MtCarConstant.BASE_PHOTO_URL + model.getPhoto())
                    .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerCrop()
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(imgv_photo);

            tv_name.setMyanmarText(model.getName());
            tv_account_type.setMyanmarText(model.getType());


            if(model.getType().equals("Authorized Dealer"))
            {
                imgv_brand_logo.setVisibility(View.VISIBLE);
                int resID = context.getResources().getIdentifier("brand_" + model.getAuthorizedDealerBrand().toLowerCase().replace(" ", "_"), "mipmap",  context.getPackageName());
                imgv_brand_logo.setImageResource(resID);
                tv_contact.setMyanmarText("Contact - "+model.getAuthorizedDealerContact());
            }
            else
            {
                imgv_brand_logo.setVisibility(View.GONE);
                tv_contact.setMyanmarText("Contact - "+model.getPhone());
            }

            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(model.getType().equals("Authorized Dealer"))
                    {
                        Intent intent = new Intent(context, AuthorizedDealerActivity.class);
                        intent.putExtra("member_id", model.getMemberId());
                        context.startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(context, MyCarActivity.class);
                        intent.putExtra("me", false);
                        intent.putExtra("name", model.getName());
                        intent.putExtra("phone", model.getPhone());
                        intent.putExtra("id", model.getMemberId());
                        context.startActivity(intent);
                        ActivityAnimationHelper.setFadeInOutAnimation((Activity) context);
                    }

                }
            });

        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder{

        private Context context;

        public HeaderHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }
        private void bindHeader(final ListHeaderModel model) {


        }


    }

}
