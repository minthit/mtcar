package com.minthittun.mtcar.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.PhotoDetailActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.PhotoModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarListPhotoListAdapter extends BaseAdapter {

    private CarPhotoModel2 carPhotoModel2;

    public CarListPhotoListAdapter(CarPhotoModel2 carPhotoModel2)
    {
        this.carPhotoModel2 = carPhotoModel2;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        return new CarListPhotoListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CarListPhotoListAdapter.ViewHolder) holder).bindPost((PhotoModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;


        @BindView(R.id.imgv_photo)
        ImageView imgv_car;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final PhotoModel model) {

            int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotoName())
                    .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerCrop()
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(imgv_car);

            imgv_car.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Intent intent = new Intent(context, PhotoDetailActivity.class);
                    intent.putExtra("photo", MtCarConstant.BASE_PHOTO_URL + "" +model.getPhotoName());
                    context.startActivity(intent);
                    ActivityAnimationHelper.setFadeInOutAnimation((Activity) context);*/

                    Intent intent = new Intent(context, CarDetailActivity.class);
                    intent.putExtra("obj", carPhotoModel2);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                }
            });

        }
    }

}
