package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.minthittun.mtcar.fregment.AuthorizedDealerAndShowroomFragment;
import com.minthittun.mtcar.fregment.SellFragment;
import com.minthittun.mtcar.fregment.HomeFragment;
import com.minthittun.mtcar.fregment.MenuFragment;

/**
 * Created by hpuser on 9/22/2015.
 */
public class MainAppViewPagerAdapter extends FragmentPagerAdapter {


    public MainAppViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new HomeFragment();
            case 1:
                return new SellFragment();
            case 2:
                return new AuthorizedDealerAndShowroomFragment();
            case 3:
                return new MenuFragment();
        }

        return null;
    }


    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}
