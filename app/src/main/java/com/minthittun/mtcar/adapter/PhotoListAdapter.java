package com.minthittun.mtcar.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.minthittun.mtcar.LoginActivity;
import com.minthittun.mtcar.MenuActivity;
import com.minthittun.mtcar.PhotoDetailActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.PhotoListRefreshCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.model.PhotoModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class PhotoListAdapter extends BaseAdapter {

    private PhotoListRefreshCallback photoListRefreshCallback;

    public PhotoListAdapter(PhotoListRefreshCallback photoListRefreshCallback)
    {
        this.photoListRefreshCallback = photoListRefreshCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo_list, parent, false);
        return new PhotoListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PhotoListAdapter.ViewHolder) holder).bindPost((PhotoModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.relative_item)
        RelativeLayout relative_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_member_photo;

        @BindView(R.id.btn_delete)
        Button btn_delete;

        Call<Integer> callPhotoDelete;
        ServiceHelper.ApiService service;

        ProgressDialog progressDialog;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            service = ServiceHelper.getClient(context);

            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Deleting ....");

        }

        void bindPost(final PhotoModel model) {

            int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotoName())
                    .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerCrop()
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(imgv_member_photo);

            relative_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, PhotoDetailActivity.class);
                    intent.putExtra("photo", MtCarConstant.BASE_PHOTO_URL + model.getPhotoName());
                    context.startActivity(intent);

                }
            });

            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new AlertDialog.Builder(context)
                            .setMessage("Are you sure")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int whichButton) {

                                            progressDialog.show();

                                            callPhotoDelete = service.deletePhotoById(model.getPhotoId());
                                            callPhotoDelete.enqueue(new Callback<Integer>() {
                                                @Override
                                                public void onResponse(Call<Integer> call, Response<Integer> response) {

                                                    progressDialog.dismiss();

                                                    if(response.isSuccessful())
                                                    {
                                                        photoListRefreshCallback.refreshList();
                                                        Toast.makeText(context, "Successful", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(context, "Try again", Toast.LENGTH_SHORT).show();
                                                    }

                                                }

                                                @Override
                                                public void onFailure(Call<Integer> call, Throwable t) {

                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Try again", Toast.LENGTH_SHORT).show();

                                                }
                                            });

                                        }
                                    })
                            .setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int whichButton) {

                                            dialog.dismiss();
                                        }
                                    }).show();

                }
            });

        }
    }

}
