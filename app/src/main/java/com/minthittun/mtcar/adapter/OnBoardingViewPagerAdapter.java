package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.minthittun.mtcar.fregment.HomeFragment;
import com.minthittun.mtcar.fregment.MenuFragment;
import com.minthittun.mtcar.fregment.OnBoarding1Fragment;
import com.minthittun.mtcar.fregment.OnBoarding2Fragment;
import com.minthittun.mtcar.fregment.OnBoarding3Fragment;
import com.minthittun.mtcar.fregment.OnBoardingLastFragment;
import com.minthittun.mtcar.fregment.SellFragment;

/**
 * Created by hpuser on 9/22/2015.
 */
public class OnBoardingViewPagerAdapter extends FragmentPagerAdapter {


    public OnBoardingViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new OnBoarding1Fragment();
            case 1:
                return new OnBoarding2Fragment();
            case 2:
                return new OnBoarding3Fragment();
            case 3:
                return new OnBoardingLastFragment();
        }

        return null;
    }


    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}
