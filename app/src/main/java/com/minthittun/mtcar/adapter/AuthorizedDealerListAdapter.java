package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.AuthorizedDealerActivity;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class AuthorizedDealerListAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;

    public AuthorizedDealerListAdapter()
    {
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_authorized_dealer, parent, false);
        return new AuthorizedDealerListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AuthorizedDealerListAdapter.ViewHolder) holder).bindPost((MemberModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_name)
        MyanTextView tv_name;

        @BindView(R.id.tv_brand_name)
        MyanTextView tv_brand_name;

        @BindView(R.id.tv_contact)
        MyanTextView tv_contact;



        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final MemberModel model) {

            int resID = context.getResources().getIdentifier("brand_" + model.getAuthorizedDealerBrand().toLowerCase().replace(" ", "_"), "mipmap",  context.getPackageName());
            imgv_photo.setImageResource(resID);

            tv_name.setMyanmarText(model.getName());
            tv_brand_name.setMyanmarText(model.getAuthorizedDealerBrand());
            tv_contact.setMyanmarText(model.getAuthorizedDealerContact());


            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, AuthorizedDealerActivity.class);
                    intent.putExtra("member_id", model.getMemberId());
                    context.startActivity(intent);

                }
            });

        }
    }

}
