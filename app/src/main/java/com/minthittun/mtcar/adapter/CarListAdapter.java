package com.minthittun.mtcar.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.MyCarActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.common.Pageable;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.AdvertisementModel;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.ReactModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarListAdapter extends BaseAdapter {

    private static final int ITEM_CAR_FEED = 5;
    private static final int ITEM_ADS = 6;

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;
    private CarSaveCallback carSaveCallback;
    private ServiceHelper.ApiService service;
    private SharePreferenceHelper sharePreferenceHelper;
    private Call<ReactModel> callReact;

    public CarListAdapter(Context context)
    {
        sharePreferenceHelper = new SharePreferenceHelper(context);
        service = ServiceHelper.getClient(context);
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
    }

    public void registerCallback(CarSaveCallback carSaveCallback)
    {
        this.carSaveCallback = carSaveCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_CAR_FEED)
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_car, parent, false);
            return new CarListAdapter.ViewHolder(view);
        }
        else
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ads, parent, false);
            return new CarListAdapter.AdsViewHolder(view);
        }
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        if(viewType ==  ITEM_CAR_FEED)
        {
            ((CarListAdapter.ViewHolder) holder).bindPost((CarPhotoMemberModel) getItemsList().get(position));
        }
        else if(viewType == ITEM_ADS)
        {
            ((CarListAdapter.AdsViewHolder) holder).bindPost((AdvertisementModel) getItemsList().get(position));
        }


    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        Pageable item = getItemsList().get(position);
        if (item instanceof CarPhotoMemberModel) {
            return ITEM_CAR_FEED;
        } else if (item instanceof AdvertisementModel) {
            return ITEM_ADS;
        } else {
            return super.getItemViewType(position);
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.tv_sale_or_rent)
        TextView tv_sale_or_rent;

        @BindView(R.id.chk_save)
        CheckBox chk_save;

        @BindView(R.id.chk_like)
        CheckBox chk_like;

        @BindView(R.id.chk_inquiry)
        CheckBox chk_inquiry;

        @BindView(R.id.tv_react_count)
        TextView tv_react_count;

        @BindView(R.id.rv_photo)
        RecyclerView rv_photo;
        private CarListPhotoListAdapter carListPhotoListAdapter;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.imgv_member)
        CircleImageView imgv_member;

        @BindView(R.id.tv_car)
        TextView tv_car;

        @BindView(R.id.tv_car_detail)
        TextView tv_car_detail;

        @BindView(R.id.tv_datetime)
        TextView tv_datetime;

        @BindView(R.id.tv_price)
        TextView tv_price;

        @BindView(R.id.imgv_admin_approved)
        ImageView imgv_admin_approved;

        @BindView(R.id.imgv_premium)
        ImageView imgv_premium;

        @BindView(R.id.tv_name)
        TextView  tv_name;

        @BindView(R.id.tv_member_type)
        TextView tv_member_type;

        @BindView(R.id.linear_member)
        LinearLayout linear_member;

        int reactCount = 0;


        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final CarPhotoMemberModel model) {

            //Passing data
            CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
            carPhotoModel.setCar(model.getCar());
            carPhotoModel.setPhotos(model.getPhotos());
            carPhotoModel.setMemberId(model.getMember().getMemberId());
            carPhotoModel.setContact(model.getMember().getPhone());
            carPhotoModel.setMemberName(model.getMember().getName());
            carPhotoModel.setMemberType(model.getMember().getType());
            carPhotoModel.setAuthorizedDealerBrand(model.getMember().getAuthorizedDealerBrand());

            if(model.getPhotos().size() > 0)
            {
                int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                Picasso.with(context)
                        .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                        .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into(imgv_photo);
            }

            //Photolist
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv_photo.setLayoutManager(layoutManager);
            rv_photo.setNestedScrollingEnabled(false);
            carListPhotoListAdapter = new CarListPhotoListAdapter(carPhotoModel);
            rv_photo.setAdapter(carListPhotoListAdapter);

            if(model.getCar().getSaleOrRent() == 1)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setText("For sale");
            }
            else if(model.getCar().getSaleOrRent() == 2)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setText("For rent");
            }
            else
            {
                tv_sale_or_rent.setVisibility(View.GONE);
            }

            for(int i = 0; i<model.getPhotos().size(); i++)
            {
                carListPhotoListAdapter.add(model.getPhotos().get(i));
            }

            if(model.getCar().getIsAdminApproved() == 1)
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
            }
            else
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
            }

            if(model.getCar().getIsPremium() == 1)
            {
                imgv_premium.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_premium.setVisibility(View.GONE);
            }

            tv_price.setText(myDecimalFormat.getCurrencyFormat(model.getCar().getPrice()) + " KS");
            tv_car.setText(model.getCar().getModelName() + " - " + model.getCar().getCarNo());
            tv_car_detail.setText("Year " + model.getCar().getYear() + " - Miles " + model.getCar().getKilometer() + " Km");

            chk_like.setChecked(model.isReact());

            reactCount = model.getReactCount();
            tv_react_count.setText(getReactCountValue(reactCount));

            try {
                tv_datetime.setText(showDatetimeHelper.getDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getCar().getAccesstime().replace("T", " ")))));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(MtCarConstant.BASE_PHOTO_URL + model.getMember().getPhoto())
                    .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerCrop()
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(imgv_member);

            tv_name.setText(model.getMember().getName());
            tv_member_type.setText(model.getMember().getType());
            chk_save.setChecked(model.isSaved());

            chk_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_save);

                    carSaveCallback.saveCar(model.getCar().getCarId());

                }
            });


            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                    carPhotoModel.setCar(model.getCar());
                    carPhotoModel.setPhotos(model.getPhotos());
                    carPhotoModel.setMemberId(model.getMember().getMemberId());
                    carPhotoModel.setContact(model.getMember().getPhone());
                    carPhotoModel.setMemberName(model.getMember().getName());
                    carPhotoModel.setMemberType(model.getMember().getType());
                    carPhotoModel.setAuthorizedDealerBrand(model.getMember().getAuthorizedDealerBrand());

                    Intent intent = new Intent(context, CarDetailActivity.class);
                    intent.putExtra("obj", carPhotoModel);
                    context.startActivity(intent);
                    ActivityAnimationHelper.setFadeInOutAnimation((Activity) context);

                }
            });

            linear_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, MyCarActivity.class);
                    intent.putExtra("me", false);
                    intent.putExtra("name", model.getMember().getName());
                    intent.putExtra("phone", model.getMember().getPhone());
                    intent.putExtra("member_type", model.getMember().getType());
                    intent.putExtra("id", model.getMember().getMemberId());
                    context.startActivity(intent);
                    ActivityAnimationHelper.setFadeInOutAnimation((Activity) context);

                }
            });

            chk_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_like);

                    if(chk_like.isChecked())
                    {
                        reactCount ++;
                    }
                    else
                    {
                        reactCount --;
                    }

                    tv_react_count.setText(getReactCountValue(reactCount));

                    callReact = service.saveReact(model.getCar().getCarId(), sharePreferenceHelper.getLoginMemberId());
                    callReact.enqueue(new Callback<ReactModel>() {
                        @Override
                        public void onResponse(Call<ReactModel> call, Response<ReactModel> response) {

                        }

                        @Override
                        public void onFailure(Call<ReactModel> call, Throwable t) {

                        }
                    });

                }
            });

            chk_inquiry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*YoYo.with(Techniques.RubberBand)
                            .duration(500)
                            .playOn(chk_inquiry);*/

                    //Call Phone
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + "0" +model.getMember().getPhone().substring(2)));
                    context.startActivity(intent);

                }
            });

        }

        private String getReactCountValue(int count)
        {
            if(count == 0)
            {
                return count + " like";
            }
            else
            {
                if(count > 1)
                {
                    return count + " likes";
                }
                else
                {
                    return count + " like";
                }
            }
        }

    }

    class AdsViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.imgv_ads_photo)
        ImageView imgv_ads_photo;

        @BindView(R.id.tv_title)
        TextView tv_title;

        @BindView(R.id.tv_desc)
        TextView tv_desc;

        @BindView(R.id.btn_detail)
        Button btn_detail;

        AdsViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final AdvertisementModel model) {

            int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(MtCarConstant.BASE_ADS_PHOTO_URL + model.getFilename())
                    .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerCrop()
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(imgv_ads_photo);

            tv_title.setText(model.getTitle());
            tv_desc.setText(model.getDescription());
            btn_detail.setText(model.getDisplayText());

            btn_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String url = model.getWebLink();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);

                }
            });

        }

    }

}
