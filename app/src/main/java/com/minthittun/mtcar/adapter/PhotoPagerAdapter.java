package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.minthittun.mtcar.fregment.PhotoViewFragment;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.model.PhotoModel;

import java.util.ArrayList;

/**
 * Created by hello on 9/27/17.
 */

public class PhotoPagerAdapter extends FragmentPagerAdapter {

    Context context;
    int PAGE_COUNT;
    ArrayList<PhotoModel> itemList = new ArrayList<PhotoModel>();

    public PhotoPagerAdapter(FragmentManager fm, ArrayList<PhotoModel> itemList, Context context) {
        super(fm);

        this.itemList = itemList;
        PAGE_COUNT = itemList.size();
        this.context = context;

    }

    @Override
    public Fragment getItem(int arg0) {

        PhotoViewFragment myFragment = new PhotoViewFragment();
        Bundle data = new Bundle();

        data.putString("photo", MtCarConstant.BASE_PHOTO_URL + itemList.get(arg0).getPhotoName());

        myFragment.setArguments(data);
        return myFragment;

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

}
