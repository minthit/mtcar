package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.minthittun.mtcar.AuthorizedDealerListActivity;
import com.minthittun.mtcar.CarFeedActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.model.CarBrandModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarBrandListAdapter extends BaseAdapter {


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car_brand, parent, false);
        return new CarBrandListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CarBrandListAdapter.ViewHolder) holder).bindPost((CarBrandModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_logo)
        ImageView imgv_logo;

        @BindView(R.id.tv_brand_name)
        MyanBoldTextView tv_brand_name;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final CarBrandModel model) {

            int resID = context.getResources().getIdentifier(model.getLogo(), "mipmap",  context.getPackageName());
            imgv_logo.setImageResource(resID);

            tv_brand_name.setMyanmarText(model.getBrandName());

            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CarFeedActivity.setQuery(0, 0, false, "", model.getBrandName());
                    Intent intent = new Intent(context, CarFeedActivity.class);
                    context.startActivity(intent);


                }
            });

        }

    }

}
