package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.CarTypeSelectCallback;
import com.minthittun.mtcar.model.CartypeModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarTypeListAdapter extends BaseAdapter {

    private CarTypeSelectCallback carTypeSelectCallback;

    public CarTypeListAdapter(CarTypeSelectCallback carTypeSelectCallback){
        this.carTypeSelectCallback = carTypeSelectCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car_category, parent, false);
        return new CarTypeListAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CarTypeListAdapter.ViewHolder) holder).bindPost(position, (CartypeModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.v_select)
        View v_select;

        @BindView(R.id.cv_car_category)
        CardView cv_car_category;

        @BindView(R.id.imgv_car_type)
        ImageView imgv_car_type;

        @BindView(R.id.tv_title)
        MyanBoldTextView tv_title;

        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final int position, final CartypeModel model) {

            if(model.isSelected())
            {
                v_select.setVisibility(View.VISIBLE);
            }
            else
            {
                v_select.setVisibility(View.GONE);
            }

            int resID = context.getResources().getIdentifier(model.getIcon(), "mipmap",  context.getPackageName());
            imgv_car_type.setImageResource(resID);
            tv_title.setMyanmarText(model.getTitle());

            cv_car_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(model.isSelected())
                    {
                        model.setSelected(false);
                    }
                    else
                    {
                        model.setSelected(true);
                    }

                    carTypeSelectCallback.select(position, model);

                }
            });


        }

    }


}
