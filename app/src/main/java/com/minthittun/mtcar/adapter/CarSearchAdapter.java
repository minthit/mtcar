package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.MyCarActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class CarSearchAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;
    private CarSaveCallback carSaveCallback;

    public CarSearchAdapter()
    {
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
    }

    public void registerCallback(CarSaveCallback carSaveCallback)
    {
        this.carSaveCallback = carSaveCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_car, parent, false);
        return new CarSearchAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CarSearchAdapter.ViewHolder) holder).bindPost((CarPhotoMemberModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_car)
        MyanTextView tv_car;

        @BindView(R.id.tv_sale_or_rent)
        MyanTextView tv_sale_or_rent;

        @BindView(R.id.imgv_admin_approved)
        ImageView imgv_admin_approved;

        @BindView(R.id.imgv_premium)
        ImageView imgv_premium;

        @BindView(R.id.tv_car_detail)
        MyanTextView tv_car_detail;

        @BindView(R.id.tv_datetime)
        MyanTextView tv_datetime;



        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final CarPhotoMemberModel model) {

            if(model.getPhotos().size() > 0)
            {
                int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                Picasso.with(context)
                        .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                        .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into(imgv_photo);
            }


            if(model.getCar().getSaleOrRent() == 1)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setMyanmarText("For sale");
            }
            else if(model.getCar().getSaleOrRent() == 2)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setMyanmarText("For rent");
            }
            else
            {
                tv_sale_or_rent.setVisibility(View.GONE);
            }

            if(model.getCar().getIsAdminApproved() == 1)
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
            }
            else
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
            }

            if(model.getCar().getIsPremium() == 1)
            {
                imgv_premium.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_premium.setVisibility(View.GONE);
            }

            tv_car.setMyanmarText(model.getCar().getModelName() + " - " + model.getCar().getCarNo());
            tv_car_detail.setMyanmarText(model.getCar().getYear() + " - " + model.getCar().getKilometer() + " Km");
            try {
                tv_datetime.setMyanmarText(showDatetimeHelper.getDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getCar().getAccesstime().replace("T", " ")))));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                    carPhotoModel.setCar(model.getCar());
                    carPhotoModel.setPhotos(model.getPhotos());
                    carPhotoModel.setMemberId(model.getMember().getMemberId());
                    carPhotoModel.setContact(model.getMember().getPhone());
                    carPhotoModel.setMemberName(model.getMember().getName());
                    carPhotoModel.setMemberType(model.getMember().getType());
                    carPhotoModel.setAuthorizedDealerBrand(model.getMember().getAuthorizedDealerBrand());

                    Intent intent = new Intent(context, CarDetailActivity.class);
                    intent.putExtra("obj", carPhotoModel);
                    context.startActivity(intent);

                }
            });

        }
    }

}
