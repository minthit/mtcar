package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.RealestateDetailActivity;
import com.minthittun.mtcar.common.BaseAdapter;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.RealestatePhotoMemberModel;
import com.minthittun.mtcar.model.RealestatePhotoModel2;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zmn_pc on 10/31/2016.
 */

public class RealestateSearchAdapter extends BaseAdapter {

    private MyDateFormat myDateFormat;
    private ShowDatetimeHelper showDatetimeHelper;
    private MyDecimalFormat myDecimalFormat;
    private CarSaveCallback carSaveCallback;

    public RealestateSearchAdapter()
    {
        myDateFormat = new MyDateFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        myDecimalFormat = new MyDecimalFormat();
    }

    public void registerCallback(CarSaveCallback carSaveCallback)
    {
        this.carSaveCallback = carSaveCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_realestate, parent, false);
        return new RealestateSearchAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((RealestateSearchAdapter.ViewHolder) holder).bindPost((RealestatePhotoMemberModel) getItemsList().get(position));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        @BindView(R.id.linear_item)
        LinearLayout linear_item;

        @BindView(R.id.imgv_photo)
        ImageView imgv_photo;

        @BindView(R.id.tv_address)
        TextView tv_address;

        @BindView(R.id.tv_sale_or_rent)
        TextView tv_sale_or_rent;

        @BindView(R.id.imgv_admin_approved)
        ImageView imgv_admin_approved;

        @BindView(R.id.imgv_premium)
        ImageView imgv_premium;

        @BindView(R.id.tv_state_township)
        TextView tv_state_township;

        @BindView(R.id.tv_datetime)
        TextView tv_datetime;



        ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final RealestatePhotoMemberModel model) {

            if(model.getPhotos().size() > 0)
            {
                int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
                Picasso.with(context)
                        .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                        .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into(imgv_photo);
            }


            if(model.getRealestate().getSaleOrRent() == 1)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setText("For sale");
            }
            else if(model.getRealestate().getSaleOrRent() == 2)
            {
                tv_sale_or_rent.setVisibility(View.VISIBLE);
                tv_sale_or_rent.setText("For rent");
            }
            else
            {
                tv_sale_or_rent.setVisibility(View.GONE);
            }

            if(model.getRealestate().getIsAdminApproved() == 1)
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
            }
            else
            {
                imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
            }

            if(model.getRealestate().getIsPremium() == 1)
            {
                imgv_premium.setVisibility(View.VISIBLE);
            }
            else
            {
                imgv_premium.setVisibility(View.GONE);
            }

            tv_address.setText(model.getRealestate().getAddress());
            tv_state_township.setText(model.getRealestate().getState() + " - " + model.getRealestate().getTownship());
            try {
                tv_datetime.setText(showDatetimeHelper.getDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getRealestate().getAccesstime().replace("T", " ")))));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            linear_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    RealestatePhotoModel2 realestatePhotoModel = new RealestatePhotoModel2();
                    realestatePhotoModel.setRealestate(model.getRealestate());
                    realestatePhotoModel.setPhotos(model.getPhotos());
                    realestatePhotoModel.setContact(model.getMember().getPhone());
                    realestatePhotoModel.setMemberName(model.getMember().getName());

                    Intent intent = new Intent(context, RealestateDetailActivity.class);
                    intent.putExtra("obj", realestatePhotoModel);
                    context.startActivity(intent);

                }
            });

        }
    }

}
