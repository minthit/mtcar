package com.minthittun.mtcar.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.minthittun.mtcar.R;

import butterknife.ButterKnife;

/**
 * Created by user on 6/19/2015.
 **/
public class TownshipAdapter extends ArrayAdapter<String> {

    Context mContext;
    int mLayoutResourceId;

    public TownshipAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return myGetView(position, convertView, parent);
    }


    /**
     * Returns the view for a specific item on the list
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return myGetView(position, convertView, parent);
    }

    public View myGetView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ButterKnife.bind(this, row);

        final String currentItem = getItem(position);


        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        final TextView tv_state = (TextView) row.findViewById(R.id.tv_state_division);
        tv_state.setText(currentItem);


        return row;
    }

}
