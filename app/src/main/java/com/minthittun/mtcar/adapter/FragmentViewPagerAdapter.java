package com.minthittun.mtcar.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.minthittun.mtcar.fregment.CarFeedFragment;
import com.minthittun.mtcar.fregment.RealEstateFragment;

/**
 * Created by hpuser on 9/22/2015.
 */
public class FragmentViewPagerAdapter extends FragmentPagerAdapter {


    public FragmentViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new CarFeedFragment();
            case 1:
                return new RealEstateFragment();
        }

        return null;
    }


    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
}
