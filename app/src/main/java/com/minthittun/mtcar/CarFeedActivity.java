package com.minthittun.mtcar;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.common.EndlessRecyclerViewScrollListener;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarAdsResultModel;
import com.minthittun.mtcar.model.CarPhotoMemberAdsModel;
import com.minthittun.mtcar.model.MemberModel;
import com.minthittun.mtcar.model.SaveCarModel;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarFeedActivity extends BaseActivity implements CarSaveCallback, SwipeRefreshLayout.OnRefreshListener {

    private SharePreferenceHelper sharePreferenceHelper;

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    CarListAdapter carListAdapter;

    ServiceHelper.ApiService service;
    Call<CarAdsResultModel> carCallList;
    Call<SaveCarModel> callSaveCar;

    int page = 1;
    private boolean isLoading;
    private boolean isEnd;
    private Context context;

    public static int query_premiumCar = 0;
    public static int query_featuredCar = 0;
    public static boolean query_editorChoiceCar = false;
    public static String query_category = "";
    public static String query_brand = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_car_feed;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        this.context = this;
        init();
        getCarList();

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Cars");

        swipe_refresh_layout.setOnRefreshListener(this);

        service = ServiceHelper.getClient(context);
        sharePreferenceHelper = new SharePreferenceHelper(context);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        carListAdapter = new CarListAdapter(context);
        carListAdapter.registerCallback(this);
        recycler_view.setAdapter(carListAdapter);
        recycler_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.i("INFO", "isEnd : " + String.valueOf(isEnd));
                Log.i("INFO", "isLoading : " + String.valueOf(isLoading));

                if (!isLoading && !isEnd) {

                    getCarList();

                } else if (isEnd) {
                    Log.i("INFO", "REACHED");
                }
            }
        });
    }

    public static void setQuery(int premiumCar, int featuredCar, boolean editorChoiceCar, String category, String brand)
    {
        query_premiumCar = premiumCar;
        query_featuredCar = featuredCar;
        query_editorChoiceCar = editorChoiceCar;
        query_category = category;
        query_brand = brand;
    }

    private void getCarList()
    {

        if(page == 1)
        {
            recycler_view.showShimmerAdapter();
        }

        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        carListAdapter.clearFooter();

        // Show Loading
        carListAdapter.showLoading();

        carCallList = service.getAllCarsv2(page, sharePreferenceHelper.getLoginMemberId(), query_featuredCar, query_premiumCar, query_editorChoiceCar, query_category, query_brand);
        carCallList.enqueue(new Callback<CarAdsResultModel>() {
            @Override
            public void onResponse(Call<CarAdsResultModel> call, Response<CarAdsResultModel> response) {

                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                // Loading Ends
                isLoading = false;
                carListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {

                    CarAdsResultModel resultModel = response.body();

                    isEnd = resultModel.getData().isEmpty();

                    for (CarPhotoMemberAdsModel t : resultModel.getData()) {

                        //2 = ads, 1 = car

                        if(t.getType() == 1)
                        {
                            Log.d("CarAdFeed", t.getCar().getCar().getModelName());
                            carListAdapter.add(t.getCar());
                        }
                        else if(t.getType() == 2)
                        {
                            Log.d("CarAdFeed", t.getAdvertisement().getTitle());
                            carListAdapter.add(t.getAdvertisement());
                        }

                    }
                    page++;
                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<CarAdsResultModel> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                carListAdapter.clearFooter();
                carListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getCarList();
                    }
                });
            }

        });

    }


    @Override
    public void onRefresh() {

        ServiceHelper.removeFromCache("car/getAllCars");
        page = 1;
        carListAdapter.clear();
        getCarList();

    }


    @Override
    public void saveCar(String carId) {

        callSaveCar = service.saveCar(carId, sharePreferenceHelper.getLoginMemberId());
        callSaveCar.enqueue(new Callback<SaveCarModel>() {
            @Override
            public void onResponse(Call<SaveCarModel> call, Response<SaveCarModel> response) {

            }

            @Override
            public void onFailure(Call<SaveCarModel> call, Throwable t) {

            }
        });

    }

}
