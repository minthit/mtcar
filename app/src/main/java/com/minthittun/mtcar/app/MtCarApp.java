package com.minthittun.mtcar.app;

import android.app.Application;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.koekoetech.androidcommonlibrary.helper.AndroidCommonSetup;
import com.minthittun.mtcar.BuildConfig;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.MyRealmMigration;

import net.gotev.uploadservice.UploadService;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by hello on 11/18/17.
 */

public class MtCarApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

        AndroidCommonSetup.getInstance().init(getApplicationContext());

        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Realm.init(this);
        //1.8 => 2
        //1.9 => 3
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(3)
                .migration(new MyRealmMigration())
                .build();
        Realm.setDefaultConfiguration(config);


    }

}
