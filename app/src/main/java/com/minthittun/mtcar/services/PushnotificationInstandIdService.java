package com.minthittun.mtcar.services;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by hello on 10/28/17.
 */

public class PushnotificationInstandIdService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("PushnotiInstant", "Refreshed token: " + refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("all");

    }

}
