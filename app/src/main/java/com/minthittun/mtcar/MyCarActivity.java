package com.minthittun.mtcar;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.minthittun.mtcar.adapter.MyCarListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.SaveCarModel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCarActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, CarSaveCallback {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    private SharePreferenceHelper sharePreferenceHelper;
    public static boolean isRefreshNeed = false;

    ServiceHelper.ApiService service;
    Call<ArrayList<CarPhotoModel>> callList;

    MyCarListAdapter myCarListAdapter;

    private String member_id;
    private boolean isMe;
    private String member_name = "", member_phone = "", member_type = "", authorized_dealer_brand = "";
    Call<SaveCarModel> callSaveCar;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_car;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();



        getList();

    }

    private void init()
    {
        setupToolbar(true);
        swipe_refresh_layout.setOnRefreshListener(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);

        if(getIntent().getStringExtra("id") == null)
        {
            setupToolbarText("My Car");
            member_id = sharePreferenceHelper.getLoginMemberId();
            isMe = true;
            member_type = "";
            member_name = "";
            member_phone = "";
        }
        else
        {
            setupToolbarText("Cars");
            member_id = getIntent().getStringExtra("id");
            isMe = false;
            member_name = getIntent().getStringExtra("name");
            member_phone = getIntent().getStringExtra("phone");
            member_type = getIntent().getStringExtra("member_type");
            authorized_dealer_brand = getIntent().getStringExtra("authorized_dealer_brand");
        }



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        myCarListAdapter = new MyCarListAdapter(isMe, member_id, member_name, member_phone, member_type, authorized_dealer_brand, this);
        myCarListAdapter.registerCallback(this);
        recycler_view.setAdapter(myCarListAdapter);
    }

    private void getList()
    {
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        myCarListAdapter.clearFooter();

        // Show Loading
        myCarListAdapter.showLoading();
        if(getIntent().getStringExtra("save") == null)
        {
            callList = service.getCarListByMemberId(member_id);
        }
        else
        {
            callList = service.getSaveCarListByMemberId(member_id);
        }
        callList.enqueue(new Callback<ArrayList<CarPhotoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CarPhotoModel>> call, Response<ArrayList<CarPhotoModel>> response) {

                // Loading Ends
                myCarListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {
                    ArrayList<CarPhotoModel> resultModel = response.body();

                    for (CarPhotoModel q : resultModel) {
                        myCarListAdapter.add(q);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CarPhotoModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                myCarListAdapter.clearFooter();
                myCarListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new MyCarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {

        myCarListAdapter.clear();
        getList();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if(isRefreshNeed)
        {
            onRefresh();
            isRefreshNeed = false;
        }

    }

    @Override
    public void saveCar(String carId) {

        callSaveCar = service.saveCar(carId, sharePreferenceHelper.getLoginMemberId());
        callSaveCar.enqueue(new Callback<SaveCarModel>() {
            @Override
            public void onResponse(Call<SaveCarModel> call, Response<SaveCarModel> response) {

            }

            @Override
            public void onFailure(Call<SaveCarModel> call, Throwable t) {

            }
        });

    }
}
