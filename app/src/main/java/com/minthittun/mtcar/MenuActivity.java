package com.minthittun.mtcar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minthittun.mtcar.adapter.MyCarListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;

public class MenuActivity extends BaseActivity {

    @BindView(R.id.imgv_photo)
    ImageView imgv_photo;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_type)
    TextView tv_type;

    @BindView(R.id.linear_profile)
    LinearLayout linear_profile;


    @BindView(R.id.linear_save_car)
    LinearLayout linear_save_car;

    @BindView(R.id.linear_sell_car)
    LinearLayout linear_sell_car;

    @BindView(R.id.linear_logout)
    LinearLayout linear_logout;

    private SharePreferenceHelper sharePreferenceHelper;

    private MemberModel memberModel;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_menu;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        getMemberInformation();

        linear_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, ProfileActivity.class);
                startActivity(intent);

            }
        });


        linear_save_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, MyCarActivity.class);
                intent.putExtra("me", false);
                intent.putExtra("name", "");
                intent.putExtra("phone", sharePreferenceHelper.getLoginMemberPhone());
                intent.putExtra("id", sharePreferenceHelper.getLoginMemberId());
                intent.putExtra("member_type", memberModel.getType());
                intent.putExtra("authorized_dealer_brand", memberModel.getAuthorizedDealerBrand());
                intent.putExtra("save", "save");
                startActivity(intent);

            }
        });

        linear_sell_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, MyCarActivity.class);

                startActivity(intent);

            }
        });

        linear_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(MenuActivity.this)
                        .setMessage("Are you sure")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        deleteAllMember();

                                        sharePreferenceHelper.logoutSharePreference();
                                        Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        }
                                        startActivity(intent);

                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        dialog.dismiss();
                                    }
                                }).show();

            }
        });

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Menu");

        sharePreferenceHelper = new SharePreferenceHelper(this);
    }

    private void deleteAllMember()
    {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<MemberModel> results = realm.where(MemberModel.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    private void getMemberInformation()
    {
        Realm realm = Realm.getDefaultInstance();
        memberModel = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
        Picasso.with(MenuActivity.this)
                .load(MtCarConstant.BASE_PHOTO_URL + memberModel.getPhoto())
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerCrop()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_photo);

        tv_name.setText(memberModel.getName());
        tv_type.setText(memberModel.getType());
    }



}
