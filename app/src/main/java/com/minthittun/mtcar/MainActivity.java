package com.minthittun.mtcar;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.common.EndlessRecyclerViewScrollListener;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarResultModel;
import com.minthittun.mtcar.model.MemberModel;
import com.minthittun.mtcar.model.SaveCarModel;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements CarSaveCallback, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.fbtn_add_car)
    FloatingActionButton fbtn_add_car;

    private SharePreferenceHelper sharePreferenceHelper;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    CarListAdapter carListAdapter;

    ServiceHelper.ApiService service;
    Call<CarResultModel> carCallList;
    Call<SaveCarModel> callSaveCar;

    int page = 1;
    private boolean isLoading;
    private boolean isEnd;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getCarList();
        checkProfile();

        fbtn_add_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, CarFormActivity.class);
                startActivity(intent);

            }
        });

    }

    private void init()
    {
        setupToolbar(false);
        setupToolbarText(" MyCar");
        getSupportActionBar().setIcon(R.mipmap.car_toolbar);

        swipe_refresh_layout.setOnRefreshListener(this);

        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        carListAdapter = new CarListAdapter(this);
        carListAdapter.registerCallback(this);
        recycler_view.setAdapter(carListAdapter);
        recycler_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.i("INFO", "isEnd : " + String.valueOf(isEnd));
                Log.i("INFO", "isLoading : " + String.valueOf(isLoading));

                if (!isLoading && !isEnd) {

                    getCarList();

                } else if (isEnd) {
                    Log.i("INFO", "REACHED");
                }
            }
        });
    }

    private void getCarList()
    {

        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        carListAdapter.clearFooter();

        // Show Loading
        carListAdapter.showLoading();

        carCallList = service.getAllCars(page, sharePreferenceHelper.getLoginMemberId());
        carCallList.enqueue(new Callback<CarResultModel>() {
            @Override
            public void onResponse(Call<CarResultModel> call, Response<CarResultModel> response) {

                // Loading Ends
                isLoading = false;
                carListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {


                    CarResultModel resultModel = response.body();

                    isEnd = resultModel.getData().isEmpty();

                    for (CarPhotoMemberModel t : resultModel.getData()) {
                        carListAdapter.add(t);
                    }
                    page++;
                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<CarResultModel> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                carListAdapter.clearFooter();
                carListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getCarList();
                    }
                });
            }

        });

    }

    private void checkProfile()
    {
        Realm realm = Realm.getDefaultInstance();
        MemberModel model = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        if(model.getName() == null)
        {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_profile)
        {

            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        ServiceHelper.removeFromCache("car/getAllCars");
        page = 1;
        carListAdapter.clear();
        getCarList();

    }


    @Override
    public void saveCar(String carId) {

        callSaveCar = service.saveCar(carId, sharePreferenceHelper.getLoginMemberId());
        callSaveCar.enqueue(new Callback<SaveCarModel>() {
            @Override
            public void onResponse(Call<SaveCarModel> call, Response<SaveCarModel> response) {

            }

            @Override
            public void onFailure(Call<SaveCarModel> call, Throwable t) {

            }
        });

    }
}
