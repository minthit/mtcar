package com.minthittun.mtcar;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.JsonDbSetup;
import com.minthittun.mtcar.helper.SharePreferenceHelper;

import java.io.IOException;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.linear_logo)
    LinearLayout linear_logo;

    @BindView(R.id.cv_item)
    CardView cv_item;

    private SharePreferenceHelper sharePreferenceHelper;
    private JsonDbSetup jsonDbSetup;

    @BindView(R.id.tv_version)
    MyanTextView tv_version;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(false);
        getSupportActionBar().hide();
        jsonDbSetup = new JsonDbSetup(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);

        try {
            tv_version.setMyanmarText("Version " + getVersionName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(jsonDbSetup.isDbsetupDone())
        {
            cv_item.setVisibility(View.GONE);
            linear_logo.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    initProcess();

                }

            }, 1500);
        }
        else
        {
            cv_item.setVisibility(View.VISIBLE);
            linear_logo.setVisibility(View.GONE);
            try {
                jsonDbSetup.locationDataSetup();
                initProcess();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void initProcess()
    {
        if(sharePreferenceHelper.isLogin())
        {
            if(sharePreferenceHelper.isTutorialShown())
            {
                Intent intent = new Intent(this, MainAppActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(this);
            }
            else
            {
                Intent intent = new Intent(this, OnBoardingViewActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(this);
            }
        }
        else
        {
            Intent intent = new Intent(this, LoginActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            startActivity(intent);
            ActivityAnimationHelper.setFadeInOutAnimation(this);
        }
    }

    private String getVersionName() throws PackageManager.NameNotFoundException {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                this.getPackageName(), 0);

        return info.versionName;
    }

}
