package com.minthittun.mtcar;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.minthittun.mtcar.adapter.CarBrandListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.model.CarBrandModel;

import butterknife.BindView;

public class BrandListActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private CarBrandListAdapter carBrandListAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_brand_list;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getCarBrandList();

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Brands In Myanmar");

        carBrandListAdapter = new CarBrandListAdapter();
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recycler_view.setAdapter(carBrandListAdapter);
    }

    private void getCarBrandList()
    {
        carBrandListAdapter.clear();
        String[] brands = getResources().getStringArray(R.array.manufacture);
        for (int i = 1; i<=(brands.length - 1); i++) {

            CarBrandModel model = new CarBrandModel();
            model.setBrandName(brands[i]);
            model.setLogo("brand_" + brands[i].toLowerCase().replace(" ", "_").replace("-","_"));
            carBrandListAdapter.add(model);

        }

    }

}
