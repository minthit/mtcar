package com.minthittun.mtcar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoModel2;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import butterknife.BindView;

public class CarDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private CarPhotoModel2 carModel;
    private MyDecimalFormat myDecimalFormat;
    private ShowDatetimeHelper showDatetimeHelper;

    @BindView(R.id.slider_gallery)
    SliderLayout slider_gallery;

    @BindView(R.id.tv_price)
    MyanBoldTextView tv_price;

    @BindView(R.id.tv_car_detail)
    MyanBoldTextView tv_car_detail;

    @BindView(R.id.tv_car_model)
    MyanBoldTextView tv_car_model;

    @BindView(R.id.btn_contact)
    Button btn_contact;

    @BindView(R.id.btn_authorized_dealer_contact)
    Button btn_authorized_dealer_contact;

    @BindView(R.id.tv_transmission_type)
    MyanTextView tv_transmission_type;

    @BindView(R.id.tv_fuel)
    MyanTextView tv_fuel;

    @BindView(R.id.tv_color)
    MyanTextView tv_color;

    @BindView(R.id.tv_engine)
    MyanTextView tv_engine;

    @BindView(R.id.tv_steering_place)
    MyanTextView tv_steering_place;

    @BindView(R.id.tv_steering_type)
    MyanTextView tv_steering_type;

    @BindView(R.id.tv_detail)
    MyanTextView tv_detail;

    @BindView(R.id.tv_grade)
    MyanBoldTextView tv_grade;

    @BindView(R.id.tv_insurance)
    MyanTextView tv_insurance;

    @BindView(R.id.tv_current_location)
    MyanTextView tv_current_location;

    @BindView(R.id.imgv_admin_approved)
    ImageView imgv_admin_approved;

    @BindView(R.id.imgv_premium)
    ImageView imgv_premium;


    private Handler handler;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_car_detail;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        bindData();

        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Call Phone
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "0" +carModel.getContact().substring(2)));
                startActivity(intent);

            }
        });

        btn_authorized_dealer_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent intent = new Intent(CarDetailActivity.this, AuthorizedDealerActivity.class);
            intent.putExtra("member_id", carModel.getMemberId());
            startActivity(intent);

            }
        });

        /*handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                btn_contact.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeIn)
                        .duration(700)
                        .playOn(btn_contact);

            }
        }, 500);*/

    }

    private void init()
    {
        setupToolbar(true);

        handler = new Handler();

        myDecimalFormat = new MyDecimalFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        carModel = (CarPhotoModel2) getIntent().getSerializableExtra("obj");
        setupToolbarText(carModel.getCar().getModelName());
    }

    private void bindData()
    {
        HashMap<String,String> url_maps = new HashMap<String, String>();

        for(int i = 0; i<carModel.getPhotos().size(); i++)
        {
            url_maps.put(UUID.randomUUID().toString(), MtCarConstant.BASE_PHOTO_URL + carModel.getPhotos().get(i).getPhotoName());
        }

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            slider_gallery.addSlider(textSliderView);

            slider_gallery.setPresetTransformer(SliderLayout.Transformer.Accordion);
            slider_gallery.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            slider_gallery.setCustomAnimation(new DescriptionAnimation());
            slider_gallery.setDuration(5000);
            slider_gallery.addOnPageChangeListener(this);
        }

        tv_car_model.setMyanmarText(carModel.getCar().getModelName() + " - Car No. " + carModel.getCar().getCarNo());
        tv_price.setMyanmarText(myDecimalFormat.getCurrencyFormat(carModel.getCar().getPrice()) + " KS");
        tv_car_detail.setMyanmarText("Year " + carModel.getCar().getYear() + " - Miles " + carModel.getCar().getKilometer() + " Km");
        tv_transmission_type.setMyanmarText(carModel.getCar().getTransmissionType());
        tv_fuel.setMyanmarText(carModel.getCar().getFuel());
        tv_color.setMyanmarText(carModel.getCar().getColor());
        tv_engine.setMyanmarText(String.valueOf(carModel.getCar().getEngine()));
        tv_steering_place.setMyanmarText(carModel.getCar().getSteeringPlace());
        tv_steering_type.setMyanmarText(carModel.getCar().getSteeringType());
        tv_detail.setMyanmarText(isNullOrEmptyDash(carModel.getCar().getDetail()));
        tv_grade.setMyanmarText(isNullOrEmptyDash(carModel.getCar().getGrade()));
        tv_insurance.setMyanmarText(isNullOrEmptyDash(carModel.getCar().getInsuranceCompany()) + " - " + carModel.getCar().getInsuranceRemain() + " Months");
        tv_current_location.setMyanmarText(isNullOrEmptyDash(carModel.getCar().getCurrentLocation()));

        if(carModel.getCar().getIsAdminApproved() == 1)
        {
            imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
        }
        else
        {
            imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
        }

        if(carModel.getCar().getIsPremium() == 1)
        {
            imgv_premium.setVisibility(View.VISIBLE);
        }
        else
        {
            imgv_premium.setVisibility(View.GONE);
        }

        if(carModel.getMemberType().equals("Authorized Dealer"))
        {
            btn_contact.setVisibility(View.GONE);

            btn_authorized_dealer_contact.setVisibility(View.VISIBLE);
            btn_authorized_dealer_contact.setText(carModel.getAuthorizedDealerBrand() + " Authorized Dealer");
        }
        else
        {
            btn_authorized_dealer_contact.setVisibility(View.GONE);
            btn_contact.setVisibility(View.VISIBLE);
        }

    }

    private int slidePosition = 0;

    @Override
    public void onSliderClick(BaseSliderView slider) {

        /*Intent intent = new Intent(CarDetailActivity.this, PhotoDetailActivity.class);
        intent.putExtra("photo", slider.getUrl());
        startActivity(intent);
        ActivityAnimationHelper.setFadeInOutAnimation(CarDetailActivity.this);*/

        Intent intent = new Intent(CarDetailActivity.this, PhotoDetailPagerActivity.class);
        intent.putExtra("photos", carModel.getPhotos());
        intent.putExtra("current_index", slidePosition);
        startActivity(intent);
        ActivityAnimationHelper.setFadeInOutAnimation(CarDetailActivity.this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        slidePosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private String isNullOrEmptyDash(String text)
    {
        if(text != null)
        {
            if(text.equals("") || text.isEmpty())
            {
                return " - ";
            }
            else
            {
                return text;
            }
        }
        else
        {
            return " - ";
        }
    }


}
