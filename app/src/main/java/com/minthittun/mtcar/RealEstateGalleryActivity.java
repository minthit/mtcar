package com.minthittun.mtcar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.minthittun.mtcar.adapter.PhotoListAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.CameraHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.PhotoListRefreshCallback;
import com.minthittun.mtcar.helper.PictureCameraGalleryCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.PhotoModel;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealEstateGalleryActivity extends BaseActivity implements PhotoListRefreshCallback, SwipeRefreshLayout.OnRefreshListener, PictureCameraGalleryCallback, UploadStatusDelegate {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    PhotoListAdapter photoListAdapter;
    SharePreferenceHelper sharePreferenceHelper;
    CameraHelper cameraHelper;
    ServiceHelper.ApiService service;
    Call<ArrayList<PhotoModel>> callPhotoList;
    private ProgressDialog progressDialog;
    private Handler handler;
    private String picturePath;

    @BindView(R.id.btn_camera)
    Button btn_camera;

    @BindView(R.id.btn_gallery)
    Button btn_gallery;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_real_estate_gallery;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCamera();
            }
        });

        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGallery();
            }
        });

        getPhotoList(getIntent().getStringExtra("id"));

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Gallery");

        swipe_refresh_layout.setOnRefreshListener(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        cameraHelper = new CameraHelper(this);
        cameraHelper.registerGalleryCallback(this);
        service = ServiceHelper.getClient(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recycler_view.setLayoutManager(gridLayoutManager);
        photoListAdapter = new PhotoListAdapter(this);
        recycler_view.setAdapter(photoListAdapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.setCancelable(false);
        handler = new Handler();
    }

    private void getPhotoList(String realEstateId)
    {
        swipe_refresh_layout.setRefreshing(false);

        photoListAdapter.clear();
        // Clear already showing footers
        photoListAdapter.clearFooter();

        // Show Loading
        photoListAdapter.showLoading();

        callPhotoList = service.getRealEstatePhotoList(realEstateId);
        callPhotoList.enqueue(new Callback<ArrayList<PhotoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PhotoModel>> call, Response<ArrayList<PhotoModel>> response) {

                photoListAdapter.clearFooter();
                if(response.isSuccessful())
                {
                    for (PhotoModel t : response.body()) {
                        photoListAdapter.add(t);
                    }
                }
                else
                {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<PhotoModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                photoListAdapter.clearFooter();
                photoListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new PhotoListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getPhotoList(getIntent().getStringExtra("id"));
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {

        swipe_refresh_layout.setRefreshing(false);
        getPhotoList(getIntent().getStringExtra("id"));

    }

    public void callCamera() {

        sharePreferenceHelper.setTempPhotoName(cameraHelper.getDateTImeStampForFileName() + ".jpg");

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File pictureDirectory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                cameraHelper.TEMP_FOLDER_NAME);
        if (!pictureDirectory.exists()) {
            pictureDirectory.mkdirs();
        }
        File file = new File(pictureDirectory.getAbsolutePath()
                + File.separator + sharePreferenceHelper.getTempPhotoName());

        sharePreferenceHelper.setTempPhotoPath(file.getAbsolutePath());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, 222);
    }

    public void callGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, 111);
    }

    private void galleryProcess(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap bmImg = BitmapFactory.decodeFile(picturePath, options);
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, picturePath);

        cameraHelper.saveGalleryPhoto(temp);
    }

    private void cameraProcess() {
        File pictureDirectory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                cameraHelper.TEMP_FOLDER_NAME);
        File file = new File(pictureDirectory.getAbsolutePath()
                + File.separator + sharePreferenceHelper.getTempPhotoName());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;// reduce images resolution

        Bitmap bmImg = BitmapFactory.decodeFile(file.getAbsolutePath(),
                options);

        Log.d("PhotoListActivity", "FileName : " + sharePreferenceHelper.getTempPhotoName() + " Path : " + file.getPath());
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, file.getAbsolutePath());

        cameraHelper.saveGalleryPhoto(temp);

        progressDialog.dismiss();
    }

    public void uploadMultipart(String path) {
        try {
            MultipartUploadRequest req =
                    new MultipartUploadRequest(this, MtCarConstant.BASE_URL + "car/carGalleryPhotoUpload")
                            .addFileToUpload(path, "file")
                            .addParameter("carId",  getIntent().getStringExtra("id"))
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2);

            String uploadID = req.setDelegate(this).startUpload();

            cameraHelper.deleteFile(sharePreferenceHelper.getTempPhotoPath());

        } catch (Exception exc) {
            Log.e("AndroidUploadService", exc.getMessage(), exc);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 111) {

                galleryProcess(data);

            } else if (requestCode == 222) {

                progressDialog.show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        cameraProcess();

                    }
                }, 2000);


            }
        }

    }



    @Override
    public void getFilePath(String path) {

        uploadMultipart(path);

    }

    @Override
    public void refreshList() {

        getPhotoList(getIntent().getStringExtra("id"));

    }


    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {
        progressDialog.show();
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        progressDialog.show();
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Uploaded successful!", Toast.LENGTH_SHORT).show();
        getPhotoList(getIntent().getStringExtra("id"));
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {
        progressDialog.show();
    }
}
