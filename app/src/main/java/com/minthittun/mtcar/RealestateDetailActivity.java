package com.minthittun.mtcar;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ShowDatetimeHelper;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.RealestatePhotoModel2;

import java.util.HashMap;
import java.util.UUID;

import butterknife.BindView;

public class RealestateDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private RealestatePhotoModel2 realEstateModel;
    private MyDecimalFormat myDecimalFormat;
    private ShowDatetimeHelper showDatetimeHelper;

    @BindView(R.id.slider_gallery)
    SliderLayout slider_gallery;

    @BindView(R.id.tv_price)
    TextView tv_price;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_state_township)
    TextView tv_state_township;

    @BindView(R.id.btn_contact)
    Button btn_contact;

    @BindView(R.id.imgv_admin_approved)
    ImageView imgv_admin_approved;

    @BindView(R.id.imgv_premium)
    ImageView imgv_premium;

    @BindView(R.id.tv_detail)
    TextView tv_detail;

    @BindView(R.id.tv_house_size)
    TextView tv_house_size;

    @BindView(R.id.tv_lot_size)
    TextView tv_lot_size;

    @BindView(R.id.tv_bed)
    TextView tv_bed;

    @BindView(R.id.tv_bath)
    TextView tv_bath;

    private Handler handler;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_realestate_detail;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        bindData();

        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Call Phone
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "0" +realEstateModel.getContact().substring(2)));
                startActivity(intent);

            }
        });

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                btn_contact.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeIn)
                        .duration(700)
                        .playOn(btn_contact);

            }
        }, 500);

    }

    private void init()
    {
        setupToolbar(true);

        handler = new Handler();

        myDecimalFormat = new MyDecimalFormat();
        showDatetimeHelper = new ShowDatetimeHelper();
        realEstateModel = (RealestatePhotoModel2) getIntent().getSerializableExtra("obj");
        setupToolbarText(realEstateModel.getRealestate().getAddress());
    }

    private void bindData()
    {
        HashMap<String,String> url_maps = new HashMap<String, String>();

        for(int i = 0; i<realEstateModel.getPhotos().size(); i++)
        {
            url_maps.put(UUID.randomUUID().toString(), MtCarConstant.BASE_PHOTO_URL + realEstateModel.getPhotos().get(i).getPhotoName());
        }

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            slider_gallery.addSlider(textSliderView);

            slider_gallery.setPresetTransformer(SliderLayout.Transformer.Accordion);
            slider_gallery.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            slider_gallery.setCustomAnimation(new DescriptionAnimation());
            slider_gallery.setDuration(5000);
            slider_gallery.addOnPageChangeListener(this);
        }

        tv_address.setText(realEstateModel.getRealestate().getAddress());
        tv_price.setText(myDecimalFormat.getCurrencyFormat(realEstateModel.getRealestate().getPrice()) + " KS");
        tv_state_township.setText(realEstateModel.getRealestate().getState() + " - " + realEstateModel.getRealestate().getTownship());

        if(realEstateModel.getRealestate().getIsAdminApproved() == 1)
        {
            imgv_admin_approved.setImageResource(R.mipmap.admin_approved_active);
        }
        else
        {
            imgv_admin_approved.setImageResource(R.mipmap.admin_approved);
        }

        if(realEstateModel.getRealestate().getIsPremium() == 1)
        {
            imgv_premium.setVisibility(View.VISIBLE);
        }
        else
        {
            imgv_premium.setVisibility(View.GONE);
        }

        tv_bath.setText(String.valueOf(realEstateModel.getRealestate().getBaths()));
        tv_bed.setText(String.valueOf(realEstateModel.getRealestate().getBeds()));
        tv_house_size.setText(String.valueOf(realEstateModel.getRealestate().getHouseSize()) + " Sq Ft");
        tv_lot_size.setText(String.valueOf(realEstateModel.getRealestate().getLotSize()) + " Sq Ft Lot");
        tv_detail.setText(realEstateModel.getRealestate().getDetail());

    }

    private int slidePosition = 0;

    @Override
    public void onSliderClick(BaseSliderView slider) {

        /*Intent intent = new Intent(RealestateDetailActivity.this, PhotoDetailActivity.class);
        intent.putExtra("photo", slider.getUrl());
        startActivity(intent);
        ActivityAnimationHelper.setFadeInOutAnimation(RealestateDetailActivity.this);*/

        Intent intent = new Intent(RealestateDetailActivity.this, PhotoDetailPagerActivity.class);
        intent.putExtra("photos", realEstateModel.getPhotos());
        intent.putExtra("current_index", slidePosition);
        startActivity(intent);
        ActivityAnimationHelper.setFadeInOutAnimation(RealestateDetailActivity.this);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        slidePosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
