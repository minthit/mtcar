package com.minthittun.mtcar;

import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.materialrangebar.IRangeBarFormatter;
import com.appyvet.materialrangebar.RangeBar;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.adapter.CarSearchAdapter;
import com.minthittun.mtcar.common.BaseActivity;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.MyDateFormat;
import com.minthittun.mtcar.helper.MyDecimalFormat;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarResultModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarSearchActivity extends BaseActivity implements CarSaveCallback {

    private Timer timer;

    @BindView(R.id.linear_close)
    LinearLayout linear_close;

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    @BindView(R.id.edt_keyword)
    EditText edt_keyword;

    @BindView(R.id.linear_filter)
    LinearLayout linear_filter;


    ServiceHelper.ApiService service;
    Call<ArrayList<CarPhotoMemberModel>> carCallList;

    CarSearchAdapter carSearchAdapter;

    private SharePreferenceHelper sharePreferenceHelper;
    private MyDecimalFormat myDecimalFormat;

    //Filter
    private boolean isFiltered = false;
    private int PRICE_RANGE_CONST = 1000000;
    private float minPricePreview = 50, minprice = 50, maxPricePreview = 1000, maxprice = 1000;
    private float minYear = 1990, maxYear = 2018;
    private String keyword = "", manufacture = "", category = "";
    private TextView tv_price_range_left_preview, tv_price_range_right_preview,
            tv_year_range_left_preview, tv_year_range_right_preview;
    private RangeBar range_bar_price, range_bar_year;
    private Spinner spinner_manufacture, spinner_category;
    private Button btn_filter;
    private LinearLayout linear_clear_filter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_car_search;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        edt_keyword.addTextChangedListener(searchTextWatcher);

        linear_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        linear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = getLayoutInflater().inflate(R.layout.car_filter_layout, null);

                final BottomSheetDialog dialog = new BottomSheetDialog(CarSearchActivity.this);
                dialog.setContentView(view);


                //init
                tv_price_range_left_preview = dialog.findViewById(R.id.tv_price_range_left_preview);
                tv_price_range_right_preview = dialog.findViewById(R.id.tv_price_range_right_preview);
                tv_year_range_left_preview = dialog.findViewById(R.id.tv_year_range_left_preview);
                tv_year_range_right_preview = dialog.findViewById(R.id.tv_year_range_right_preview);
                btn_filter = dialog.findViewById(R.id.btn_filter);
                spinner_manufacture = dialog.findViewById(R.id.spinner_manufacture);
                spinner_category = dialog.findViewById(R.id.spinner_category);
                linear_clear_filter = dialog.findViewById(R.id.linear_clear_filter);
                range_bar_price = dialog.findViewById(R.id.range_bar_price);
                range_bar_year = dialog.findViewById(R.id.range_bar_year);
                range_bar_year.setTickEnd(Calendar.getInstance().get(Calendar.YEAR));
                //init

                dialog.show();

                //data bind

                selectValue(spinner_manufacture, manufacture);
                selectValue(spinner_category, category);

                range_bar_price.setRangePinsByValue(minPricePreview, maxPricePreview);
                minprice = Float.parseFloat(range_bar_price.getLeftPinValue()) * PRICE_RANGE_CONST;
                maxprice = Float.parseFloat(range_bar_price.getRightPinValue()) * PRICE_RANGE_CONST;
                tv_price_range_left_preview.setText(myDecimalFormat.getCurrencyFormat(minprice) + " KS");
                tv_price_range_right_preview.setText(myDecimalFormat.getCurrencyFormat(maxprice) +  " KS");

                range_bar_year.setRangePinsByValue(minYear, maxYear);
                tv_year_range_left_preview.setText((int)minYear + " Year");
                tv_year_range_right_preview.setText((int)maxYear + " Year");
                //data bind

                //event

                range_bar_price.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                    @Override
                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                        minPricePreview = Float.parseFloat(leftPinValue);
                        maxPricePreview = Float.parseFloat(rightPinValue);
                        minprice = Float.parseFloat(leftPinValue) * PRICE_RANGE_CONST;
                        maxprice = Float.parseFloat(rightPinValue) * PRICE_RANGE_CONST;
                        tv_price_range_left_preview.setText(myDecimalFormat.getCurrencyFormat(minprice) + " KS");
                        tv_price_range_right_preview.setText(myDecimalFormat.getCurrencyFormat(maxprice) +  " KS");

                    }
                });

                range_bar_price.setFormatter(new IRangeBarFormatter() {
                    @Override
                    public String format(String value) {

                        return value + "lk";
                    }
                });

                range_bar_year.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                    @Override
                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                        minYear = Float.parseFloat(leftPinValue);
                        maxYear = Float.parseFloat(rightPinValue);
                        tv_year_range_left_preview.setText((int)minYear + " Year");
                        tv_year_range_right_preview.setText((int)maxYear + " Year");
                    }
                });

                linear_clear_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        manufacture = "";
                        category = "";

                        minprice = 50 * PRICE_RANGE_CONST;
                        minPricePreview = 50;
                        maxprice = 1000 * PRICE_RANGE_CONST;
                        maxPricePreview = 1000;

                        minYear = 1990;
                        maxYear = 2018;

                        range_bar_price.setRangePinsByValue(minPricePreview, maxPricePreview);
                        range_bar_year.setRangePinsByValue(minYear, maxYear);

                        performSearch();

                        dialog.dismiss();

                    }
                });

                btn_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        isFiltered = true;
                        dialog.dismiss();

                        if(!spinner_manufacture.getSelectedItem().toString().equals("Select"))
                        {
                            manufacture = spinner_manufacture.getSelectedItem().toString();
                        }

                        if(!spinner_category.getSelectedItem().toString().equals("Select"))
                        {
                            category = spinner_category.getSelectedItem().toString();
                        }

                        performSearch();

                    }
                });

            }
        });



    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Car Search");
        getSupportActionBar().hide();

        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        myDecimalFormat = new MyDecimalFormat();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        carSearchAdapter = new CarSearchAdapter();
        carSearchAdapter.registerCallback(this);
        recycler_view.setAdapter(carSearchAdapter);

    }

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // do your actual work here

                    CarSearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!edt_keyword.getText().toString().isEmpty())
                            {
                                keyword = edt_keyword.getText().toString();
                                performSearch();
                            }
                            else
                            {
                                keyword = "";
                            }
                        }
                    });

                }
            }, 600);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // nothing to do here
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // user is typing: reset already started timer (if existing)
            if (timer != null) {
                timer.cancel();
            }
        }
    };


    private void performSearch()
    {
        Log.d("CarSearch", "Keyword : " + keyword +
                                        "\nManufacture : " + manufacture +
                                        "\nCategory : " + category +
                                        "\nMinPrice : " + minprice +
                                        "\nMaxPrice : " + maxprice +
                                        "\nMinYear : " + minYear +
                                        "\nMaxYear : " + maxYear);

        if(!isFiltered)
        {
            minprice = 50 * PRICE_RANGE_CONST;
            maxprice = 1000 * PRICE_RANGE_CONST;
        }

        recycler_view.showShimmerAdapter();

        carSearchAdapter.clear();
        // Clear already showing footers
        carSearchAdapter.clearFooter();

        // Show Loading
        carSearchAdapter.showLoading();

        carCallList = service.searchCar(sharePreferenceHelper.getLoginMemberId(),
                keyword, manufacture, category, minprice, maxprice, (int)minYear, (int)maxYear);
        carCallList.enqueue(new Callback<ArrayList<CarPhotoMemberModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CarPhotoMemberModel>> call, Response<ArrayList<CarPhotoMemberModel>> response) {

                recycler_view.hideShimmerAdapter();
                // Loading Ends
                carSearchAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {


                    ArrayList<CarPhotoMemberModel> resultModel = response.body();

                    if(resultModel.size() > 0)
                    {
                        for (CarPhotoMemberModel t : resultModel) {
                            carSearchAdapter.add(t);
                        }
                    }
                    else
                    {
                        carSearchAdapter.showEmptyView(R.layout.layout_empty_view);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CarPhotoMemberModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                recycler_view.hideShimmerAdapter();
                carSearchAdapter.clearFooter();
                carSearchAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        performSearch();
                    }
                });
            }

        });
    }


    @Override
    public void saveCar(String carId) {

    }

    private void selectValue(Spinner spinner, Object value)
    {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

}
