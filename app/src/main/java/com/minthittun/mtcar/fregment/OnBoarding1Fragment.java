package com.minthittun.mtcar.fregment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.LoginActivity;
import com.minthittun.mtcar.MyCarActivity;
import com.minthittun.mtcar.ProfileActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by hpuser on 9/22/2015.
 */

public class OnBoarding1Fragment extends Fragment{

    private Context context;

    @BindView(R.id.imgv_pic)
    ImageView imgv_pic;

    @BindView(R.id.tv_title)
    MyanBoldTextView tv_title;

    @BindView(R.id.tv_desc)
    MyanTextView tv_desc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_onboarding_1, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init();

        showData();

        return rootView;

    }

    private void init() {

    }

    private void showData(){

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(imgv_pic);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_title);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_desc);

        imgv_pic.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);
        tv_desc.setVisibility(View.VISIBLE);
    }


}

