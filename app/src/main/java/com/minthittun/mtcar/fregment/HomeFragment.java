package com.minthittun.mtcar.fregment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanProgressDialog;
import com.minthittun.mtcar.BrandListActivity;
import com.minthittun.mtcar.CarDetailActivity;
import com.minthittun.mtcar.CarFeedActivity;
import com.minthittun.mtcar.CarSearchActivity;
import com.minthittun.mtcar.MainAppActivity;
import com.minthittun.mtcar.ProfileActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.adapter.CarBrandListAdapter;
import com.minthittun.mtcar.adapter.SimpleCarListAdapter;
import com.minthittun.mtcar.cardadapterlib.CarCardPagerAdapter;
import com.minthittun.mtcar.cardadapterlib.CarTypeCardPagerAdapter;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.CustomDialogHelper;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarBrandModel;
import com.minthittun.mtcar.model.CarHomeViewModel;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarPhotoModel2;
import com.minthittun.mtcar.model.CartypeModel;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hpuser on 9/22/2015.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.linear_main)
    LinearLayout linear_main;

    @BindView(R.id.btn_brands_car)
    Button btn_brands_car;

    @BindView(R.id.btn_feature_car)
    Button btn_feature_car;

    @BindView(R.id.btn_premium_car)
    Button btn_premium_car;

    @BindView(R.id.btn_editor_choice_car)
    Button btn_editor_choice_car;

    @BindView(R.id.cv_car_of_day)
    CardView cv_car_of_day;

    @BindView(R.id.imgv_car_of_day)
    ImageView imgv_car_of_day;

    @BindView(R.id.tv_car_of_day_title)
    MyanBoldTextView tv_car_of_day_title;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private SimpleCarListAdapter simpleCarListAdapter;

    @BindView(R.id.rv_brands)
    RecyclerView rv_brands;
    private CarBrandListAdapter carBrandListAdapter;

    @BindView(R.id.edt_search)
    EditText edt_search;

    @BindView(R.id.car_type_view_pager)
    ViewPager car_type_view_pager;

    @BindView(R.id.feature_car_view_pager)
    ViewPager feature_car_view_pager;

    @BindView(R.id.premium_car_view_pager)
    ViewPager premium_car_view_pager;

    @BindView(R.id.nested_scroll)
    NestedScrollView nested_scroll;

    @BindView(R.id.cv_toolbar)
    CardView cv_toolbar;

    @BindView(R.id.cv_sell_your_car)
    CardView cv_sell_your_car;

    private CarCardPagerAdapter featureCarCardPagerAdapter;
    private CarCardPagerAdapter premiumCarCardPagerAdapter;
    private CarTypeCardPagerAdapter carTypeCardPagerAdapter;

    private ServiceHelper.ApiService service;
    private Call<CarHomeViewModel> callHomeView;

    private SharePreferenceHelper sharePreferenceHelper;

    private MyanProgressDialog progressDialog;

    private CarHomeViewModel carHomeViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);

        ButterKnife.bind(this, rootView);

        init();
        getData();

        Log.d("MemberID", sharePreferenceHelper.getLoginMemberId());

        nested_scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                if (scrollY > oldScrollY) {
                    cv_toolbar.setElevation(6);
                }
                if (scrollY < oldScrollY) {
                    cv_toolbar.setElevation(6);
                }

                if (scrollY == 0) {
                    cv_toolbar.setElevation(0);
                }

                if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )) {
                    cv_toolbar.setElevation(6);
                }
            }
        });

        edt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), CarSearchActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        btn_brands_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), BrandListActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        btn_editor_choice_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarFeedActivity.setQuery(0, 0, true, "", "");
                Intent intent = new Intent(getContext(), CarFeedActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        btn_premium_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarFeedActivity.setQuery(1, 0, false, "", "");
                Intent intent = new Intent(getContext(), CarFeedActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        btn_feature_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarFeedActivity.setQuery(0, 1, false, "", "");
                Intent intent = new Intent(getContext(), CarFeedActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        cv_sell_your_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainAppActivity)getActivity()).movePager(1);

            }
        });

        return rootView;

    }

    private void init() {

        sharePreferenceHelper = new SharePreferenceHelper(getContext());
        progressDialog = new MyanProgressDialog(getContext());
        service = ServiceHelper.getClient(getContext());
        cv_toolbar.setElevation(0);

        simpleCarListAdapter = new SimpleCarListAdapter();
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recycler_view.setAdapter(simpleCarListAdapter);

        carBrandListAdapter = new CarBrandListAdapter();
        rv_brands.setHasFixedSize(true);
        rv_brands.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rv_brands.setAdapter(carBrandListAdapter);

        featureCarCardPagerAdapter = new CarCardPagerAdapter();
        feature_car_view_pager.setAdapter(featureCarCardPagerAdapter);
        feature_car_view_pager.setOffscreenPageLimit(3);

        premiumCarCardPagerAdapter = new CarCardPagerAdapter();
        premium_car_view_pager.setAdapter(premiumCarCardPagerAdapter);
        premium_car_view_pager.setOffscreenPageLimit(3);

        carTypeCardPagerAdapter = new CarTypeCardPagerAdapter();
        car_type_view_pager.setAdapter(carTypeCardPagerAdapter);
        car_type_view_pager.setOffscreenPageLimit(3);

    }

    private void getData()
    {
        linear_main.setVisibility(View.GONE);
        progressDialog.showDialog();
        callHomeView = service.getCarDiscover(sharePreferenceHelper.getLoginMemberId());
        callHomeView.enqueue(new Callback<CarHomeViewModel>() {
            @Override
            public void onResponse(Call<CarHomeViewModel> call, Response<CarHomeViewModel> response) {

                progressDialog.hideDialog();
                if(response.isSuccessful())
                {
                    if(response.body() != null)
                    {
                        if(response.body().getCarOfTheDay() != null && response.body().getEditorChoice().size() > 0
                                && response.body().getFeaturedCar().size() > 0 && response.body().getPremiumCar().size() > 0)
                        {
                            linear_main.setVisibility(View.VISIBLE);
                            carHomeViewModel = response.body();
                            bindData(carHomeViewModel);
                        }
                    }
                }
                else
                {
                    retry();
                }

            }

            @Override
            public void onFailure(Call<CarHomeViewModel> call, Throwable t) {

                retry();

            }

            private void retry()
            {
                final CustomDialogHelper customDialogHelper = new CustomDialogHelper(getContext(), false, false);
                customDialogHelper.showDialog("Connection problem", "Please try again.", "Retry");
                customDialogHelper.getTextView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        customDialogHelper.hideDialog();
                        getData();

                    }
                });
            }

        });
    }

    private void bindData(CarHomeViewModel model)
    {
        getCarOfDay(model.getCarOfTheDay());
        getEditorChoice(model.getEditorChoice());
        getFeatureCar(model.getFeaturedCar());
        getPremiumCar(model.getPremiumCar());

        getCarBrandList();
        getCarTypeList();
    }


    private void getCarOfDay(final CarPhotoMemberModel model)
    {
        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));
        Picasso.with(getContext())
                .load(MtCarConstant.BASE_PHOTO_URL + model.getPhotos().get(0).getPhotoName())
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerCrop()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_car_of_day);
        tv_car_of_day_title.setMyanmarText(model.getCar().getModelName() + " - Year " + model.getCar().getYear());

        cv_car_of_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CarPhotoModel2 carPhotoModel = new CarPhotoModel2();
                carPhotoModel.setCar(model.getCar());
                carPhotoModel.setPhotos(model.getPhotos());
                carPhotoModel.setMemberId(model.getMember().getMemberId());
                carPhotoModel.setContact(model.getMember().getPhone());
                carPhotoModel.setMemberName(model.getMember().getName());
                carPhotoModel.setMemberType(model.getMember().getType());
                carPhotoModel.setAuthorizedDealerBrand(model.getMember().getAuthorizedDealerBrand());

                Intent intent = new Intent(getContext(), CarDetailActivity.class);
                intent.putExtra("obj", carPhotoModel);
                getContext().startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation((Activity) getContext());

            }
        });

    }

    private void getEditorChoice(ArrayList<CarPhotoMemberModel> list)
    {
        simpleCarListAdapter.clear();
        for (CarPhotoMemberModel model :
                list) {

            simpleCarListAdapter.add(model);

        }

    }

    private void getFeatureCar(ArrayList<CarPhotoMemberModel> list)
    {
        featureCarCardPagerAdapter.clearList();
        for (CarPhotoMemberModel model :
                list) {
            featureCarCardPagerAdapter.addCardItem(model);
        }
        featureCarCardPagerAdapter.notifyDataSetChanged();
    }

    private void getPremiumCar(ArrayList<CarPhotoMemberModel> list)
    {
        premiumCarCardPagerAdapter.clearList();
        for (CarPhotoMemberModel model :
                list) {
            premiumCarCardPagerAdapter.addCardItem(model);
        }
        premiumCarCardPagerAdapter.notifyDataSetChanged();
    }

    private void getCarBrandList()
    {
        carBrandListAdapter.clear();
        String[] brands = getResources().getStringArray(R.array.manufacture);
        for (int i = 1; i<=6; i++) {

            CarBrandModel model = new CarBrandModel();
            model.setBrandName(brands[i]);
            model.setLogo("brand_" + brands[i].toLowerCase().replace(" ", "_"));
            carBrandListAdapter.add(model);

        }

    }

    private void getCarTypeList()
    {
        String[] categories = getResources().getStringArray(R.array.category);
        for(int i = 1; i<=(categories.length - 1); i++)
        {
            CartypeModel cartype = new CartypeModel();
            cartype.setTitle(categories[i]);
            cartype.setIcon("type_" + categories[i].toLowerCase().replace(" ", "_"));
            carTypeCardPagerAdapter.addCardItem(cartype);
        }
        carTypeCardPagerAdapter.notifyDataSetChanged();
    }

    private void checkProfile()
    {
        Realm realm = Realm.getDefaultInstance();
        MemberModel model = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        if(model.getName() == null)
        {
            final CustomDialogHelper customDialogHelper = new CustomDialogHelper(getContext(), false, false);
            customDialogHelper.showDialog("Profile information", "Please setup your profile.", "Setup");
            customDialogHelper.getTextView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    customDialogHelper.hideDialog();

                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    startActivity(intent);
                    ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        checkProfile();

    }
}

