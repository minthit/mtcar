package com.minthittun.mtcar.fregment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.adapter.AuthorizedDealerAndShowroomListAdapter;
import com.minthittun.mtcar.adapter.AuthorizedDealerListAdapter;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.adapter.MyCarListAdapter;
import com.minthittun.mtcar.adapter.MyCarSellListAdapter;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.ListHeaderModel;
import com.minthittun.mtcar.model.MemberModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hpuser on 9/22/2015.
 */

public class AuthorizedDealerAndShowroomFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private boolean _isLoaded = false;
    private Context context;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;


    ServiceHelper.ApiService service;
    Call<ArrayList<MemberModel>> callList;

    AuthorizedDealerAndShowroomListAdapter authorizedDealerAndShowroomListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_authorized_dealer_n_showroom, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init();


        return rootView;

    }

    private void init() {

        swipe_refresh_layout.setOnRefreshListener(this);
        service = ServiceHelper.getClient(getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        authorizedDealerAndShowroomListAdapter = new AuthorizedDealerAndShowroomListAdapter();
        recycler_view.setAdapter(authorizedDealerAndShowroomListAdapter);

        addHeader();
    }

    private void addHeader()
    {
        ListHeaderModel listHeaderModel = new ListHeaderModel();
        listHeaderModel.setTitle("");
        authorizedDealerAndShowroomListAdapter.addHeader(listHeaderModel);
    }

    private void getList()
    {


        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        authorizedDealerAndShowroomListAdapter.clearFooter();

        // Show Loading
        authorizedDealerAndShowroomListAdapter.showLoading();
        callList = service.getAuthorizedDealerAndShowRoom();
        callList.enqueue(new Callback<ArrayList<MemberModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberModel>> call, Response<ArrayList<MemberModel>> response) {

                // Loading Ends
                authorizedDealerAndShowroomListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {
                    ArrayList<MemberModel> resultModel = response.body();



                    if(resultModel.size() > 0)
                    {
                        for (MemberModel q : resultModel) {
                            authorizedDealerAndShowroomListAdapter.add(q);
                        }
                    }
                    else
                    {
                        authorizedDealerAndShowroomListAdapter.showEmptyView(R.layout.empty_layout);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<MemberModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                authorizedDealerAndShowroomListAdapter.clearFooter();
                authorizedDealerAndShowroomListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new MyCarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {

        authorizedDealerAndShowroomListAdapter.clear();
        addHeader();
        getList();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            getList();
            _isLoaded = true;
        }

    }

}

