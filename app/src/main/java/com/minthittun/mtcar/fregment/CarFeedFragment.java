package com.minthittun.mtcar.fregment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.CarSearchActivity;
import com.minthittun.mtcar.MainActivity;
import com.minthittun.mtcar.MainAppActivity;
import com.minthittun.mtcar.ProfileActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.common.EndlessRecyclerViewScrollListener;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarAdsResultModel;
import com.minthittun.mtcar.model.CarPhotoMemberAdsModel;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarResultModel;
import com.minthittun.mtcar.model.MemberModel;
import com.minthittun.mtcar.model.SaveCarModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hpuser on 9/22/2015.
 */

public class CarFeedFragment extends Fragment implements CarSaveCallback, SwipeRefreshLayout.OnRefreshListener {

    private SharePreferenceHelper sharePreferenceHelper;

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    CarListAdapter carListAdapter;

    ServiceHelper.ApiService service;
    Call<CarAdsResultModel> carCallList;
    Call<SaveCarModel> callSaveCar;

    int page = 1;
    private boolean isLoading;
    private boolean isEnd;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_car_feed, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init(rootView);
        getCarList();
        checkProfile();
        

        return rootView;

    }

    private void init(View v) {

        swipe_refresh_layout.setOnRefreshListener(this);

        service = ServiceHelper.getClient(v.getContext());
        sharePreferenceHelper = new SharePreferenceHelper(v.getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(v.getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        carListAdapter = new CarListAdapter(getContext());
        carListAdapter.registerCallback(this);
        recycler_view.setAdapter(carListAdapter);
        recycler_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.i("INFO", "isEnd : " + String.valueOf(isEnd));
                Log.i("INFO", "isLoading : " + String.valueOf(isLoading));

                if (!isLoading && !isEnd) {

                    getCarList();

                } else if (isEnd) {
                    Log.i("INFO", "REACHED");
                }
            }
        });

    }

    private void getCarList()
    {

        if(page == 1)
        {
            recycler_view.showShimmerAdapter();
        }

        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        carListAdapter.clearFooter();

        // Show Loading
        carListAdapter.showLoading();

        //carCallList = service.getAllCarsv2(page, sharePreferenceHelper.getLoginMemberId());
        carCallList.enqueue(new Callback<CarAdsResultModel>() {
            @Override
            public void onResponse(Call<CarAdsResultModel> call, Response<CarAdsResultModel> response) {

                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                // Loading Ends
                isLoading = false;
                carListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {

                    CarAdsResultModel resultModel = response.body();

                    isEnd = resultModel.getData().isEmpty();

                    for (CarPhotoMemberAdsModel t : resultModel.getData()) {

                        //2 = ads, 1 = car

                        if(t.getType() == 1)
                        {
                            Log.d("CarAdFeed", t.getCar().getCar().getModelName());
                            carListAdapter.add(t.getCar());
                        }
                        else if(t.getType() == 2)
                        {
                            Log.d("CarAdFeed", t.getAdvertisement().getTitle());
                            carListAdapter.add(t.getAdvertisement());
                        }

                    }
                    page++;
                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<CarAdsResultModel> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                carListAdapter.clearFooter();
                carListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getCarList();
                    }
                });
            }

        });

    }

    private void checkProfile()
    {
        Realm realm = Realm.getDefaultInstance();
        MemberModel model = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        if(model.getName() == null)
        {
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {

        ServiceHelper.removeFromCache("car/getAllCars");
        page = 1;
        carListAdapter.clear();
        getCarList();

    }


    @Override
    public void saveCar(String carId) {

        callSaveCar = service.saveCar(carId, sharePreferenceHelper.getLoginMemberId());
        callSaveCar.enqueue(new Callback<SaveCarModel>() {
            @Override
            public void onResponse(Call<SaveCarModel> call, Response<SaveCarModel> response) {

            }

            @Override
            public void onFailure(Call<SaveCarModel> call, Throwable t) {

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_car, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                // do s.th.

                Intent intent = new Intent(getContext(), CarSearchActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                return true;
            case R.id.action_add:
                // do s.th.

                Intent carAddIntent = new Intent(getContext(), CarFormActivity.class);
                startActivity(carAddIntent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

