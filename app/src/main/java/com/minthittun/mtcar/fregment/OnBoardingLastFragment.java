package com.minthittun.mtcar.fregment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.MainAppActivity;
import com.minthittun.mtcar.OnBoardingViewActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hpuser on 9/22/2015.
 */

public class OnBoardingLastFragment extends Fragment{

    private boolean _isLoaded = false;
    private Context context;

    @BindView(R.id.imgv_pic)
    ImageView imgv_pic;

    @BindView(R.id.tv_title)
    MyanBoldTextView tv_title;

    @BindView(R.id.tv_desc)
    MyanTextView tv_desc;

    private SharePreferenceHelper sharePreferenceHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_onboarding_last, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init();

        imgv_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharePreferenceHelper.setTutorialShown();
                Intent intent = new Intent(getContext(), MainAppActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation((Activity) getContext());

            }
        });

        tv_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharePreferenceHelper.setTutorialShown();
                Intent intent = new Intent(getContext(), MainAppActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation((Activity) getContext());

            }
        });

        return rootView;

    }

    private void init() {
        sharePreferenceHelper = new SharePreferenceHelper(getContext());
    }

    private void showData(){
        YoYo.with(Techniques.RotateIn)
                .duration(800)
                .playOn(imgv_pic);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_title);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_desc);

        imgv_pic.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);
        tv_desc.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            showData();
            _isLoaded = true;
        }

    }

}

