package com.minthittun.mtcar.fregment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.adapter.MyCarListAdapter;
import com.minthittun.mtcar.adapter.MyCarSellListAdapter;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoModel;
import com.minthittun.mtcar.model.ListHeaderModel;
import com.minthittun.mtcar.model.SaveCarModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hpuser on 9/22/2015.
 */

public class SellFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private boolean _isLoaded = false;
    private Context context;

    @BindView(R.id.fbtn_sell)
    FloatingActionButton fbtn_sell;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    private SharePreferenceHelper sharePreferenceHelper;

    ServiceHelper.ApiService service;
    Call<ArrayList<CarPhotoModel>> callList;

    MyCarSellListAdapter myCarListAdapter;

    private String member_id;
    private boolean isMe;
    private String member_name = "", member_phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_sell, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init();

        fbtn_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent carAddIntent = new Intent(getContext(), CarFormActivity.class);
                startActivity(carAddIntent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });


        return rootView;

    }

    private void init() {

        swipe_refresh_layout.setOnRefreshListener(this);
        sharePreferenceHelper = new SharePreferenceHelper(getContext());
        service = ServiceHelper.getClient(getContext());

        member_id = sharePreferenceHelper.getLoginMemberId();
        isMe = true;
        member_name = "";
        member_phone = "";

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        myCarListAdapter = new MyCarSellListAdapter(isMe, sharePreferenceHelper.getLoginMemberId(), member_name, member_phone, "", "", getContext());
        recycler_view.setAdapter(myCarListAdapter);

        addHeader();
    }

    private void addHeader()
    {
        ListHeaderModel listHeaderModel = new ListHeaderModel();
        listHeaderModel.setTitle("");
        myCarListAdapter.addHeader(listHeaderModel);
    }

    private void getList()
    {
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        myCarListAdapter.clearFooter();

        // Show Loading
        myCarListAdapter.showLoading();
        callList = service.getCarListByMemberId(member_id);
        callList.enqueue(new Callback<ArrayList<CarPhotoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CarPhotoModel>> call, Response<ArrayList<CarPhotoModel>> response) {

                // Loading Ends
                myCarListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {
                    ArrayList<CarPhotoModel> resultModel = response.body();



                    if(resultModel.size() > 0)
                    {
                        for (CarPhotoModel q : resultModel) {
                            myCarListAdapter.add(q);
                        }
                    }
                    else
                    {
                        myCarListAdapter.showEmptyView(R.layout.empty_layout);
                    }

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CarPhotoModel>> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                myCarListAdapter.clearFooter();
                myCarListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new MyCarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {

        myCarListAdapter.clear();
        addHeader();
        getList();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            getList();
            _isLoaded = true;
        }

    }

}

