package com.minthittun.mtcar.fregment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hpuser on 9/22/2015.
 */

public class PhotoViewFragment extends Fragment {

    private String photo;
    @BindView(R.id.imgv_photo)
    ImageView imgv_photo;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Bundle data = getArguments();

        photo = data.getString("photo");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_photo_view, container, false);
        setHasOptionsMenu(true);

        ButterKnife.bind(this, rootView);
        init(rootView);



        return rootView;

    }

    private void init(View v) {

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));

        Picasso.with(v.getContext())
                .load(photo)
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerInside()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_photo);

    }

}

