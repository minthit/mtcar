package com.minthittun.mtcar.fregment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.minthittun.mtcar.CarFormActivity;
import com.minthittun.mtcar.CarSearchActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.RealEstateFormActivity;
import com.minthittun.mtcar.RealestateSearchActivity;
import com.minthittun.mtcar.adapter.CarListAdapter;
import com.minthittun.mtcar.adapter.RealestateListAdapter;
import com.minthittun.mtcar.common.EndlessRecyclerViewScrollListener;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.CarSaveCallback;
import com.minthittun.mtcar.helper.RealestateSaveCallback;
import com.minthittun.mtcar.helper.ServiceHelper;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.CarPhotoMemberModel;
import com.minthittun.mtcar.model.CarResultModel;
import com.minthittun.mtcar.model.RealestateAdsResultModel;
import com.minthittun.mtcar.model.RealestatePhotoMemberAdsModel;
import com.minthittun.mtcar.model.RealestatePhotoMemberModel;
import com.minthittun.mtcar.model.RealestateResultModel;
import com.minthittun.mtcar.model.SaveCarModel;
import com.minthittun.mtcar.model.SaveRealestateModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hpuser on 9/22/2015.
 */

public class RealEstateFragment extends Fragment implements RealestateSaveCallback, SwipeRefreshLayout.OnRefreshListener {

    private boolean _isLoaded = false;

    private SharePreferenceHelper sharePreferenceHelper;

    @BindView(R.id.recycler_view)
    ShimmerRecyclerView recycler_view;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    RealestateListAdapter realestateListAdapter;

    ServiceHelper.ApiService service;
    Call<RealestateAdsResultModel> realestateCallList;
    Call<SaveRealestateModel> callSaveRealestate;

    int page = 1;
    private boolean isLoading;
    private boolean isEnd;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_real_estate, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init(rootView);


        return rootView;

    }

    private void init(View v) {

        swipe_refresh_layout.setOnRefreshListener(this);

        service = ServiceHelper.getClient(v.getContext());
        sharePreferenceHelper = new SharePreferenceHelper(v.getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(v.getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        realestateListAdapter = new RealestateListAdapter(getContext());
        realestateListAdapter.registerCallback(this);
        recycler_view.setAdapter(realestateListAdapter);
        recycler_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.i("INFO", "isEnd : " + String.valueOf(isEnd));
                Log.i("INFO", "isLoading : " + String.valueOf(isLoading));

                if (!isLoading && !isEnd) {

                    getList();

                } else if (isEnd) {
                    Log.i("INFO", "REACHED");
                }
            }
        });

    }

    private void getList()
    {
        if(page == 1)
        {
            recycler_view.showShimmerAdapter();
        }

        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        // Clear already showing footers
        realestateListAdapter.clearFooter();

        // Show Loading
        realestateListAdapter.showLoading();

        realestateCallList = service.getAllRealestatesv2(page, sharePreferenceHelper.getLoginMemberId());
        realestateCallList.enqueue(new Callback<RealestateAdsResultModel>() {
            @Override
            public void onResponse(Call<RealestateAdsResultModel> call, Response<RealestateAdsResultModel> response) {

                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                // Loading Ends
                isLoading = false;
                realestateListAdapter.clearFooter();
                // Loading Ends

                if (response.isSuccessful()) {


                    RealestateAdsResultModel resultModel = response.body();

                    isEnd = resultModel.getData().isEmpty();

                    for (RealestatePhotoMemberAdsModel t : resultModel.getData()) {

                        //2 = ads, 1 = realestate

                        if(t.getType() == 1)
                        {
                            realestateListAdapter.add(t.getRealestate());
                        }
                        else if(t.getType() == 2)
                        {
                            realestateListAdapter.add(t.getAdvertisement());
                        }

                    }
                    page++;
                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<RealestateAdsResultModel> call, Throwable t) {
                handleFailure();
            }

            // handling Network Failure
            private void handleFailure() {
                if(page == 1)
                {
                    recycler_view.hideShimmerAdapter();
                }
                realestateListAdapter.clearFooter();
                realestateListAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new CarListAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getList();
                    }
                });
            }

        });

    }

    @Override
    public void onRefresh() {

        ServiceHelper.removeFromCache("realestate/getAllRealestates");
        page = 1;
        realestateListAdapter.clear();
        getList();

    }


    @Override
    public void saveRealestate(String realestateId) {
        callSaveRealestate = service.saveRealestate(realestateId, sharePreferenceHelper.getLoginMemberId());
        callSaveRealestate.enqueue(new Callback<SaveRealestateModel>() {
            @Override
            public void onResponse(Call<SaveRealestateModel> call, Response<SaveRealestateModel> response) {

            }

            @Override
            public void onFailure(Call<SaveRealestateModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            getList();
            _isLoaded = true;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_real_estate, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                // do s.th.

                Intent intent = new Intent(getContext(), RealestateSearchActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                return true;
            case R.id.action_add:
                // do s.th.

                Intent realEstateIntent = new Intent(getContext(), RealEstateFormActivity.class);
                startActivity(realEstateIntent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

