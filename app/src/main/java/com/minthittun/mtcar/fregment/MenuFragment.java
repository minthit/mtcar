package com.minthittun.mtcar.fregment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minthittun.mtcar.LoginActivity;
import com.minthittun.mtcar.MenuActivity;
import com.minthittun.mtcar.MyCarActivity;
import com.minthittun.mtcar.MyRealestateActivity;
import com.minthittun.mtcar.ProfileActivity;
import com.minthittun.mtcar.R;
import com.minthittun.mtcar.helper.ActivityAnimationHelper;
import com.minthittun.mtcar.helper.BitmapTransform;
import com.minthittun.mtcar.helper.MtCarConstant;
import com.minthittun.mtcar.helper.SharePreferenceHelper;
import com.minthittun.mtcar.model.MemberModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by hpuser on 9/22/2015.
 */

public class MenuFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private boolean _isLoaded = false;

    private Context context;
    @BindView(R.id.imgv_photo)
    CircleImageView imgv_photo;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_type)
    TextView tv_type;

    @BindView(R.id.linear_profile)
    LinearLayout linear_profile;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.linear_save_car)
    LinearLayout linear_save_car;

    @BindView(R.id.linear_sell_car)
    LinearLayout linear_sell_car;

    /*@BindView(R.id.linear_sell_real_estate)
    LinearLayout linear_sell_real_estate;

    @BindView(R.id.linear_save_realestate)
    LinearLayout linear_save_realestate;*/

    @BindView(R.id.linear_logout)
    LinearLayout linear_logout;

    private SharePreferenceHelper sharePreferenceHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_menu, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init(rootView);



        linear_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), ProfileActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });


        linear_save_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), MyCarActivity.class);
                intent.putExtra("me", false);
                intent.putExtra("name", "");
                intent.putExtra("member_type", "");
                intent.putExtra("phone", sharePreferenceHelper.getLoginMemberPhone());
                intent.putExtra("id", sharePreferenceHelper.getLoginMemberId());
                intent.putExtra("save", "save");
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        linear_sell_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), MyCarActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        /*linear_sell_real_estate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), MyRealestateActivity.class);
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });

        linear_save_realestate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), MyRealestateActivity.class);
                intent.putExtra("me", false);
                intent.putExtra("name", "");
                intent.putExtra("phone", sharePreferenceHelper.getLoginMemberPhone());
                intent.putExtra("id", sharePreferenceHelper.getLoginMemberId());
                intent.putExtra("save", "save");
                startActivity(intent);
                ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

            }
        });*/

        linear_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        deleteAllMember();

                                        sharePreferenceHelper.logoutSharePreference();
                                        Intent intent = new Intent(getContext(), LoginActivity.class);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        }
                                        startActivity(intent);
                                        ActivityAnimationHelper.setFadeInOutAnimation(getActivity());

                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        dialog.dismiss();
                                    }
                                }).show();

            }
        });

        return rootView;

    }

    private void init(View v) {

        swipe_refresh_layout.setOnRefreshListener(this);
        sharePreferenceHelper = new SharePreferenceHelper(v.getContext());
    }

    private void deleteAllMember()
    {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<MemberModel> results = realm.where(MemberModel.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    private void getMemberInformation()
    {
        Realm realm = Realm.getDefaultInstance();
        MemberModel model = realm.where(MemberModel.class).equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();

        int size = (int) Math.ceil(Math.sqrt(MtCarConstant.MAX_WIDTH * MtCarConstant.MAX_HEIGHT));

        Picasso.with(context)
                .load(MtCarConstant.BASE_PHOTO_URL + model.getPhoto())
                .transform(new BitmapTransform(MtCarConstant.MAX_WIDTH, MtCarConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerCrop()
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(imgv_photo);

        tv_name.setText(model.getName());
        tv_type.setText(model.getType());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            getMemberInformation();
            _isLoaded = true;
        }

    }

    @Override
    public void onRefresh() {

        swipe_refresh_layout.setRefreshing(false);
        getMemberInformation();

    }
}

