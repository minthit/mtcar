package com.minthittun.mtcar.fregment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.koekoetech.androidcommonlibrary.custom_control.MyanBoldTextView;
import com.koekoetech.androidcommonlibrary.custom_control.MyanTextView;
import com.minthittun.mtcar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hpuser on 9/22/2015.
 */

public class OnBoarding2Fragment extends Fragment{

    private boolean _isLoaded = false;
    private Context context;

    @BindView(R.id.imgv_pic)
    ImageView imgv_pic;

    @BindView(R.id.tv_title)
    MyanBoldTextView tv_title;

    @BindView(R.id.tv_desc)
    MyanTextView tv_desc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.fragment_onboarding_2, container, false);

        ButterKnife.bind(this, rootView);
        this.context = rootView.getContext();
        init();


        return rootView;

    }

    private void init() {

    }

    private void showData(){
        YoYo.with(Techniques.FlipInY)
                .duration(800)
                .playOn(imgv_pic);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_title);

        YoYo.with(Techniques.FadeIn)
                .duration(800)
                .playOn(tv_desc);

        imgv_pic.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);
        tv_desc.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && !_isLoaded)
        {
            showData();
            _isLoaded = true;
        }

    }

}

